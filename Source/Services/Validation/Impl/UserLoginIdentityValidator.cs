﻿using System.Linq;
using StartupApp.Domain.Model;
using Startup.Framework.Persistence.Interfaces;
using Startup.Framework.Validation.Impl;

namespace StartupApp.Services.Validation.Impl
{
    public class UserLoginIdentityValidator : ValidatorBase<UserLoginIdentity>
    {
        #region Private Variables
        private readonly IRepository<UserLoginIdentity> _repository;
        #endregion

        #region Constructors
        public UserLoginIdentityValidator(IRepository<UserLoginIdentity> repository)
        {
            _repository = repository;
        }
        #endregion

        #region Implementation of ValidatorBase<BetaInvite>
        protected override void ValidateEntity(UserLoginIdentity entity)
        {
            Validate_Identity_Doesnt_Exist(entity);

            Validate_Provider_Doesnt_Exist_For_User(entity);
        }
        #endregion

        #region Private Methods
        private void Validate_Identity_Doesnt_Exist(UserLoginIdentity entity)
        {
            var userIdentities = _repository.QueryAll().Where(u => u.LoginProvider == entity.LoginProvider &&
                                                                   u.ProviderKey == entity.ProviderKey).ToList();

            if (userIdentities.Count > 1 || (userIdentities.Count == 1 && userIdentities[0].Id != entity.Id))
            {
                AddValidationError(new DefaultValidationError("UserLoginIdentity", string.Format("{0} account is already associated with another login.", entity.LoginProvider.ToLower())));
            }
        }

        private void Validate_Provider_Doesnt_Exist_For_User(UserLoginIdentity entity)
        {
            var userIdentities = _repository.QueryAll().Where(u => u.LoginProvider == entity.LoginProvider &&
                                                                   u.User.Id == entity.User.Id).ToList();

            if (userIdentities.Count > 1 || (userIdentities.Count == 1 && userIdentities[0].Id != entity.Id))
            {
                AddValidationError(new DefaultValidationError("UserLoginIdentity", string.Format("You already have an associated login with {0}.", entity.LoginProvider.ToLower())));
            }
        }
        #endregion
    }
}
