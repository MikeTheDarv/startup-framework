﻿using System;
using System.Linq;
using StartupApp.Common.Enums;
using StartupApp.Domain.Model;
using Startup.Framework.Persistence.Interfaces;
using Startup.Framework.Validation.Impl;

namespace StartupApp.Services.Validation.Impl
{
    public class UserValidator : ValidatorBase<User>
    {
        #region Private Variables
        private readonly IRepository<User> _repository;
        #endregion

        #region Constructors
        public UserValidator(IRepository<User> repository)
        {
            _repository = repository;
        }
        #endregion

        #region Implementation of ValidatorBase<BetaInvite>
        protected override void ValidateEntity(User entity)
        {
            if (entity.IsSystemProtected)
            {
                AddValidationError(new DefaultValidationError("IsSystemProtected",
                                                              "This user is system protected and cannot be edited."));
                return;
            }

            if (entity.UserType == ApplicationUserType.Null)
            {
                AddValidationError(new DefaultValidationError("UserType", "User type cannot be null."));
            }

            Validate_LoginName_And_EmailAddress_Are_Unique(entity);

            Validate_User_Has_Identity(entity);
        }
        #endregion

        #region Private Methods
        private void Validate_LoginName_And_EmailAddress_Are_Unique(User entity)
        {
            var users = _repository.QueryAll().Where(u => u.EmailAddress == entity.EmailAddress ||
                                                          u.UserName == entity.UserName).ToList();

            if (users.Count > 1 || (users.Count == 1 && users[0].Id != entity.Id))
            {
                // -- Email address taken
                if (users[0].EmailAddress.Equals(entity.EmailAddress, StringComparison.CurrentCultureIgnoreCase))
                {
                    AddValidationError(new DefaultValidationError("EmailAddress", "The email address is already taken."));
                    return; // -- Login Name = email address so return to avoid duplicate errors.
                }

                // -- Email address taken
                if (users[0].UserName.Equals(entity.UserName, StringComparison.CurrentCultureIgnoreCase))
                {
                    AddValidationError(new DefaultValidationError("UserName", "The user name is already taken."));
                }
            }
        }

        private void Validate_User_Has_Identity(User entity)
        {
            var identityCount = entity.IsTransient()
                ? entity.LoginIdentities.Count
                : _repository.QueryAll().Where(u => u.Id == entity.Id).SelectMany(u => u.LoginIdentities).Count();

            if (string.IsNullOrWhiteSpace(entity.PasswordHash) && identityCount == 0)
            {
                AddValidationError(new DefaultValidationError("PasswordHash", "User must have an identity or external login."));
            }
        }
        #endregion
    }
}
