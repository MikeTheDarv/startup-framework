﻿namespace StartupApp.Services.Validation.Impl
{
    public class DefaultValidator<TEntity> : ValidatorBase<TEntity> where TEntity : class
    {
        #region Implementation of ApplicationValidatorBase
        protected override void ValidateEntity(TEntity entity)
        {
        }
        #endregion
    }
}
