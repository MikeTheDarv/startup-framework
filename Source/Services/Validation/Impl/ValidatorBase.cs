﻿using System;
using Startup.Framework.Validation.Impl;

namespace StartupApp.Services.Validation.Impl
{
    public abstract class ValidatorBase<TEntity> : DataAnnotationsValidatorBase where TEntity : class
    {
        #region Implementation of IValidator
        protected override void ValidateEntity(object entity)
        {
            var actualEntity = entity as TEntity;

            if (actualEntity == null)
            {
                throw new ArgumentNullException("entity");
            }

            ValidateEntity(actualEntity);
        }
        #endregion

        #region Abstract Methods
        protected abstract void ValidateEntity(TEntity entity);
        #endregion
    }
}
