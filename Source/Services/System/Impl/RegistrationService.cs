﻿using System;
using StartupApp.Common.Enums;
using StartupApp.Domain.DataTransferObjects.ServiceObjects;
using StartupApp.Domain.Model;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.System.Interfaces;

namespace StartupApp.Services.System.Impl
{
    public class RegistrationService : IRegistrationService
    {
        #region Private Variables
        private readonly IUserDomainService _userDomainService;
        private readonly IEmailService _emailService;
        private readonly IEmailUrlResolver _emailUrlResolver;
        #endregion

        #region Constructors
        public RegistrationService(IUserDomainService userDomainService, IEmailService emailService, IEmailUrlResolver emailUrlResolver)
        {
            _userDomainService = userDomainService;
            _emailService = emailService;
            _emailUrlResolver = emailUrlResolver;
        }

        #endregion

        #region Implementation of IRegistrationService
        public User Register(User user, string password)
        {
            user.UserType = ApplicationUserType.User;
            
            // -- Create the user
            var persistedUser = _userDomainService.Create(user, password);

            // -- Activation Url paramaters
            var parameters = new
            {
                emailAddress = persistedUser.EmailAddress,
                activationCode = persistedUser.ActivationCode.ToString()
            };

            var activationUrl = _emailUrlResolver.ResolveUrl(EmailUrlType.UserActivation, parameters);

            // -- Kick off welcome email
            _emailService.SendEmails(ApplicationEmailType.UserRegistered, new UserRegisteredEmailDto { ActivationUrl = activationUrl, User = persistedUser });

            return persistedUser;
        }

        public void ForgotPassword(string emailAddress)
        {
            var userId = _userDomainService.SelectWhere(u => u.EmailAddress == emailAddress, u => u.Id);

            if (userId == default(int))
            {
                throw new ArgumentException(string.Format("We couldn't find an account associated with {0}.", emailAddress));
            }

            string newPassword;
            var user = _userDomainService.ResetPassword(userId, out newPassword);
            var emailDto = new UserForgotPasswordEmailDto {User = user, TemporaryPassword = newPassword};

            _emailService.SendEmails(ApplicationEmailType.UserForgotPassword, emailDto);
        }
        #endregion
    }
}
