﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using StartupApp.Common.Enums;
using StartupApp.Domain.DataTransferObjects.ServiceObjects;
using StartupApp.Domain.Model;
using StartupApp.Services.System.Interfaces;
using Startup.Framework.Net;
using Startup.Framework.Net.Impl;
using Startup.Framework.Net.Interfaces;
using Startup.Framework.Persistence.Interfaces;

namespace StartupApp.Services.System.Impl
{
    public class TokenizedEmailService : TokenizedEmailMessageService, IEmailService
    {
        #region Private Variables
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IExceptionHandler _exceptionHandler;
        #endregion

        #region Constructors

        public TokenizedEmailService(ISmtpClient smtpClient,
            IRepository<EmailTemplate> emailTemplateRepository,
            IExceptionHandler exceptionHandler)
            : base(smtpClient)
        {
            _emailTemplateRepository = emailTemplateRepository;
            _exceptionHandler = exceptionHandler;
        }
        #endregion

        #region Implementation of IEmailService
        public bool SendEmails(ApplicationEmailType emailType, object payload)
        {
            var emailTemplates = _emailTemplateRepository.QueryAll().Where(template => template.EmailType == emailType);

            bool allSuccessful = true;
            foreach (var emailTemplate in emailTemplates)
            {
                if (!emailTemplate.IsActive)
                {
                    allSuccessful = false;
                    continue;
                }

                try
                {
                    var emailMessage = emailTemplate.ToTokenizedEmailMessage(payload);

                    MapEmailTokens(emailMessage, payload);

                    allSuccessful = allSuccessful && SendEmail(emailMessage);
                }
                catch (InvalidOperationException ioe)
                {
                    allSuccessful = false;
                    _exceptionHandler.HandleException(ioe);
                }
                catch (SmtpFailedRecipientsException sfre)
                {
                    allSuccessful = false;
                    _exceptionHandler.HandleException(sfre);
                }
                catch (SmtpException se)
                {
                    allSuccessful = false;
                    _exceptionHandler.HandleException(se);
                }
            }

            return allSuccessful;
        }
        #endregion

        #region Private Methods
        private void MapEmailTokens(ITokenizedEmailMessage emailMessage, object payload)
        {
            var @switch = new Dictionary<Type, Action>
                {
                    {typeof(UserForgotPasswordEmailDto), () => MapUserForgotPasswordTokens(emailMessage, (UserForgotPasswordEmailDto)payload)},
                    {typeof(UserRegisteredEmailDto), () => MapUserWelcomeTokens(emailMessage, (UserRegisteredEmailDto)payload)},
                };

            var payloadType = payload.GetType();
            if (@switch.ContainsKey(payloadType))
            {
                @switch[payloadType]();
            }
        }

        private void MapUserForgotPasswordTokens(ITokenizedEmailMessage emailMessage, UserForgotPasswordEmailDto payload)
        {
            var userTokens = MailUtility.GetEmailMessageTokensFromAttributes(payload.User);

            foreach (string token in userTokens)
            {
                emailMessage.MapToken(token, userTokens[token]);
            }
        }

        private void MapUserWelcomeTokens(ITokenizedEmailMessage emailMessage, UserRegisteredEmailDto payload)
        {
            var userTokens = MailUtility.GetEmailMessageTokensFromAttributes(payload.User);

            foreach (string token in userTokens)
            {
                emailMessage.MapToken(token, userTokens[token]);
            }
        }
        #endregion
    }
}
