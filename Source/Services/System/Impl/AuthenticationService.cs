﻿using System.Linq;
using System.Security.Claims;
using StartupApp.Common.Exceptions;
using StartupApp.Security.Authentication.Interfaces;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.System.Interfaces;

namespace StartupApp.Services.System.Impl
{
    public class AuthenticationService : IAuthenticationService
    {
        #region Private Variables
        private readonly IUserDomainService _userDomainService;
        private readonly IAuthenticationManager _authenticationManager;
        #endregion

        #region Constructors
        public AuthenticationService(IUserDomainService userDomainService, IAuthenticationManager authenticationManager)
        {
            _userDomainService = userDomainService;
            _authenticationManager = authenticationManager;
        }

        #endregion

        #region Implementation of IAuthenticationService
        public ClaimsIdentity Authenticate(string loginName, string password, bool persist)
        {
            var users =
                _userDomainService.SelectAllWhere(u => u.UserName == loginName || u.EmailAddress == loginName, u => u).ToList();

            if (users.Count > 1)
            {
                throw new LoginException("An error occurred while authenticating.");
            }

            if (users.Count == 0)
            {
                throw new LoginException("The email address or password you entered is incorrect.");
            }

            var user = users[0];

            if (!_userDomainService.VerifyPassword(user, password))
            {
                throw new LoginException("The email address or password you entered is incorrect.");
            }

            if (!user.IsActive)
            {
                throw new LoginException("The email address provided has been deactivated, and cannot be logged in.");
            }

            var principal = _authenticationManager.PersistPrincipal(user, persist);

            return principal;
        }

        public ClaimsIdentity AuthenticateExternal(string loginProvider, string providerKey, bool persist)
        {
            var user =
                _userDomainService.SelectWhere(u => u.LoginIdentities.Any(identity =>
                    identity.LoginProvider == loginProvider &&
                    identity.ProviderKey == providerKey), u => u);

            if (user == null)
            {
                throw new LoginException("Could not identify user from login provider and provider key.");
            }

            if (!user.IsActive)
            {
                throw new LoginException("The email address provided has been deactivated, and cannot be logged in.");
            }

            var principal = _authenticationManager.PersistPrincipal(user, persist);

            return principal;
        }

        public void Invalidate()
        {
            _authenticationManager.RevokePrincipal();
        }
        #endregion
    }
}
