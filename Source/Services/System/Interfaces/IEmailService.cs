﻿using StartupApp.Common.Enums;

namespace StartupApp.Services.System.Interfaces
{
    public interface IEmailService
    {
        bool SendEmails(ApplicationEmailType emailType, object payload);
    }
}
