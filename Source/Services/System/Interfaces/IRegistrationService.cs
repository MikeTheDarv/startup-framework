﻿using StartupApp.Domain.Model;

namespace StartupApp.Services.System.Interfaces
{
    public interface IRegistrationService
    {
        User Register(User user, string password);
        void ForgotPassword(string emailAddress);
    }
}
