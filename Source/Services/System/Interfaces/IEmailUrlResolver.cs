﻿using StartupApp.Common.Enums;

namespace StartupApp.Services.System.Interfaces
{
    public interface IEmailUrlResolver
    {
        string ResolveUrl(EmailUrlType urlType, object routeValues = null);
    }
}
