﻿using System.Security.Claims;

namespace StartupApp.Services.System.Interfaces
{
    public interface IAuthenticationService
    {
        ClaimsIdentity Authenticate(string loginName, string password, bool persist);
        ClaimsIdentity AuthenticateExternal(string loginProvider, string providerKey, bool persist);
        void Invalidate();
    }
}
