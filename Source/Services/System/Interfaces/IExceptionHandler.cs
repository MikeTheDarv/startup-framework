﻿using System;

namespace StartupApp.Services.System.Interfaces
{
    public interface IExceptionHandler
    {
        void HandleException(Exception exception);
        void HandleException(Exception exception, bool useBaseException);
    }
}
