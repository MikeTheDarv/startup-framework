﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using StartupApp.Common.Exceptions;
using StartupApp.Common.Utilities;
using StartupApp.Domain.Model;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.Validation.Impl;
using Startup.Framework.Persistence.Interfaces;

namespace StartupApp.Services.Domain.Impl
{
    public class UserDomainService : ModelDomainServiceBase<User>, IUserDomainService
    {
        #region Private Variables
        private const int SALT_SIZE_IN_BYTES = 20;
        private const int RANDOM_PASSWORD_LENGTH = 10;
        private readonly IModelDomainService<UserLoginIdentity> _userLoginIdentityDomainService;
        #endregion

        #region Constructors

        public UserDomainService(IRepository<User> repository,
            ValidatorBase<User> validator,
            IModelDomainService<UserLoginIdentity> userLoginIdentityDomainService)
            : base(repository, validator)
        {
            _userLoginIdentityDomainService = userLoginIdentityDomainService;
        }

        #endregion

        #region Implementation of IUserDomainService
        public User Create(User user, string password)
        {
            if (!user.IsTransient())
            {
                throw new ArgumentException("Only transient user's can be passed to the create method.", "user");
            }

            SetupTransientUser(user, password);

            user = base.Save(user);

            return user;
        }

        public bool VerifyPassword(User user, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                return false;
            }

            string hashedPassword = CreatePasswordHash(password, user.PasswordSalt);
            return hashedPassword == user.PasswordHash;
        }

        public User ResetPassword(int userId, out string newPassword)
        {
            var user = Select(userId, u => u);

            if (user == null)
            {
                throw new ArgumentException(string.Format("Could not find user with id #{0}", userId), "userId");
            }

            user.IsPendingPasswordReset = true;

            newPassword = GenerateRandomPassword();

            CreatePasswordCredentials(user, newPassword);

            return base.Save(user);
        }

        public User ChangePassword(int userId, string oldPassword, string newPassword)
        {
            var user = Select(userId, u => u);

            if (user == null)
            {
                throw new ArgumentException(string.Format("Could not find user with id #{0}", userId), "userId");
            }

            // -- Password hash may be null if user has logged in via
            //    an external provider.  This method allows the password
            //    to be set or updated if logged in via Local Auth.
            if (!string.IsNullOrWhiteSpace(user.PasswordHash) && !VerifyPassword(user, oldPassword))
            {
                throw new ChangePasswordException("Provided current password was incorrect.");
            }

            user.IsPendingPasswordReset = false;

            CreatePasswordCredentials(user, newPassword);

            return base.Save(user);
        }

        public UserLoginIdentity AddLoginIdentity(int userId, UserLoginIdentity userIdentity)
        {
            userIdentity.User = Select(userId, u => u);
            return _userLoginIdentityDomainService.Save(userIdentity);
        }

        public void RemoveIdentityLogin(int userId, string loginProvider, string providerKey)
        {
            var userIdentity =
                _userLoginIdentityDomainService.SelectWhere(identity => identity.LoginProvider == loginProvider &&
                                                                        identity.ProviderKey == providerKey &&
                                                                        identity.User.Id == userId, identity => identity);

            if (userIdentity != null)
            {
                _userLoginIdentityDomainService.Delete(userIdentity);
            }
        }

        public bool Activate(int userId, Guid activationCode)
        {
            var user = Select(userId, u => u);

            if (user == null)
            {
                throw new ArgumentException(string.Format("Could not find user with id #{0}", userId), "userId");
            }

            if (activationCode == user.ActivationCode)
            {
                user.IsActivated = true;

                base.Save(user);

                return true;
            }

            return false;
        }

        #endregion

        #region Overrides
        public override User Save(User user)
        {
            if (user.IsTransient())
            {
                throw new ArgumentException("Use the 'Create' method for transient uesrs.", "user");
            }

            return base.Save(user);
        }
        #endregion

        #region Private Methods
        private void SetupTransientUser(User user, string password)
        {
            // -- Do not hash the password if blank, otherwise
            //    model validation will not kick off the 'required'
            //    validation exception.
            if (!string.IsNullOrWhiteSpace(password))
            {
                CreatePasswordCredentials(user, password);
            }

            user.IsActive = true;
            user.ActivationCode = Guid.NewGuid();
        }

        private void CreatePasswordCredentials(User user, string password)
        {
            user.PasswordSalt = CreatePasswordSalt();
            user.PasswordHash = CreatePasswordHash(password, user.PasswordSalt);
        }
        
        private static string CreatePasswordSalt()
        {
            RNGCryptoServiceProvider cryptoService = new RNGCryptoServiceProvider();
            byte[] buff = new byte[SALT_SIZE_IN_BYTES];
            cryptoService.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        private static string CreatePasswordHash(string password, string salt)
        {
            string saltedPassword = string.Concat(password, salt);
            string hashedPassword = string.Concat(HashPasswordForStoringInConfigFile(saltedPassword), salt);
            return hashedPassword;
        }

        private static string HashPasswordForStoringInConfigFile(string password)
        {
            // TODO: Update hashing method!
            return HashUtility.HashFormsAuthentication(password);
        }

        private static string GenerateRandomPassword()
        {
            const string ALLOWED_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            const int BYTE_SIZE = 0x100;

            var allowedCharSet = new HashSet<char>(ALLOWED_CHARS).ToArray();

            // Guid.NewGuid and System.Random are not particularly random. By using a
            // cryptographically-secure random number generator, the caller is always
            // protected, regardless of use.
            using (var rng = new RNGCryptoServiceProvider())
            {
                var result = new StringBuilder();
                var buf = new byte[128];

                while (result.Length < RANDOM_PASSWORD_LENGTH)
                {
                    rng.GetBytes(buf);
                    for (var i = 0; i < buf.Length && result.Length < RANDOM_PASSWORD_LENGTH; ++i)
                    {
                        // Divide the byte into allowedCharSet-sized groups. If the
                        // random value falls into the last group and the last group is
                        // too small to choose from the entire allowedCharSet, ignore
                        // the value in order to avoid biasing the result.
                        var outOfRangeStart = BYTE_SIZE - (BYTE_SIZE % allowedCharSet.Length);
                        if (outOfRangeStart <= buf[i]) continue;
                        result.Append(allowedCharSet[buf[i] % allowedCharSet.Length]);
                    }
                }
                return result.ToString().ToUpper();
            }
        }
        #endregion
    }
}
