﻿using Startup.Framework.Persistence.Interfaces;
using StartupApp.Domain.Model;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.Validation.Impl;

namespace StartupApp.Services.Domain.Impl
{
    public class DefaultDomainService<TEntity> : ModelDomainServiceBase<TEntity> where TEntity : class, IModelBase<int>
    {
        #region Constructors
        public DefaultDomainService(IRepository<TEntity> repository, ValidatorBase<TEntity> validator)
            : base(repository, validator)
        {
        }
        #endregion
    }
}
