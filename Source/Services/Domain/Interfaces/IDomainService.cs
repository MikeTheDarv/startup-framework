﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace StartupApp.Services.Domain.Interfaces
{
    public interface IDomainService<TEntity, in TPrimaryKey>
    {
        TDto Select<TDto>(TPrimaryKey id, Expression<Func<TEntity, TDto>> transformer);
        TDto SelectWhere<TDto>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TDto>> transformer);
        TEntity Proxy(TPrimaryKey id);
        IEnumerable<TDto> SelectAll<TDto>(Expression<Func<TEntity, TDto>> transformer);
        IEnumerable<TDto> SelectAllWhere<TDto>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TDto>> transformer);
        TEntity Save(TEntity entity);
        void Delete(TEntity entity);
        int Count(Expression<Func<TEntity, bool>> predicate);
        bool Exists(Expression<Func<TEntity, bool>> predicate);
    }

    public interface IModelDomainService<TEntity> : IDomainService<TEntity, int>
    {
    }
}
