﻿using System;
using StartupApp.Domain.Model;

namespace StartupApp.Services.Domain.Interfaces
{
    public interface IUserDomainService : IDomainService<User, int>
    {
        User Create(User user, string password);
        bool VerifyPassword(User user, string password);
        User ResetPassword(int userId, out string newPassword);
        User ChangePassword(int userId, string oldPassword, string newPassword);
        UserLoginIdentity AddLoginIdentity(int userId, UserLoginIdentity userIdentity);
        void RemoveIdentityLogin(int userId, string loginProvider, string providerKey);
        bool Activate(int userId, Guid activationCode);
    }
}
