﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Startup.Framework.Persistence.Interfaces;
using Startup.Framework.Validation;
using StartupApp.Domain.Model;
using StartupApp.Services.Validation.Impl;

namespace StartupApp.Services.Domain.Interfaces
{
    public abstract class ModelDomainServiceBase<TEntity> : IModelDomainService<TEntity>
        where TEntity : class, IModelBase<int>
    {
        #region Properties
        internal IRepository<TEntity> Repository { get; private set; }
        internal ValidatorBase<TEntity> Validator { get; private set; }
        #endregion

        #region Constructors
        protected ModelDomainServiceBase(IRepository<TEntity> repository,
                                         ValidatorBase<TEntity> validator)
        {
            Validator = validator;
            Repository = repository;
        }
        #endregion

        #region Implementation of IDomainService<TEntity>
        public virtual TDto Select<TDto>(int id, Expression<Func<TEntity, TDto>> transformer)
        {
            return Repository.QueryAll().Where(entity => entity.Id == id).Select(transformer).FirstOrDefault();
        }

        public TDto SelectWhere<TDto>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TDto>> transformer)
        {
            return Repository.QueryAll().Where(predicate).Select(transformer).FirstOrDefault();
        }

        public virtual TEntity Proxy(int id)
        {
            throw new NotImplementedException("EntityFramework does not have a 'Proxy' equivalent.");
        }

        public virtual IEnumerable<TDto> SelectAll<TDto>(Expression<Func<TEntity, TDto>> transformer)
        {
            return Repository.QueryAll().Select(transformer);
        }

        public IEnumerable<TDto> SelectAllWhere<TDto>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TDto>> transformer)
        {
            return Repository.QueryAll().Where(predicate).Select(transformer);
        }

        public virtual TEntity Save(TEntity entity)
        {
            if (!Validator.Validate(entity))
            {
                throw new ValidationException(Validator.ValidationErrors);
            }

            return Repository.SaveOrUpdate(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            Repository.Delete(entity);
        }

        public virtual int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.QueryAll().Count(predicate);
        }

        public virtual bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            return Count(predicate) > 0;
        }
        #endregion
    }
}
