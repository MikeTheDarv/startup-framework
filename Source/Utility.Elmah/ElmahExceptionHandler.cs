﻿using System;
using StartupApp.Services.System.Interfaces;

namespace StartupApp.Utility.Elmah
{
    public class ElmahExceptionHandler : IExceptionHandler
    {
        #region Implemenation of IExceptionHandler
        public void HandleException(Exception exception)
        {
            HandleException(exception, true);
        }

        public void HandleException(Exception exception, bool useBaseException)
        {
            if (useBaseException)
            {
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                }
            }
            
            global::Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
        }
        #endregion
    }
}
