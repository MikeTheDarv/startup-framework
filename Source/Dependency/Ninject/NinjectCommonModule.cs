﻿using System;
using System.Data.Entity;
using Ninject.Parameters;
using Ninject.Web.Common;
using StartupApp.Data;
using StartupApp.Data.Impl;
using StartupApp.Data.Interfaces;
using StartupApp.Dependency.Ninject.Impl;
using StartupApp.Domain.Model;
using StartupApp.Services.Domain.Impl;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.System.Impl;
using StartupApp.Services.System.Interfaces;
using StartupApp.Services.Validation.Impl;
using Ninject;
using Ninject.Modules;
using Startup.Framework.Dependency.Interfaces;
using Startup.Framework.Impl.EntityFramework.Persistence;
using Startup.Framework.Net.Impl;
using Startup.Framework.Net.Interfaces;
using Startup.Framework.Persistence.Interfaces;
using Startup.Framework.Validation.Impl;
using Startup.Framework.Validation.Interfaces;
using StartupApp.Utility.Elmah;

namespace StartupApp.Dependency.Ninject
{
    public class NinjectCommonModule : NinjectModule
    {
        #region Overrides of NinjectModule
        public override void Load()
        {
            /* Entity Framework */
            Kernel.Bind<IDataConfigurationProvider>().To<EfDataConfigurationProvider>();
            Kernel.Bind<DbContext>().To<StartupAppDbContext>().InRequestScope();
            Kernel.Bind<IUnitOfWork>().To<EfUnitOfWork>();
            // Kernel.Bind<IRevisionListener>().To<NhModelRevisionListener>();

            /* Application */
            Kernel.Bind<IDependencyServiceLocator>().To<NinjectServiceLocator>()
                                                    .WithParameter(new ConstructorArgument("kernel", Kernel));

            /* Repositories */
            Kernel.Bind(typeof(IRepository<>)).To(typeof(EfRepository<>)).InRequestScope();

            /* Validation */
            Func<Type, IValidator> validatorFactory = type => (IValidator)Kernel.Get(typeof(ValidatorBase<>).MakeGenericType(type));
            Kernel.Bind<IValidationProvider>().ToConstant(new DefaultFactoryValidationProvider(validatorFactory));
            Kernel.Bind(typeof(ValidatorBase<>)).To(typeof(DefaultValidator<>));      // --- Default Validator
            Kernel.Bind<ValidatorBase<User>>().To<UserValidator>();
            Kernel.Bind<ValidatorBase<UserLoginIdentity>>().To<UserLoginIdentityValidator>();

            /* Framework */
            Kernel.Bind<ISmtpClient>().To<DefaultSmtpClient>();
            Kernel.Bind<IEmailMessage>().To<DefaultEmailMessage>();
            Kernel.Bind<ITokenizedEmailMessage>().To<TokenizedEmailMessage>();
            Kernel.Bind<IExceptionHandler>().To<ElmahExceptionHandler>();
            
            /* Services */
            Kernel.Bind<IEmailMessageService>().To<DefaultEmailMessageService>();
            Kernel.Bind<ITokenizedEmailMessageService>().To<TokenizedEmailMessageService>();

            /* -- Domain -- */
            // Kernel.Bind<IDomainIdentifierLookupService>().To<DomainIdentifierLookupService>();
            Kernel.Bind(typeof(IModelDomainService<>)).To(typeof(DefaultDomainService<>));    // --- Default Service
            Kernel.Bind<IUserDomainService>().To<UserDomainService>();

            /* Application */
            Kernel.Bind<IEmailService>().To<TokenizedEmailService>();
            Kernel.Bind<IRegistrationService>().To<RegistrationService>();
            Kernel.Bind<IAuthenticationService>().To<AuthenticationService>();

            /* Media Services */
            // -- Must call this service in a factory method because custom
            //    configuration sections cannot be accessed in the application
            //    "pre-start" event.
            //  Func<IContext, IMediaStorageService> mediaStorageServiceFactory = context => new AmazonS3MediaStorageService(new AmazonS3ConfigurationSection("amazonS3StorageConfig"));
            //  Func<IContext, IMediaStorageService> mediaStorageServiceFactory = context => new FileSystemMediaStorageService(new FileSystemStorageConfigurationSection("fileSystemStorageConfig"));
            //Func<IContext, IMediaStorageService> mediaStorageServiceFactory = context => new AzureBlobMediaStorageService(new AzureBlobConfigurationSection("azureBlobStorageConfig"));
            //Kernel.Bind<IMediaStorageService>().ToMethod(mediaStorageServiceFactory);
        }
        #endregion
    }
}
