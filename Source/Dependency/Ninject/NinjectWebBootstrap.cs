using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Mvc;
using System.Web.Routing;
using StartupApp.Dependency.Ninject.Impl;
using StartupApp.Security.Authentication.Impl;
using StartupApp.Security.Authentication.Interfaces;
using StartupApp.Security.Identity.Interfaces;
using StartupApp.Services.System.Interfaces;
using StartupApp.Utility.WebRoles.HttpModules;
using StartupApp.Utility.WebRoles.Impl.Security.Identity;
using StartupApp.Utility.WebRoles.Impl.Services.System;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Selection.Heuristics;
using Ninject.Web.Common;
using IDependencyResolver = System.Web.Http.Dependencies.IDependencyResolver;

namespace StartupApp.Dependency.Ninject
{
    public static class NinjectWebBootstrap
    {
        #region Private Variables
        private static readonly Bootstrapper _bootstrapper = new Bootstrapper();
        private static Action<IKernel> _registerHandler;
        #endregion

        #region Public Methods
        public static void Start()
        {
            Start(null);
        }

        public static void Start(Action<IKernel> registerHandler)
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));

            _registerHandler = registerHandler;
            _bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            _bootstrapper.ShutDown();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Components.Add<IInjectionHeuristic, NinjectDependencyInjectionAttributeHeuristic>();

            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            // -- For MVC
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory(kernel));

            // -- For Web API
            GlobalConfiguration.Configuration.DependencyResolver = new NinjectWebApiResolver(kernel);

            // -- Register global dependencies
            RegisterServices(kernel);

            // -- Register specific dependencies
            if (_registerHandler != null)
            {
                // Callback for web specific bootstrap
                _registerHandler(kernel);
            }

            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            /* Http Modules */
            kernel.Bind<IHttpModule>().To<EfProfHttpModule>();
            kernel.Bind<IHttpModule>().To<SystemIdentityHttpModule>();
            
            /* Web related only */
            kernel.Bind<IAuthenticationManager>().To<OwinAuthenticationManager>();
            kernel.Bind<ISystemIdentityStore>().To<HttpCacheSystemIdentityStore>();
            kernel.Bind<ISystemIdentityManager>().To<HttpContextSystemIdentityManager>();
            kernel.Bind<IEmailUrlResolver>().To<HttpContextEmailUrlResolver>();

            /* Common Bindings */
            kernel.Load<NinjectCommonModule>();
        }
        #endregion

        #region Nested Class: NinjectControllerFactory
        public class NinjectControllerFactory : DefaultControllerFactory
        {
            #region Private Variables
            private readonly IKernel _ninjectKernel;
            #endregion

            #region Constructors
            public NinjectControllerFactory(IKernel ninjectKernel)
            {
                _ninjectKernel = ninjectKernel;
            }
            #endregion

            #region Overrides
            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {
                return (controllerType == null)
                    ? base.GetControllerInstance(requestContext, null)
                    : (IController) _ninjectKernel.Get(controllerType);
            }
            #endregion
        }
        #endregion

        #region Nested Class: NinjectWebApiResolver
        public class NinjectWebApiResolver : IDependencyResolver
        {
            #region Private Variables
            private readonly IKernel _kernel;
            #endregion

            #region Constructors
            public NinjectWebApiResolver(IKernel kernel)
            {
                _kernel = kernel;
            }
            #endregion

            #region Implementation of IDependencyResolver
            public IDependencyScope BeginScope()
            {
                return this;
            }
            #endregion

            #region Implementation of IDependencyScope
            public void Dispose()
            {
            }

            public object GetService(Type serviceType)
            {
                try
                {
                    return _kernel.Get(serviceType);
                }
                catch (ActivationException)
                {
                    return null;
                }
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                try
                {
                    return _kernel.GetAll(serviceType);
                }
                catch (ActivationException)
                {
                    return new object[0];
                }
            }
            #endregion
        }
        #endregion
    }
}
