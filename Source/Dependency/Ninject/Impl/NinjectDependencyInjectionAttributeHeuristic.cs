﻿using System.Reflection;
using Ninject.Components;
using Ninject.Selection.Heuristics;
using Startup.Framework.Dependency;

namespace StartupApp.Dependency.Ninject.Impl
{
    public class NinjectDependencyInjectionAttributeHeuristic : NinjectComponent, IInjectionHeuristic
    {
        public bool ShouldInject(MemberInfo member)
        {
            return member.IsDefined(typeof(InjectDependencyAttribute), true);
        }
    }

}
