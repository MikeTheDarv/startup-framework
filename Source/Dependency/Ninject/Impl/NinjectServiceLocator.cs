﻿using System;
using System.Collections.Generic;
using Ninject;
using Startup.Framework.Dependency.Interfaces;

namespace StartupApp.Dependency.Ninject.Impl
{
    public class NinjectServiceLocator : IDependencyServiceLocator
    {
        #region Private Variables
        private readonly IKernel _kernel;
        #endregion

        #region Constructors
        public NinjectServiceLocator(IKernel kernel)
        {
            _kernel = kernel;
        }
        #endregion

        #region Implementation of IDependencyServiceLocator
        public object GetInstance(Type serviceType)
        {
            return _kernel.Get(serviceType);
        }

        public object GetInstance(Type serviceType, string key)
        {
            if (key == null)
            {
                return GetInstance(serviceType);
            }

            return _kernel.Get(serviceType, key);
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        public TService GetInstance<TService>()
        {
            return _kernel.Get<TService>();
        }

        public TService GetInstance<TService>(string key)
        {
            if (key == null)
            {
                return _kernel.Get<TService>();
            }

            return _kernel.Get<TService>(key);
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            return _kernel.GetAll<TService>();
        }
        #endregion
    }
}
