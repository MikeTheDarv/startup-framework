﻿using StartupApp.Data.Interfaces;

namespace StartupApp.Data.Impl
{
    public class EfDataConfigurationProvider : IDataConfigurationProvider
    {
        #region Implementation of IDataConfigurationProvider
        public object Configure(string connectionString, string sessionName, bool debug)
        {
            return new StartupAppDbContext(connectionString);
        }
        #endregion
    }
}
