using StartupApp.Common.Enums;
using StartupApp.Domain.Model;

namespace StartupApp.Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<StartupAppDbContext>
    {
        #region Constructors
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        #endregion

        #region Overrides
        protected override void Seed(StartupAppDbContext context)
        {
            // -- Initial User Data
            SeedUserData(context);

            // -- Initial Email Template Data
            SeedEmailTemplateData(context);
        }
        #endregion

        #region Private Methods
        private static void SeedUserData(StartupAppDbContext context)
        {
            var guestUser = new User
            {
                FirstName = "Anonymous",
                LastName = "User",
                EmailAddress = "guest@app.co",
                PasswordHash = "ABC123",
                PasswordSalt = "ABC123",
                IsSystemProtected = true,
                UserType = ApplicationUserType.Null
            };

            var adminUser = new User
            {
                FirstName = "Super",
                LastName = "Admin",
                EmailAddress = "admin@app.co",
                PasswordHash = "ABC123",
                PasswordSalt = "ABC123",
                IsSystemProtected = true,
                UserType = ApplicationUserType.SystemAdministrator
            };

            // -- User seed data has been moved to migration "InsertSystemUsers"
            //    because Entity Framework won't allow us to set the primary key.
            //    Instead, we execute SQL scripts.
            // context.Users.AddOrUpdate(user => user.EmailAddress, guestUser, adminUser);
        }

        private static void SeedEmailTemplateData(StartupAppDbContext context)
        {
            var welcomeEmail = new EmailTemplate
            {
                Name = "User Welcome Email",
                Description = "Welcome email for users who complete registration.",
                MailTo = "{EmailAddress}",
                MailBodyHtml =
                    "<p>Hi {FirstName},</p><p>Thank you for registering! Here is your registration information. </p><p><strong>Username: </strong>{EmailAddress}</p><p>Please confirm your email address by clicking <a href=\"{ActivationUrl}\">here</a>.</p><p>Thank you for your interest and support,<br />The Team</p>",
                MailBodyText =
                    "Hi {FirstName},\n\nThank you for registering! Here is your registration information.\n\nUsername: {EmailAddress}\n\nPlease confirm your email address by navigating to {ActivationUrl}.\n\nThank you for your interest and support,\nThe Team",
                MailFrom = "App <hi@app.com>",
                MailSubject = "Welcome!  Thank you for registering",
                EmailType = ApplicationEmailType.UserRegistered
            };

            var forgotPasswordEmail = new EmailTemplate
            {
                Name = "User Forgot Password Email",
                Description = "A user has requested their password is reset.  A temporary password will be assigned which they can use to then log in and create a new password.",
                MailTo = "{EmailAddress}",
                MailBodyHtml =
                    "<p>Hi {FirstName},</p><p>Here is the temporary password you have requested.&nbsp; Once you log in, you will be prompted to change your password.</p><p><strong>Temporary Password: </strong>{TemporaryPassword}</p><p>If you did not request this please contact Support.</p><p>Thank you,<br />The Team</p>",
                MailBodyText =
                    "Hi {FirstName},\n\nHere is the temporary password you have requested. Once you log in, you will be prompted to change your password.\n\nTemporary Password: {TemporaryPassword}\n\nIf you did not request this please contact Support.\n\nThank you,\nThe Team",
                MailFrom = "App <hi@app.com>",
                MailSubject = "App - Your temporary password",
                EmailType = ApplicationEmailType.UserForgotPassword
            };

            context.EmailTemplates.AddOrUpdate(email => email.Name, welcomeEmail, forgotPasswordEmail);
        }
        #endregion
    }
}
