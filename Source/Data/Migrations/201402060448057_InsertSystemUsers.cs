using StartupApp.Common.Constants;

namespace StartupApp.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InsertSystemUsers : DbMigration
    {
        public override void Up()
        {
            // -- Inserts the system users
            //    Anonymous + Super Admin
            const string SQL = "SET IDENTITY_INSERT [dbo].[Users] ON; " +
                               "INSERT INTO [dbo].[Users] ([Id], [UserName], [PasswordHash], [PasswordSalt], [FirstName], [LastName], [EmailAddress], [IsActive], [IsActivated], [ActivationCode], [IsSystemProtected], [IsPendingPasswordReset], [UserType], [AvatarFileIdentifier], [TimeZoneId], [Version], [CreatedUtc], [CreatedBy], [ModifiedLastUtc], [ModifiedLastBy]) VALUES ({0}, N'guest@app.co', N'ABC123', N'ABC123', N'Anonymous', N'User', N'guest@app.co', 0, 0, N'00000000-0000-0000-0000-000000000000', 1, 0, 0, N'', N'Pacific Standard Time', 0, N'2014-02-06 04:45:57', -999, N'2014-02-06 04:45:57', -999); " +
                               "INSERT INTO [dbo].[Users] ([Id], [UserName], [PasswordHash], [PasswordSalt], [FirstName], [LastName], [EmailAddress], [IsActive], [IsActivated], [ActivationCode], [IsSystemProtected], [IsPendingPasswordReset], [UserType], [AvatarFileIdentifier], [TimeZoneId], [Version], [CreatedUtc], [CreatedBy], [ModifiedLastUtc], [ModifiedLastBy]) VALUES ({1}, N'admin@app.co', N'ABC123', N'ABC123', N'Super', N'Admin', N'admin@app.co', 0, 0, N'00000000-0000-0000-0000-000000000000', 1, 0, -999, N'', N'Pacific Standard Time', 0, N'2014-02-06 04:45:57', -999, N'2014-02-06 04:45:57', -999); " +
                               "SET IDENTITY_INSERT [dbo].[Users] OFF";

            Sql(string.Format(SQL, UserConstants.ANONYMOUS_USER_ID, UserConstants.SYSTEM_ADMIN_USER_ID));
        }

        public override void Down()
        {
        }
    }
}
