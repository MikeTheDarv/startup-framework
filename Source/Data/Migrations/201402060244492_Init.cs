namespace StartupApp.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Init : DbMigration
    {
        public override void Up()
        {
            // -- Users
            CreateTable(
                "dbo.Users",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserName = c.String(nullable: false, maxLength: 100),
                    PasswordHash = c.String(nullable: false, maxLength: 512),
                    PasswordSalt = c.String(nullable: false, maxLength: 128),
                    FirstName = c.String(nullable: false, maxLength: 50),
                    LastName = c.String(nullable: false, maxLength: 50),
                    EmailAddress = c.String(nullable: false, maxLength: 100),
                    IsActive = c.Boolean(nullable: false),
                    IsActivated = c.Boolean(nullable: false),
                    ActivationCode = c.Guid(nullable: false),
                    IsSystemProtected = c.Boolean(nullable: false),
                    IsPendingPasswordReset = c.Boolean(nullable: false),
                    UserType = c.Int(nullable: false),
                    AvatarFileIdentifier = c.String(nullable: false, maxLength: 100),
                    TimeZoneId = c.String(nullable: false, maxLength: 100),
                    Version = c.Int(nullable: false),
                    CreatedUtc = c.DateTime(nullable: false),
                    CreatedBy = c.Int(nullable: false),
                    ModifiedLastUtc = c.DateTime(nullable: false),
                    ModifiedLastBy = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true)
                .Index(t => t.EmailAddress, unique: true);

            // -- User Identities
            CreateTable(
                "dbo.UserLoginIdentities",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    LoginProvider = c.String(nullable: false, maxLength: 50),
                    ProviderKey = c.String(nullable: false, maxLength: 256),
                    Version = c.Int(nullable: false),
                    CreatedUtc = c.DateTime(nullable: false),
                    CreatedBy = c.Int(nullable: false),
                    ModifiedLastUtc = c.DateTime(nullable: false),
                    ModifiedLastBy = c.Int(nullable: false),
                    User_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id)
                .Index(t => new { t.LoginProvider, t.ProviderKey }, unique: true);

            // -- Email Templates
            CreateTable(
                "dbo.EmailTemplates",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 200),
                    Description = c.String(nullable: false, maxLength: 500),
                    BccList = c.String(nullable: false, maxLength: 1000),
                    CcList = c.String(nullable: false, maxLength: 1000),
                    IsBodyHtml = c.Boolean(nullable: false),
                    MailBodyHtml = c.String(nullable: false),
                    MailBodyText = c.String(nullable: false),
                    MailFrom = c.String(nullable: false, maxLength: 1000),
                    ReplyTo = c.String(nullable: false, maxLength: 1000),
                    MailPriority = c.Int(nullable: false),
                    MailSubject = c.String(nullable: false, maxLength: 250),
                    MailTo = c.String(nullable: false, maxLength: 1000),
                    IsActive = c.Boolean(nullable: false),
                    EmailType = c.Int(nullable: false),
                    Version = c.Int(nullable: false),
                    CreatedUtc = c.DateTime(nullable: false),
                    CreatedBy = c.Int(nullable: false),
                    ModifiedLastUtc = c.DateTime(nullable: false),
                    ModifiedLastBy = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.UserLoginIdentities", "User_Id", "dbo.Users");

            DropIndex("dbo.Users", new[] { "UserName" });
            DropIndex("dbo.Users", new[] { "EmailAddress" });

            DropIndex("dbo.UserLoginIdentities", new[] { "User_Id" });
            DropIndex("dbo.UserLoginIdentities", new[] { "LoginProvider", "ProviderKey" });

            DropTable("dbo.UserLoginIdentities");
            DropTable("dbo.Users");
            DropTable("dbo.EmailTemplates");
        }
    }
}
