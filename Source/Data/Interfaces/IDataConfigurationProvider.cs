﻿namespace StartupApp.Data.Interfaces
{
    public interface IDataConfigurationProvider
    {
        object Configure(string connectionString, string sessionName, bool debug);
    }
}
