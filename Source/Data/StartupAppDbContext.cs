﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using StartupApp.Common.Constants;
using StartupApp.Domain.Model;
using StartupApp.Security.Identity.Interfaces;
using Startup.Framework.Dependency;
using Startup.Framework.Impl.EntityFramework.Auditing;
using Startup.Framework.Logging.Impl;
using Startup.Framework.Logging.Interfaces;

namespace StartupApp.Data
{
    public class StartupAppDbContext : AuditableDbContext<int>
    {
        #region Data Sets
        public DbSet<User> Users { get; set; }

        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        #endregion

        [InjectDependency]
        public ISystemIdentityStore IdentityStore { get; set; }

        #region Constructors
        public StartupAppDbContext(string connectionString) : base(connectionString)
        {
#if DEBUG
            // -- SQL trace debugging
            DbInterception.Add(new TraceLoggingDbInterceptor());
#endif
        }
        public StartupAppDbContext() : this("StartupAppDb")
        {
        }
        #endregion

        #region Overrides
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // -- Ignore Model Properties
            modelBuilder.Entity<User>().Ignore(user => user.TimeZone);
        }

        public override int GetCurrentIdentity()
        {
            /* -- Is this a hack?  Cannot inject our
             *    SystemIdentityManager because it requires
             *    Repository access to load our cached Identity.
             *    Instead, try to load using self context?
             */

            if (IdentityStore == null)
            {
                return UserConstants.ANONYMOUS_USER_ID;
            }

            string userNameOrEmail = IdentityStore.GetIdentity().EmailAddress;
            var userId = Users.Where(u => u.UserName == userNameOrEmail || u.EmailAddress == userNameOrEmail)
                .Select(u => u.Id)
                .FirstOrDefault();

            return userId == default(int) ? UserConstants.ANONYMOUS_USER_ID : userId;
        }
        #endregion
    }

    public class TraceLoggingDbInterceptor : DbCommandInterceptor
    {
        #region Private Variables
        private readonly ILogger _logger = new TraceLogger();
        private readonly Stopwatch _stopwatch = new Stopwatch();
        #endregion

        #region Overrides 
        public override void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            base.ScalarExecuting(command, interceptionContext);
            _stopwatch.Restart();
        }

        public override void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            _stopwatch.Stop();
            if (interceptionContext.Exception != null)
            {
                _logger.Error(interceptionContext.Exception, "Error executing command: {0}", command.CommandText);
            }
            else
            {
                _logger.TraceApi("SQL Database", "StartupAppDbInterceptor.ScalarExecuted", _stopwatch.Elapsed, "Command: {0}: ", command.CommandText);
            }
            base.ScalarExecuted(command, interceptionContext);
        }

        public override void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            base.NonQueryExecuting(command, interceptionContext);
            _stopwatch.Restart();
        }

        public override void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            _stopwatch.Stop();
            if (interceptionContext.Exception != null)
            {
                _logger.Error(interceptionContext.Exception, "Error executing command: {0}", command.CommandText);
            }
            else
            {
                _logger.TraceApi("SQL Database", "StartupAppDbInterceptor.NonQueryExecuted", _stopwatch.Elapsed, "Command: {0}: ", command.CommandText);
            }
            base.NonQueryExecuted(command, interceptionContext);
        }
        
        public override void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            base.ReaderExecuting(command, interceptionContext);
            _stopwatch.Restart();
        }
        
        public override void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            _stopwatch.Stop();
            if (interceptionContext.Exception != null)
            {
                _logger.Error(interceptionContext.Exception, "Error executing command: {0}", command.CommandText);
            }
            else
            {
                _logger.TraceApi("SQL Database", "StartupAppDbInterceptor.ReaderExecuted", _stopwatch.Elapsed, "Command: {0}: ", command.CommandText);
            }
            base.ReaderExecuted(command, interceptionContext);
        }
        #endregion
    }
}
