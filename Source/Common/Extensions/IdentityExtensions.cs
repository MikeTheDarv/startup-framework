﻿using System;
using System.Security.Claims;
using System.Security.Principal;

namespace StartupApp.Common.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetUserClaim(this IIdentity identity, string claimType)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }

            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            if (claimsIdentity != null)
            {
                var claim = claimsIdentity.FindFirst(claimType);
                return claim != null ? claim.Value : null;
            }

            return null;
        }
    }
}
