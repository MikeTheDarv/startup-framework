﻿using System;

namespace StartupApp.Common.Exceptions
{
    public class LoginException : Exception
    {
        #region Constructors
        public LoginException(string failedLoginReason)
            : base(failedLoginReason)
        {

        }
        #endregion
    }
}
