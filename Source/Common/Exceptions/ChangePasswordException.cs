﻿using System;

namespace StartupApp.Common.Exceptions
{
    public class ChangePasswordException : Exception
    {
        #region Constructors
        public ChangePasswordException(string reasonForFailure)
            : base(reasonForFailure)
        {

        }
        #endregion
    }
}
