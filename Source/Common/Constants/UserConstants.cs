﻿namespace StartupApp.Common.Constants
{
    public static class UserConstants
    {
        public const int ANONYMOUS_USER_ID = -999;
        public const int SYSTEM_ADMIN_USER_ID = -998;
    }
}
