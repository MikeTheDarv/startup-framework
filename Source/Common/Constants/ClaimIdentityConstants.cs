﻿namespace StartupApp.Common.Constants
{
    public static class ClaimIdentityConstants
    {
        public const string IS_PENDING_PASSWORD_RESET = "urn:app/ws/2014/claims/pendingpasswordreset";
        public const string AVATAR = "urn:app/ws/2014/claims/useravatar";
        public const string USER_ID = "urn:app/ws/2014/claims/userid";
        public const string USER_TYPE = "urn:app/ws/2014/claims/usertype";
        public const string IDENTITY_PROVIDER = "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";
    }
}
