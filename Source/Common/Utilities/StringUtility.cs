﻿using System;
using System.Text;

namespace StartupApp.Common.Utilities
{
    public static class StringUtility
    {
        #region Private Variables
        private static readonly Random _random = new Random();

        #endregion

        #region Public Methods
        public static Random RandomGenerator
        {
            get { return _random; }
        }

        public static string RandomString(int length, string includeCharacters = "")
        {
            const string ALPHA_NUMERIC = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            string charactersToUse = string.Concat(ALPHA_NUMERIC, includeCharacters);

            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(charactersToUse[RandomGenerator.Next(charactersToUse.Length)]);
            }
            return result.ToString();
        }
        #endregion
    }
}
