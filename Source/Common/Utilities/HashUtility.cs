﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;

namespace StartupApp.Common.Utilities
{
    public static class HashUtility
    {
        #region Private Variables
        private static RandomNumberGenerator _randomGenerator = new RNGCryptoServiceProvider();
        #endregion

        #region Public Methods
        public static string HashSHA1(string stringToHash)
        {
            return HashString(stringToHash, "SHA1");
        }

        public static string HashMD5(string stringToHash)
        {
            return HashString(stringToHash, "MD5");
        }

        public static string HashFormsAuthentication(string stringToHash)
        {
#pragma warning disable 612,618
            return FormsAuthentication.HashPasswordForStoringInConfigFile(stringToHash, "SHA1");
#pragma warning restore 612,618
        }

        public static string RandomOAuthState()
        {
            const int STRENGTH_IN_BITS = 256;
            const int BITS_PER_BYTE = 8;
            const int STRENGTH_IN_BYTES = STRENGTH_IN_BITS / BITS_PER_BYTE;

            byte[] data = new byte[STRENGTH_IN_BYTES];
            _randomGenerator.GetBytes(data);

            return HttpServerUtility.UrlTokenEncode(data);
        }
        #endregion

        #region Private Methods
        private static string HashString(string inputString, string hashName)
        {
            HashAlgorithm algorithm = HashAlgorithm.Create(hashName);
            if (algorithm == null)
            {
                throw new ArgumentException("Unrecognized hash name", "hashName");
            }
            byte[] hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
            return Convert.ToBase64String(hash);
        }
        #endregion
    }
}
