﻿namespace StartupApp.Common.Enums
{
    /// <summary>
    /// Enum used for url resolution within services
    /// for emails that include urls.
    /// </summary>
    public enum EmailUrlType
    {
        UserActivation
    }
}
