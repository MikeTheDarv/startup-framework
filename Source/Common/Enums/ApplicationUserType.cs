﻿namespace StartupApp.Common.Enums
{
    public enum ApplicationUserType
    {
        SystemAdministrator = -999,
        Null = 0,
        User = 1,
        Administrator = 2
    }
}
