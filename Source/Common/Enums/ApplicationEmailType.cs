﻿namespace StartupApp.Common.Enums
{
    public enum ApplicationEmailType
    {
        Null = 0,

        // -- User Emails
        UserRegistered = 10,
        UserForgotPassword = 11
    }
}
