﻿using System;
using System.Web;
using System.Web.Caching;
using StartupApp.Security.Identity.Interfaces;

namespace StartupApp.Utility.WebRoles.Impl.Security.Identity
{
    /// <summary>
    /// Represents a local system identity cache for web contexts.
    /// </summary>
    public class HttpCacheSystemIdentityStore : ISystemIdentityStore
    {
        #region Private Variables
        private const string IDENTITY_CACHE_KEY = "SystemIdentityStore_Identity";
        private const int CACHE_EXPIRATION_IN_MINUTES = 10;
        #endregion

        #region Implementation of ISystemIdentityStore
        public ISystemIdentity GetIdentity()
        {
            return HttpContext.Current.Cache[IDENTITY_CACHE_KEY] as ISystemIdentity;
        }

        public void StoreIdentity(ISystemIdentity identity)
        {
            // -- Force the cache to refresh every 10 minutes.
            var cache = HttpContext.Current.Cache;
            var expiration = DateTime.Now.AddMinutes(CACHE_EXPIRATION_IN_MINUTES);

            cache.Remove(IDENTITY_CACHE_KEY);
            cache.Add(IDENTITY_CACHE_KEY, identity, null, expiration, Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        public void StaleIdentity()
        {
            var cache = HttpContext.Current.Cache;
            cache.Remove(IDENTITY_CACHE_KEY);
        }

        #endregion
    }
}
