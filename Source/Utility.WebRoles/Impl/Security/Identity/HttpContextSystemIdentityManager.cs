﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using StartupApp.Domain.Model;
using StartupApp.Security.Identity.Impl;
using StartupApp.Security.Identity.Interfaces;
using Startup.Framework.Persistence.Interfaces;

namespace StartupApp.Utility.WebRoles.Impl.Security.Identity
{
    /// <summary>
    /// Transform the current OWIN identity to a system identity
    /// by loading it from cache or database, the populate the store.
    /// </summary>
    public class HttpContextSystemIdentityManager : ISystemIdentityManager
    {
        #region Private Variables
        private readonly ISystemIdentityStore _systemIdentityStore;
        private readonly IRepository<User> _userRepository;
        #endregion

        #region Constructors
        public HttpContextSystemIdentityManager(ISystemIdentityStore systemIdentityStore, IRepository<User> userRepository)
        {
            _userRepository = userRepository;
            _systemIdentityStore = systemIdentityStore;
        }
        #endregion

        #region Implementation of ISystemIdentityManager
        public void InitializeIdentityStore()
        {
            // -- Return anonymous identity if identity isn't authenticated
            var identity = HttpContext.Current.User.Identity;
            if (!identity.IsAuthenticated || !(identity is ClaimsIdentity))
            {
                _systemIdentityStore.StoreIdentity(GetAnonymousSystemIdentity());
                return;
            }

            // -- Grab the name identifier claim
            var claimsIdentity = (ClaimsIdentity) identity;
            var nameIdentifierClaim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
            var nameIdentifier = nameIdentifierClaim != null ? nameIdentifierClaim.Value : string.Empty;

            // -- Lookup the identity from the store.  If
            //    identity is already cached in store, do nothing,
            //    otherwise grab the identity from the database and update
            //    store.
            var systemIdentity = _systemIdentityStore.GetIdentity();
            if (systemIdentity == null || !CachedIdentityIsUser(systemIdentity, nameIdentifier))
            {
                // -- Lookup the identity from the database
                var foundIdentity =
                    _userRepository.QueryAll()
                        .FirstOrDefault(u => u.UserName == nameIdentifier || u.EmailAddress == nameIdentifier);

                systemIdentity = foundIdentity != null
                    ? new SystemIdentity(foundIdentity)
                    : GetAnonymousSystemIdentity();

                // -- Store the identity
                _systemIdentityStore.StoreIdentity(systemIdentity);
            }
        }
        #endregion

        #region Private Methods
        private ISystemIdentity GetAnonymousSystemIdentity()
        {
            var anonymousUser = new User
            {
                FirstName = "Guest"
            };

            return new SystemIdentity(anonymousUser);
        }

        private bool CachedIdentityIsUser(ISystemIdentity systemIdentity, string nameIdentifier)
        {
            return systemIdentity.UserName.Equals(nameIdentifier, StringComparison.CurrentCultureIgnoreCase);
        }
        #endregion
    }
}
