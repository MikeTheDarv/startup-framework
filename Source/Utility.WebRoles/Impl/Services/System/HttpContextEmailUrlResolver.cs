﻿using System;
using System.Web;
using System.Web.Mvc;
using StartupApp.Common.Enums;
using StartupApp.Services.System.Interfaces;

namespace StartupApp.Utility.WebRoles.Impl.Services.System
{
    public class HttpContextEmailUrlResolver : IEmailUrlResolver
    {
        #region Implementation of IEmailUrlResolver
        public string ResolveUrl(EmailUrlType urlType, object routeValues = null)
        {
            switch (urlType)
            {
                case EmailUrlType.UserActivation:
                    return GenerateUrl("activate", "account", routeValues);
                default:
                    throw new ArgumentOutOfRangeException("urlType", string.Format("Invalid url type provided - [{0}]", urlType));
            }
        }
        #endregion

        #region Private Methods
        private string GenerateUrl(string action, string controller, object routeValues = null)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var scheme = HttpContext.Current.Request.Url.Scheme;
            return urlHelper.Action(action, controller, routeValues, scheme);
        }
        #endregion
    }
}
