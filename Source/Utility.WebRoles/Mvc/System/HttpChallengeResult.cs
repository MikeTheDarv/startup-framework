﻿using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;

namespace StartupApp.Utility.WebRoles.Mvc.System
{
    public class HttpChallengeResult : HttpUnauthorizedResult
    {
        #region Private Variables
        private const string XSRF_KEY = "StartupAppXsrfId";
        #endregion

        #region Properties
        public static string XsrfKey
        {
            get { return XSRF_KEY; }
        }

        public string LoginProvider { get; set; }

        public string RedirectUri { get; set; }

        public string UserId { get; set; }
        #endregion

        #region Constructors
        public HttpChallengeResult(string provider, string redirectUri, string userId = null)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
            UserId = userId;
        }
        #endregion

        #region Overrides
        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties {RedirectUri = RedirectUri};
            if (UserId != null)
            {
                properties.Dictionary[XSRF_KEY] = UserId;
            }
            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        }
        #endregion
    }
}
