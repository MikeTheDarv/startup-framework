﻿using System.Web.Mvc;
using StartupApp.Utility.WebRoles.Mvc.System;
using Startup.Framework.Web.Mvc;

namespace StartupApp.Utility.WebRoles.Mvc
{
    public class CommonConfig
    {
        public static void RegisterBinders(ModelBinderDictionary modelBinders)
        {
            modelBinders.DefaultBinder = new EmptyStringModelBinder();
        }
    }
}
