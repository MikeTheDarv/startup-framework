﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Startup.Framework.Validation.Interfaces;
using Startup.Framework.Web.Mvc;

namespace StartupApp.Utility.WebRoles.Mvc
{
    public static class ControllerExtensions
    {
    }

    public static class HelperExtensions
    {
        #region UrlHelper Methods
        public static string Action(this UrlHelper urlHelper, string actionName, string controllerName, object routeValues, bool inheritRouteParams)
        {
            if (inheritRouteParams)
            {
                return urlHelper.Action(actionName, controllerName, routeValues);
            }

            urlHelper = new UrlHelper(new RequestContext(urlHelper.RequestContext.HttpContext, new RouteData()));
            return urlHelper.Action(actionName, controllerName, routeValues);
        }

        public static string Action(this UrlHelper urlHelper, string actionName, string controllerName, RouteValueDictionary routeValues, bool inheritRouteParams)
        {
            if (inheritRouteParams)
            {
                return urlHelper.Action(actionName, controllerName, routeValues);
            }

            urlHelper = new UrlHelper(new RequestContext(urlHelper.RequestContext.HttpContext, new RouteData()));
            return urlHelper.Action(actionName, controllerName, routeValues);
        }
        #endregion

        #region ModelStateDictionary Methods
        public static void AddValidationErrors(this ModelStateDictionary dictionary, IEnumerable<IValidationError> errors)
        {
            dictionary.AddValidationErrors(errors, error => error.Key);
        }

        public static void AddValidationErrors(this ModelStateDictionary dictionary, IEnumerable<IValidationError> errors, Func<IValidationError, string> keyProvider)
        {
            foreach (var validationError in errors)
            {
                dictionary.AddModelError(keyProvider(validationError), validationError.Message);
            }
        }
        #endregion
    }

    public static class RouteExtensions
    {
        #region Trailing Slash Methods
        public static void MapRouteTrailingSlash(this RouteCollection routes, string name, string url, object defaults)
        {
            routes.MapRouteTrailingSlash(name, url, defaults, null);
        }

        public static void MapRouteTrailingSlash(this RouteCollection routes, string name, string url, object defaults, object constraints)
        {
            if (routes == null)
                throw new ArgumentNullException("routes");

            if (url == null)
                throw new ArgumentNullException("url");

            var route = new TrailingSlashRoute(url, new MvcRouteHandler())
            {
                Defaults = new RouteValueDictionary(defaults),
                Constraints = new RouteValueDictionary(constraints)
            };

            if (String.IsNullOrEmpty(name))
                routes.Add(route);
            else
                routes.Add(name, route);
        }
        #endregion
    }
}
