﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace StartupApp.Utility.WebRoles.Mvc.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class OnlyAnonymousAttribute : FilterAttribute, IActionFilter
    {
        #region Private Variables
        private readonly string _actionName;
        private readonly string _controllerName;
        #endregion

        #region Constructors
        public OnlyAnonymousAttribute()
            : this("Index", "Home")
        {
        }

        public OnlyAnonymousAttribute(string actionName, string controllerName)
        {
            _actionName = actionName;
            _controllerName = controllerName;
        }
        #endregion

        #region Implementation of IActionFilter
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // -- Redirect if authenticated
            if (filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
                routeValueDictionary["action"] = _actionName;
                routeValueDictionary["controller"] = _controllerName;
                filterContext.Result = new RedirectToRouteResult(routeValueDictionary);
            }
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }
        #endregion
    }
}
