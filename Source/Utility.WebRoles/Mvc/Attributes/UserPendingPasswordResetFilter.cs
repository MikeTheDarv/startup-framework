﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using StartupApp.Security.Identity.Interfaces;
using Startup.Framework.Dependency;

namespace StartupApp.Utility.WebRoles.Mvc.Attributes
{
    public class UserPendingPasswordResetFilterAttribute : FilterAttribute, IActionFilter
    {
        #region Private Variables
        private readonly string _actionName;
        private readonly string _controllerName;
        private readonly string _logoutActionName;
        private readonly string _logoutControllerName;
        #endregion

        #region Properties
        [InjectDependency]
        public ISystemIdentityStore IdentityStore { get; set; }
        #endregion

        #region Constructors
        public UserPendingPasswordResetFilterAttribute(string actionName, string controllerName)
            : this(actionName, controllerName, "Logout", "Session")
        {
        }

        public UserPendingPasswordResetFilterAttribute(string actionName, string controllerName, string logoutActionName, string logoutControllerName)
        {
            _actionName = actionName;
            _controllerName = controllerName;
            _logoutActionName = logoutActionName;
            _logoutControllerName = logoutControllerName;
        }
        #endregion

        #region Implementation of IActionFilter
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // -- Prevent infinite redirect loop
            var routeData = filterContext.RouteData;
            if ((_actionName.Equals(routeData.GetRequiredString("action"), StringComparison.CurrentCultureIgnoreCase) &&
                 _controllerName.Equals(routeData.GetRequiredString("controller"),
                     StringComparison.CurrentCultureIgnoreCase)) ||
                (_logoutActionName.Equals(routeData.GetRequiredString("action"), StringComparison.CurrentCultureIgnoreCase) &&
                 _logoutControllerName.Equals(routeData.GetRequiredString("controller"),
                     StringComparison.CurrentCultureIgnoreCase)))
            {
                return;
            }

            var identity = IdentityStore.GetIdentity();
            if (identity != null)
            {
                if (identity.IsPendingPasswordReset)
                {
                    RouteValueDictionary routeValueDictionary = new RouteValueDictionary();
                    routeValueDictionary["action"] = _actionName;
                    routeValueDictionary["controller"] = _controllerName;

                    filterContext.Result = new RedirectToRouteResult(routeValueDictionary);
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            
        }
        #endregion

        #region Private Methods
        #endregion
    }
}
