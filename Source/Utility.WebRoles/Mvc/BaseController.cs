﻿using System.Web.Mvc;
using StartupApp.Security.Identity.Interfaces;
using StartupApp.Utility.WebRoles.Mvc.Attributes;
using Startup.Framework.Dependency;
using Startup.Framework.Dependency.Interfaces;
using Startup.Framework.Persistence.Interfaces;

namespace StartupApp.Utility.WebRoles.Mvc
{
    [UserPendingPasswordResetFilter("ResetPassword", "Account")]
    public class BaseController : Controller
    {
        #region Private Variables

        #endregion

        #region Properties

        [InjectDependency]
        public IDependencyServiceLocator DependencyServiceLocator { get; set; }
        #endregion

        #region Constructors

        #endregion

        #region Protected Methods
        protected IUnitOfWork UnitOfWork()
        {
            return DependencyServiceLocator.GetInstance<IUnitOfWork>();
        }

        protected ISystemIdentity GetIdentity()
        {
            return DependencyServiceLocator.GetInstance<ISystemIdentityStore>().GetIdentity();
        }

        protected void StaleIdentity()
        {
            var identityStore = DependencyServiceLocator.GetInstance<ISystemIdentityStore>();
            identityStore.StaleIdentity();
        }
        #endregion
    }
}
