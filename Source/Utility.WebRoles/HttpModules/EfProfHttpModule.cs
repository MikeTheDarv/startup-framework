﻿using System.Web;
using HibernatingRhinos.Profiler.Appender.EntityFramework;

namespace StartupApp.Utility.WebRoles.HttpModules
{
    public class EfProfHttpModule : IHttpModule
    {
        #region Static privates
        private static volatile bool _applicationStarted;
        private static readonly object _applicationStartLock = new object();
        #endregion

        #region Implementation of IHttpModule
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            if (!_applicationStarted)
            {
                lock (_applicationStartLock)
                {
                    if (!_applicationStarted)
                    {
                        OnStart();
                        _applicationStarted = true;
                    }
                }
            }
        }
        #endregion

        #region Private Methods

        private void OnStart()
        {
#if (DEBUG)
            EntityFrameworkProfiler.Initialize();
#endif
        }

        #endregion
    }
}
