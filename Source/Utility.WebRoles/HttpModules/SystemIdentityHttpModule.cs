﻿using System;
using System.Web;
using StartupApp.Security.Identity.Interfaces;
using Startup.Framework.Dependency.Interfaces;

namespace StartupApp.Utility.WebRoles.HttpModules
{
    public class SystemIdentityHttpModule : IHttpModule
    {
        #region Private Variables
        private readonly IDependencyServiceLocator _dependencyServiceLocator;
        #endregion

        #region Constructors
        public SystemIdentityHttpModule(IDependencyServiceLocator dependencyServiceLocator)
        {
            _dependencyServiceLocator = dependencyServiceLocator;
        }

        #endregion

        #region Implementation of IHttpModule
        public void Init(HttpApplication context)
        {
            context.PostAcquireRequestState += OnPostAcquireRequestState;
        }

        public void Dispose()
        {
        }
        #endregion

        #region Private Methods
        private void OnPostAcquireRequestState(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null)
            {
                var identityManager = _dependencyServiceLocator.GetInstance<ISystemIdentityManager>();
                identityManager.InitializeIdentityStore();
            }
        }
        #endregion
    }
}
