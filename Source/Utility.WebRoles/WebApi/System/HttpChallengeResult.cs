﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StartupApp.Utility.WebRoles.WebApi.System
{
    public class HttpChallengeResult : IHttpActionResult
    {
        #region Properties
        public string LoginProvider { get; set; }

        public HttpRequestMessage Request { get; set; }
        #endregion

        #region Constructors
        public HttpChallengeResult(string loginProvider, ApiController controller)
        {
            LoginProvider = loginProvider;
            Request = controller.Request;
        }
        #endregion

        #region Overrides
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            Request.GetOwinContext().Authentication.Challenge(LoginProvider);

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
            {
                RequestMessage = Request
            };

            return Task.FromResult(response);
        }
        #endregion
    }
}
