﻿using System;
using System.Collections.Generic;
using System.Web.Http.ModelBinding;
using Startup.Framework.Validation.Interfaces;

namespace StartupApp.Utility.WebRoles.WebApi
{
    public static class ModelBindingExtensions
    {
        #region ModelStateDictionary Methods
        public static void AddValidationErrors(this ModelStateDictionary dictionary, IEnumerable<IValidationError> errors)
        {
            dictionary.AddValidationErrors(errors, error => error.Key);
        }

        public static void AddValidationErrors(this ModelStateDictionary dictionary, IEnumerable<IValidationError> errors, Func<IValidationError, string> keyProvider)
        {
            foreach (var validationError in errors)
            {
                dictionary.AddModelError(keyProvider(validationError), validationError.Message);
            }
        }
        #endregion
    }
}
