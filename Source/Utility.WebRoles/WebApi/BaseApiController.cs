﻿using System.Web.Http;
using StartupApp.Security.Identity.Interfaces;
using Startup.Framework.Dependency;
using Startup.Framework.Dependency.Interfaces;
using Startup.Framework.Persistence.Interfaces;
using StartupApp.Services.Domain.Interfaces;

namespace StartupApp.Utility.WebRoles.WebApi
{
    public class BaseApiController : ApiController
    {
        #region Private Variables

        #endregion

        #region Properties

        [InjectDependency]
        public IDependencyServiceLocator DependencyServiceLocator { get; set; }
        #endregion

        #region Constructors

        #endregion

        #region Protected Methods
        protected IUnitOfWork UnitOfWork()
        {
            return DependencyServiceLocator.GetInstance<IUnitOfWork>();
        }

        protected ISystemIdentity GetIdentity()
        {
            return DependencyServiceLocator.GetInstance<ISystemIdentityStore>().GetIdentity();
        }

        protected void StaleIdentity()
        {
            var identityStore = DependencyServiceLocator.GetInstance<ISystemIdentityStore>();
            identityStore.StaleIdentity();
        }
        #endregion
    }
}
