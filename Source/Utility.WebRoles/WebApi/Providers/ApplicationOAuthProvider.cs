﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using StartupApp.Common.Exceptions;
using StartupApp.Services.System.Interfaces;

namespace StartupApp.Utility.WebRoles.WebApi.Providers
{
    /// <summary>
    /// Instead of logging in via a controller, this OAuth provider
    /// does the heavy lifting.  We'll plugin in to the architecture
    /// to login via our user store.
    /// </summary>
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        #region Overrides
        // ReSharper disable once CSharpWarnings::CS1998
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var authenticationService = DependencyResolver.Current.GetService<IAuthenticationService>();
            ClaimsIdentity identity;

            try
            {
                identity = authenticationService.Authenticate(context.UserName, context.Password, false);
            }
            catch (LoginException e)
            {
                context.SetError("invalid_grant", e.Message);
                return;
            }

            AuthenticationProperties properties = new AuthenticationProperties(new Dictionary<string, string> { { "userName", identity.Name } });
            AuthenticationTicket ticket = new AuthenticationTicket(identity, properties);

            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == "self")
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }
        #endregion
    }
}
