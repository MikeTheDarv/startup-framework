﻿using System.Web.Mvc;
using StartupApp.Utility.WebRoles.Mvc;

namespace StartupApp.Bootstrap
{
    public class WebBootstrapper
    {
        public static void Startup()
        {
            CommonConfig.RegisterBinders(ModelBinders.Binders);
            CommonBootstrapper.RegisterAutoMapper();
        }
    }
}
