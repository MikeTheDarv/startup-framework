﻿namespace StartupApp.Bootstrap
{
    internal static class CommonBootstrapper
    {
        public static void RegisterAutoMapper()
        {
            /* -- For use with AutoMapper!
            
            Mapper.CreateMap<Address, AddressStub>();
            Mapper.CreateMap<BetaInvite, BetaInviteStub>();
            Mapper.CreateMap<BillingPlan, BillingPlanStub>();
            Mapper.CreateMap<Account, AccountStub>();
            Mapper.CreateMap<Client, ClientStub>();
            Mapper.CreateMap<User, UserStub>();
            Mapper.CreateMap<Workspace, WorkspaceStub>();
            Mapper.CreateMap<Projection, ProjectionStub>();
            Mapper.CreateMap<ProjectionStickyNote, StickyNoteStub>();
            Mapper.CreateMap<ProjectionLink, ProjectionLinkStub>();
            Mapper.CreateMap<ProjectionLinkSet, ProjectionLinkSetStub>();
            Mapper.CreateMap<ProjectionPrototypeSettings, PrototypeSettingsStub>();
            Mapper.CreateMap<PaymentProfile, PaymentProfileStub>();
            Mapper.CreateMap<RequiredAddress, RequiredAddressStub>();
            */
        }
    }
}
