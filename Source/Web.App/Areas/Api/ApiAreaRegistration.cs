﻿using System.Web.Http;
using System.Web.Mvc;

namespace StartupApp.Web.App.Areas.Api
{
    public class ApiAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Api";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            /* -- Demo purposes */
            context.MapRoute(
                "ApiDemo",
                "api",
                new { action = "Index", controller = "HomeApi", area = "Api" }
            );

            context.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional, area = "Api" }
            );
        }
    }
}