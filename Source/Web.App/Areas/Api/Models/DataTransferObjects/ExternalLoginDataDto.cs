﻿using System.Collections.Generic;
using System.Security.Claims;

namespace StartupApp.Web.App.Areas.Api.Models.DataTransferObjects
{
    public class ExternalLoginDataDto
    {
        #region Properties
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

        public string UserName { get; set; }
        #endregion

        #region Public Methods
        public IList<Claim> GetClaims()
        {
            IList<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

            if (UserName != null)
            {
                claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
            }

            return claims;
        }
        #endregion
    }
}