﻿using System.Web.Mvc;

namespace StartupApp.Web.App.Areas.Api.Controllers
{
    public class HomeApiController : Controller
    {
        #region Actions
        // GET: /Api/Home
        public ActionResult Index()
        {
            return View();
        }
        #endregion
    }
}