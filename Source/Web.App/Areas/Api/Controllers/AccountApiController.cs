﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Startup.Framework.Validation;
using StartupApp.Common.Exceptions;
using StartupApp.Domain.Model;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.System.Interfaces;
using StartupApp.Utility.WebRoles.WebApi;
using StartupApp.Web.App.Areas.Api.Helpers;
using StartupApp.Web.App.Areas.Api.Models.BindingModels;
using StartupApp.Web.App.Areas.Api.Models.DataTransferObjects;
using StartupApp.Web.App.Areas.Api.Models.ViewModels;

namespace StartupApp.Web.App.Areas.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountApiController : BaseApiController
    {
        #region Private Variables
        private readonly IUserDomainService _userDomainService;
        private readonly IRegistrationService _registrationService;
        #endregion

        #region Constructors
        public AccountApiController(IRegistrationService registrationService, IUserDomainService userDomainService)
        {
            _registrationService = registrationService;
            _userDomainService = userDomainService;
        }

        #endregion

        #region Actions
        // GET: /Api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpGet, Route("UserInfo")]
        public UserInfoViewModel UserInfo()
        {
            var externalLogin = GetExternalLoginFromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                UserName = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
            };
        }

        // POST: /Api/Account/Register
        [AllowAnonymous]
        [HttpPost, Route("Register")]
        public IHttpActionResult Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new User
            {
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            using (var uow = UnitOfWork())
            {
                try
                {
                    // -- Register user
                    _registrationService.Register(user, model.Password);

                    uow.Commit();
                }
                catch (ValidationException e)
                {
                    uow.Rollback();

                    ModelState.AddValidationErrors(e.ValidationErrors);

                    return BadRequest(ModelState);
                }
            }

            return Ok();
        }

        // POST: /Api/Account/RegisterExternal
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [HttpPost, Route("ExternalRegister")]
        public IHttpActionResult ExternalRegister(RegisterExternalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var externalLogin = GetExternalLoginFromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            var user = new User
            {
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            user.LoginIdentities.Add(new UserLoginIdentity
            {
                LoginProvider = externalLogin.LoginProvider,
                ProviderKey = externalLogin.ProviderKey
            });

            using (var uow = UnitOfWork())
            {
                try
                {
                    // -- Register user
                    _registrationService.Register(user, string.Empty);

                    uow.Commit();
                }
                catch (ValidationException e)
                {
                    uow.Rollback();

                    ModelState.AddValidationErrors(e.ValidationErrors);

                    return BadRequest(ModelState);
                }
            }

            return Ok();
        }

        // GET: /Api/Account/Manage?returnUrl=%2F&generateState=true
        [HttpGet, Route("ManageInfo")]
        public ManageInfoViewModel Manage(string returnUrl, bool generateState = false)
        {
            var identity = GetIdentity();
            var user = _userDomainService.Select(identity.Id, u => u);

            var logins = user.LoginIdentities
                .Select(linkedAccount => new UserLoginInfoViewModel
                {
                    LoginProvider = linkedAccount.LoginProvider,
                    ProviderKey = linkedAccount.ProviderKey
                })
                .ToList();

            if (!string.IsNullOrWhiteSpace(user.PasswordHash))
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = "LocalAuth",
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = "LocalAuth",
                UserName = user.UserName,
                Logins = logins,
                ExternalLoginProviders = this.GetExternalLogins(returnUrl, generateState)
            };
        }

        // POST: /Api/Account/ChangePassword
        [HttpPost, Route("ChangePassword")]
        public IHttpActionResult ChangePassword(ChangePasswordBindingModel model)
        {
            bool hasPassword = HasPassword();

            if (!hasPassword)
            {
                ModelState state = ModelState["model.OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }
            }

            if (ModelState.IsValid)
            {
                using (var uow = UnitOfWork())
                {
                    try
                    {
                        var identity = GetIdentity();

                        _userDomainService.ChangePassword(identity.Id, model.OldPassword ?? string.Empty, model.NewPassword);

                        uow.Commit();

                        return Ok();
                    }
                    catch (ChangePasswordException e)
                    {
                        uow.Rollback();
                        ModelState.AddModelError(string.Empty, e.Message);
                    }
                }
            }

            return BadRequest(ModelState);
        }

        // POST: /Api/Account/AssociateIdentity
        [HttpPost, Route("AssociateIdentity")]
        public IHttpActionResult AssociateIdentity(AssociateIdentityBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var authentication = HttpContext.Current.GetOwinContext().Authentication;

            authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

            AuthenticationTicket ticket = Startup.GetOAuthAuthenticationOptions().AccessTokenFormat.Unprotect(model.ExternalAccessToken);

            if (ticket == null ||
                ticket.Identity == null ||
                (ticket.Properties != null
                    && ticket.Properties.ExpiresUtc.HasValue
                    && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow)
                )
            {
                return BadRequest("External login failure.");
            }

            var externalData = GetExternalLoginFromIdentity(ticket.Identity);

            if (externalData == null)
            {
                return BadRequest("The external login is already associated with an account.");
            }

            var userIdentity = new UserLoginIdentity
            {
                LoginProvider = externalData.LoginProvider,
                ProviderKey = externalData.ProviderKey
            };

            using (var uow = UnitOfWork())
            {
                try
                {
                    // -- Add identity
                    _userDomainService.AddLoginIdentity(GetIdentity().Id, userIdentity);

                    uow.Commit();
                }
                catch (ValidationException)
                {
                    uow.Rollback();

                    return BadRequest("The external login is already associated with an account.");
                }
            }

            return Ok();
        }

        // POST: /Api/Account/DisassociateIdentity
        [HttpPost, Route("DisassociateIdentity")]
        public IHttpActionResult DisassociateIdentity(DisassociateIdentityBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.LoginProvider == "LocalAuth")
            {
                // result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
            }
            else
            {
                using (var uow = UnitOfWork())
                {
                    try
                    {
                        // -- Remove identity
                        _userDomainService.RemoveIdentityLogin(GetIdentity().Id, model.LoginProvider, model.ProviderKey);

                        uow.Commit();
                    }
                    catch (ValidationException)
                    {
                        uow.Rollback();

                        return BadRequest("An error occurred.");
                    }
                }   
            }

            return Ok();
        }
        #endregion

        #region Private Methods
        private ExternalLoginDataDto GetExternalLoginFromIdentity(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

            if (providerKeyClaim == null ||
                string.IsNullOrEmpty(providerKeyClaim.Issuer) ||
                string.IsNullOrEmpty(providerKeyClaim.Value))
            {
                return null;
            }

            if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
            {
                return null;
            }

            return new ExternalLoginDataDto
            {
                LoginProvider = providerKeyClaim.Issuer,
                ProviderKey = providerKeyClaim.Value,
                UserName = identity.FindFirstValue(ClaimTypes.Name)
            };
        }

        private bool HasPassword()
        {
            var userId = User.Identity.GetUserId();
            var passwordHash = _userDomainService.SelectWhere(u => u.EmailAddress == userId, u => u.PasswordHash) ?? string.Empty;
            return !string.IsNullOrWhiteSpace(passwordHash);
        }
        #endregion
    }
}