﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using StartupApp.Common.Exceptions;
using StartupApp.Services.System.Interfaces;
using StartupApp.Utility.WebRoles.WebApi;
using StartupApp.Utility.WebRoles.WebApi.System;
using StartupApp.Web.App.Areas.Api.Helpers;
using StartupApp.Web.App.Areas.Api.Models.DataTransferObjects;
using StartupApp.Web.App.Areas.Api.Models.ViewModels;

namespace StartupApp.Web.App.Areas.Api.Controllers
{
    [Authorize]
    [RoutePrefix("Api/Session")]
    public class SessionApiController : BaseApiController
    {
        #region Private Variables
        private readonly IAuthenticationService _authenticationService;
        #endregion

        #region Constructors
        public SessionApiController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        #endregion

        #region Actions
        // GET: /Api/Session/Login -- Handled via the Utility.WebRoles.AppOAuthProvider
        public void Login() { }

        // GET: /Api/Session/ExternalLogin
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [AllowAnonymous]
        [HttpGet, Route("ExternalLogin", Name = "ExternalLogin")]
        public IHttpActionResult ExternalLogin(string provider, string error = null)
        {
            var authentication = Request.GetOwinContext().Authentication;

            if (error != null)
            {
                return Redirect(Url.Content("~/api/") + "#error=" + Uri.EscapeDataString(error));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return new HttpChallengeResult(provider, this);
            }

            var externalLogin = GetExternalLoginFromIdentity(User.Identity as ClaimsIdentity);

            if (externalLogin == null)
            {
                return InternalServerError();
            }

            if (externalLogin.LoginProvider != provider)
            {
                authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                return new HttpChallengeResult(provider, this);
            }

            try
            {
                var appIdentity = _authenticationService.AuthenticateExternal(externalLogin.LoginProvider, externalLogin.ProviderKey, false);
                var externalIdentity = new ClaimsIdentity(appIdentity.Claims, OAuthDefaults.AuthenticationType);
                var properties = new AuthenticationProperties(new Dictionary<string, string> { { "userName", externalIdentity.Name } });

                authentication.SignIn(properties, externalIdentity);
            }
            catch (LoginException)
            {
                IEnumerable<Claim> claims = externalLogin.GetClaims();
                var identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                authentication.SignIn(identity);
            }

            return Ok();
        }

        // GET: /Api/Session/ExternalLogins?returnUrl=%2F&generateState=true
        [AllowAnonymous]
        [HttpGet, Route("ExternalLogins")]
        public IEnumerable<ExternalLoginViewModel> ExternalLogins(string returnUrl, bool generateState = false)
        {
            return this.GetExternalLogins(returnUrl, generateState);
        }

        // POST: /Api/Session/Logout
        [HttpPost, Route("Logout")]
        public IHttpActionResult Logout()
        {
            _authenticationService.Invalidate();
            return Ok();
        }
        #endregion

        #region Private Methods
        private ExternalLoginDataDto GetExternalLoginFromIdentity(ClaimsIdentity identity)
        {
            if (identity == null)
            {
                return null;
            }

            Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

            if (providerKeyClaim == null ||
                string.IsNullOrEmpty(providerKeyClaim.Issuer) ||
                string.IsNullOrEmpty(providerKeyClaim.Value))
            {
                return null;
            }

            if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
            {
                return null;
            }

            return new ExternalLoginDataDto
            {
                LoginProvider = providerKeyClaim.Issuer,
                ProviderKey = providerKeyClaim.Value,
                UserName = identity.FindFirstValue(ClaimTypes.Name)
            };
        }
        #endregion
    }
}