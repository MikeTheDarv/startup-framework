﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security;
using StartupApp.Common.Utilities;
using StartupApp.Web.App.Areas.Api.Models.ViewModels;

namespace StartupApp.Web.App.Areas.Api.Helpers
{
    public static class ExternalLoginApiControllerHelper
    {
        public static IEnumerable<ExternalLoginViewModel> GetExternalLogins(this ApiController controller, string returnUrl, bool generateState = false)
        {
            var owinContext = controller.Request.GetOwinContext();
            IEnumerable<AuthenticationDescription> descriptions = owinContext.Authentication.GetExternalAuthenticationTypes();

            string state = generateState ? HashUtility.RandomOAuthState() : null;

            return descriptions.Select(description => new ExternalLoginViewModel
            {
                Name = description.Caption,
                Url = controller.Url.Route("ExternalLogin", new
                {
                    provider = description.AuthenticationType,
                    response_type = "token",
                    client_id = "self",
                    redirect_uri = new Uri(controller.Request.RequestUri, returnUrl).AbsoluteUri,
                    state
                }),
                State = state
            }).ToList();
        }
    }
}