﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using StartupApp.Bootstrap;

namespace StartupApp.Web.App
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            /* -- Web API must be called before Area Registration -- */
            GlobalConfiguration.Configure(WebApiConfig.Register);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebBootstrapper.Startup();
        }
    }
}
