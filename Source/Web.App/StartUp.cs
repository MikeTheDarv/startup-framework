﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(StartupApp.Web.App.Startup))]

namespace StartupApp.Web.App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuthentication(app);
        }
    }
}
