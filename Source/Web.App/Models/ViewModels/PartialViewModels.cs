﻿namespace StartupApp.Web.App.Models.ViewModels
{
    public class ExternalLoginsListPartial
    {
        public string Action { get; set; }
        public string ReturnUrl { get; set; }
        public string Controller { get; set; }
    }
}
