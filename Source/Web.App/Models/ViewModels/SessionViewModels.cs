﻿using System.ComponentModel.DataAnnotations;

namespace StartupApp.Web.App.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required, EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
