﻿using System.Web.Mvc;
using System.Web.Routing;
using StartupApp.Utility.WebRoles.Mvc;

namespace StartupApp.Web.App
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;
            routes.AppendTrailingSlash = true;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // -- Default Route
            routes.MapRouteTrailingSlash("Default", "{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}
