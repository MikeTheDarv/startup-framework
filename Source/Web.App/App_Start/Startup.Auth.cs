﻿using System;
using Microsoft.Owin.Security.OAuth;
using StartupApp.Configuration.Impl;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using StartupApp.Utility.WebRoles.WebApi.Providers;

namespace StartupApp.Web.App
{
    public partial class Startup
    {
        #region Private Variables
        private static OAuthAuthorizationServerOptions _serverOptions;
        #endregion

        #region Public Methods
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuthentication(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            var cookieConfiguration = new OwinCookieConfiguration("owinCookieConfiguration");
            app.UseCookieAuthentication(GetCookieAuthenticationOptions(cookieConfiguration));

            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(GetOAuthAuthenticationOptions());

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            app.UseGoogleAuthentication();
        }
        #endregion

        // -- THIS SHOULD NOT BE USED FOR PRODUCTION!
        //    DEMO ONLY
        #region Public Methods
        public static OAuthAuthorizationServerOptions GetOAuthAuthenticationOptions()
        {
            if (_serverOptions == null)
            {
                var configuration = new OwinOAuthConfiguration("owinOAuthConfiguration");

                var authenticationOptions = new OAuthAuthorizationServerOptions
                {
                    TokenEndpointPath = new PathString(configuration.TokenEndpointPath),
                    Provider = new ApplicationOAuthProvider(),
                    AuthorizeEndpointPath = new PathString(configuration.AuthorizeEndpointPath),
                    AccessTokenExpireTimeSpan = TimeSpan.FromDays(configuration.AccessTokenExpireTimeInDays),
                    AllowInsecureHttp = configuration.AllowInsecureHttp
                };

                _serverOptions = authenticationOptions;
            }

            return _serverOptions;
        }
        #endregion

        #region Private Methods
        private static CookieAuthenticationOptions GetCookieAuthenticationOptions(OwinCookieConfiguration configuration)
        {
            var authenticationOptions = new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                ExpireTimeSpan = TimeSpan.FromDays(configuration.ExpireTimeInDays),
                CookieHttpOnly = configuration.CookieHttpOnly,
                CookieSecure = configuration.CookieSecure
                    ? CookieSecureOption.Always
                    : CookieSecureOption.SameAsRequest
            };

            if (!string.IsNullOrWhiteSpace(configuration.Name))
            {
                authenticationOptions.CookieName = configuration.Name;
            }

            if (!string.IsNullOrWhiteSpace(configuration.Path))
            {
                authenticationOptions.CookiePath = configuration.Path;
            }

            if (!string.IsNullOrWhiteSpace(configuration.LoginPath))
            {
                authenticationOptions.LoginPath = new PathString(configuration.LoginPath);
            }

            if (!string.IsNullOrWhiteSpace(configuration.ReturnUrlParameter))
            {
                authenticationOptions.ReturnUrlParameter = configuration.ReturnUrlParameter;
            }

            if (!string.IsNullOrWhiteSpace(configuration.Domain))
            {
                authenticationOptions.CookieDomain = configuration.Domain;
            }

            return authenticationOptions;
        }
        #endregion
    }
}
