﻿using StartupApp.Dependency.Ninject;
using StartupApp.Web.App;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DependencyConfig), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DependencyConfig), "Stop")]

// -- All Dependency Injection implementations should resolve in the Dependency project.
namespace StartupApp.Web.App
{
    public static class DependencyConfig
    {
        #region Public Methods
        public static void Start()
        {
            NinjectWebBootstrap.Start();
        }

        public static void Stop()
        {
            NinjectWebBootstrap.Stop();
        }
        #endregion
    }
}
