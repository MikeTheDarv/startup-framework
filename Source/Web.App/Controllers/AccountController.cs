﻿using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using StartupApp.Common.Exceptions;
using StartupApp.Domain.Model;
using StartupApp.Services.Domain.Interfaces;
using StartupApp.Services.System.Interfaces;
using StartupApp.Utility.WebRoles.Mvc;
using StartupApp.Utility.WebRoles.Mvc.Attributes;
using StartupApp.Utility.WebRoles.Mvc.System;
using StartupApp.Web.App.Models.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Startup.Framework.Validation;

namespace StartupApp.Web.App.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        #region Private Variables
        private readonly IAuthenticationService _authenticationService;
        private readonly IRegistrationService _registrationService;
        private readonly IUserDomainService _userDomainService;
        private readonly IModelDomainService<UserLoginIdentity> _userLoginIdentityDomainService;
        #endregion

        #region Constructors
        public AccountController(IRegistrationService registrationService, IAuthenticationService authenticationService, IUserDomainService userDomainService, IModelDomainService<UserLoginIdentity> userLoginIdentityDomainService)
        {
            _registrationService = registrationService;
            _authenticationService = authenticationService;
            _userDomainService = userDomainService;
            _userLoginIdentityDomainService = userLoginIdentityDomainService;
        }
        #endregion

        #region Actions
        // GET: /Account/Register
        [AllowAnonymous, OnlyAnonymous("Manage", "Account")]
        public ActionResult Register()
        {
            return View("Register");
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous, OnlyAnonymous("Manage", "Account")]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    EmailAddress = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                };

                using (var uow = UnitOfWork())
                {
                    try
                    {
                        // -- Register user
                        _registrationService.Register(user, model.Password);

                        uow.Commit();

                        // -- Authenticate
                        _authenticationService.Authenticate(model.EmailAddress, model.Password, false);

                        return RedirectToAction("Index", "Home");
                    }
                    catch (ValidationException e)
                    {
                        uow.Rollback();

                        ModelState.AddValidationErrors(e.ValidationErrors);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Register", model);
        }

        // GET: /Account/ExternalRegister
        [AllowAnonymous, OnlyAnonymous("Manage", "Account")]
        public async Task<ActionResult> ExternalRegister(string returnUrl)
        {
            var owinContext = HttpContext.GetOwinContext().Authentication;
            var loginInfo = owinContext.GetExternalLoginInfo();
            var externalLogin = await owinContext.GetExternalIdentityAsync(DefaultAuthenticationTypes.ExternalCookie);

            if (loginInfo == null)
            {
                return RedirectToAction("Login", "Session");
            }

            ViewBag.ReturnUrl = returnUrl;
            ViewBag.LoginProvider = loginInfo.Login.LoginProvider;

            return View("ExternalRegister", new ExternalRegisterViewModel
            {
                EmailAddress = externalLogin.FindFirstValue(ClaimTypes.Email) ?? string.Empty,
                FirstName = externalLogin.FindFirstValue(ClaimTypes.GivenName) ?? string.Empty,
                LastName = externalLogin.FindFirstValue(ClaimTypes.Surname) ?? string.Empty
            });
        }
        
        // POST: /Account/ExternalRegister
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous, OnlyAnonymous("Manage", "Account")]
        public async Task<ActionResult> ExternalRegister(ExternalRegisterViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var owinContext = HttpContext.GetOwinContext().Authentication;
                var info = await owinContext.GetExternalLoginInfoAsync();

                if (info == null)
                {
                    return View("ExternalRegisterFailure");
                }

                var user = new User
                {
                    EmailAddress = model.EmailAddress,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                };

                user.LoginIdentities.Add(new UserLoginIdentity
                {
                    LoginProvider = info.Login.LoginProvider,
                    ProviderKey = info.Login.ProviderKey
                });

                using (var uow = UnitOfWork())
                {
                    try
                    {
                        // -- Register user
                        _registrationService.Register(user, string.Empty);

                        uow.Commit();

                        // -- Authenticate
                        _authenticationService.AuthenticateExternal(info.Login.LoginProvider, info.Login.ProviderKey, false);

                        return RedirectToLocal(returnUrl);
                    }
                    catch (ValidationException e)
                    {
                        uow.Rollback();

                        ModelState.AddValidationErrors(e.ValidationErrors, error => string.Empty);
                    }
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View("ExternalRegister", model);
        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous, OnlyAnonymous]
        public ActionResult ForgotPassword(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("ForgotPassword");
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous, OnlyAnonymous]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                using (var uow = UnitOfWork())
                {
                    try
                    {
                        // -- Reset password
                        _registrationService.ForgotPassword(model.EmailAddress);

                        uow.Commit();

                        TempData["ForgotStatus"] =
                            "Your password has been reset.  You should receive an email with a temporary password shortly.";

                        return RedirectToAction("Login", "Session", new {ReturnUrl = returnUrl});
                    }
                    catch (ArgumentException ae)
                    {
                        uow.Rollback();

                        ModelState.AddModelError("EmailAddress", ae.Message);
                    }
                    catch (ValidationException e)
                    {
                        uow.Rollback();

                        ModelState.AddValidationErrors(e.ValidationErrors);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View("ForgotPassword", model);
        }

        // GET: /Account/ResetPassword
        public ActionResult ResetPassword(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View("ResetPassword");
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                using (var uow = UnitOfWork())
                {
                    try
                    {
                        var identity = GetIdentity();

                        // -- Reset password
                        _userDomainService.ChangePassword(identity.Id, model.TemporaryPassword, model.Password);

                        uow.Commit();

                        // -- Force the cache to refresh
                        StaleIdentity();

                        return RedirectToLocal(returnUrl);
                    }
                    catch (ArgumentException ae)
                    {
                        uow.Rollback();

                        ModelState.AddModelError("Password", ae.Message);
                    }
                    catch (ChangePasswordException e)
                    {
                        uow.Rollback();

                        ModelState.AddModelError("TemporaryPassword", e.Message);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View("ResetPassword", model);
        }

        // GET: /Account/Activate
        [AllowAnonymous]
        public ActionResult Activate(string emailAddress, string activationCode)
        {
            const string ERROR_MESSAGE = "Could not activate email address with provided activation code.";

            Guid activationCodeGuid;
            if (!Guid.TryParse(activationCode, out activationCodeGuid))
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, ERROR_MESSAGE);
            }

            var userId = _userDomainService.SelectWhere(u => u.EmailAddress == emailAddress, u => u.Id);
            if (userId == default(int))
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, ERROR_MESSAGE);
            }

            using (var uow = UnitOfWork())
            {
                try
                {
                    var activationSuccessful = _userDomainService.Activate(userId, activationCodeGuid);

                    uow.Commit();

                    TempData["StatusMessage"] = activationSuccessful
                        ? "Thank you for activating your email address."
                        : ERROR_MESSAGE;

                    if (User.Identity.IsAuthenticated)
                    {
                        return RedirectToAction("Manage");
                    }

                    return RedirectToAction("Login", "Session", new {ReturnUrl = Url.Action("Manage")});
                }

                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        // GET: /Account/Manage
        public ActionResult Manage()
        {
            ViewBag.StatusMessage = TempData["StatusMessage"] as string ?? string.Empty;
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");

            return View("Manage");
        }

        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ManageViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (!hasPassword)
            {
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }
            }

            if (ModelState.IsValid)
            {
                using (var uow = UnitOfWork())
                {
                    try
                    {
                        var identity = GetIdentity();

                        _userDomainService.ChangePassword(identity.Id, model.OldPassword ?? string.Empty, model.NewPassword);

                        uow.Commit();

                        SetManageStatus("Your password has been changed.");
                        return RedirectToAction("Manage");
                    }
                    catch (ChangePasswordException e)
                    {
                        uow.Rollback();
                        ModelState.AddModelError(string.Empty, e.Message);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View("Manage", model);
        }

        // GET: /Account/AssociateIdentity
        public async Task<ActionResult> AssociateIdentity()
        {
            var identity = GetIdentity();
            var owinContext = HttpContext.GetOwinContext().Authentication;
            var loginInfo = await owinContext.GetExternalLoginInfoAsync(HttpChallengeResult.XsrfKey, identity.Id.ToString());

            if (loginInfo == null)
            {
                SetManageStatus("An error has occurred.");
                return RedirectToAction("Manage");
            }

            var userIdentity = new UserLoginIdentity
            {
                LoginProvider = loginInfo.Login.LoginProvider,
                ProviderKey = loginInfo.Login.ProviderKey
            };

            using (var uow = UnitOfWork())
            {
                try
                {
                    // -- Add identity
                    _userDomainService.AddLoginIdentity(identity.Id, userIdentity);

                    uow.Commit();
                }
                catch (ValidationException)
                {
                    uow.Rollback();
                    
                    SetManageStatus("An error has occurred.");
                }
            }

            return RedirectToAction("Manage");
        }

        // POST: /Account/AssociateIdentity
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssociateIdentity(string provider)
        {
            var identity = GetIdentity();

            // Request a redirect to the external login provider to link a login for the current user
            return new HttpChallengeResult(provider, Url.Action("AssociateIdentity", "Account"), identity.Id.ToString());
        }

        // POST: /Account/DisassociateIdentity
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DisassociateIdentity(string loginProvider, string providerKey)
        {
            var identity = GetIdentity();

            _userDomainService.RemoveIdentityLogin(identity.Id, loginProvider, providerKey);

            SetManageStatus("The external login was removed.");
            return RedirectToAction("Manage");
        }

        private void SetManageStatus(string status)
        {
            TempData["StatusMessage"] = status;
        }

        #endregion

        #region Child Actions
        [ChildActionOnly]
        public ActionResult RegisteredExternalLogins()
        {
            var identity = GetIdentity();
            var linkedAccounts =
                _userLoginIdentityDomainService.SelectAllWhere(id => id.User.Id == identity.Id, id => id).ToList();

            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;

            return PartialView("_RegisteredExternalLoginsPartial", linkedAccounts);
        }
        #endregion

        #region Private Methods
        private bool HasPassword()
        {
            var userId = User.Identity.GetUserId();
            var passwordHash = _userDomainService.SelectWhere(u => u.EmailAddress == userId, u => u.PasswordHash) ?? string.Empty;
            return !string.IsNullOrWhiteSpace(passwordHash);
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
        #endregion
    }
}
