﻿using System.Web.Mvc;
using StartupApp.Utility.WebRoles.Mvc;

namespace StartupApp.Web.App.Controllers
{
    public class HomeController : BaseController
    {
        #region Actions
        // GET: /Home/Index
        public ActionResult Index()
        {
            return View("Index");
        }

        // GET: /Home/About
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("About");
        }

        // GET: /Home/Contact
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("Contact");
        }
        #endregion
    }
}
