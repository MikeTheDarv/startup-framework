﻿using System.Web;
using System.Web.Mvc;
using StartupApp.Common.Exceptions;
using StartupApp.Services.System.Interfaces;
using StartupApp.Utility.WebRoles.Mvc;
using StartupApp.Utility.WebRoles.Mvc.Attributes;
using StartupApp.Utility.WebRoles.Mvc.System;
using StartupApp.Web.App.Models.ViewModels;
using Microsoft.Owin.Security;

namespace StartupApp.Web.App.Controllers
{
    [Authorize]
    public class SessionController : BaseController
    {
        #region Private Variables
        private readonly IAuthenticationService _authenticationService;
        #endregion

        #region Constructors
        public SessionController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }
        #endregion

        #region Actions
        // GET: /Session/Login
        [AllowAnonymous, OnlyAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.StatusMessage = TempData["ForgotStatus"] as string ?? string.Empty;
            ViewBag.ReturnUrl = returnUrl;
            return View("Login");
        }

        // POST: /Session/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous, OnlyAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _authenticationService.Authenticate(model.UserName, model.Password, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                catch (LoginException e)
                {
                    ModelState.AddModelError(string.Empty, e.Message);
                }
            }
            
            // If we got this far, something failed, redisplay form
            return View("Login", model);
        }

        // GET: /Session/ExternalLogin
        [AllowAnonymous, OnlyAnonymous]
        public ActionResult ExternalLogin(string returnUrl)
        {
            var owinContext = HttpContext.GetOwinContext().Authentication;
            var loginInfo = owinContext.GetExternalLoginInfo();
            
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            try
            {
                _authenticationService.AuthenticateExternal(loginInfo.Login.LoginProvider, loginInfo.Login.ProviderKey, false);
                return RedirectToLocal(returnUrl);
            }
            catch (LoginException)
            {
                return RedirectToAction("ExternalRegister", "Account");
            }
        }

        // POST: /Session/ExternalLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous, OnlyAnonymous]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new HttpChallengeResult(provider, Url.Action("ExternalLogin", "Session", new { ReturnUrl = returnUrl }));
        }

        // POST: /Session/Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            _authenticationService.Invalidate();
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Private Methods
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }
        #endregion
    }
}
