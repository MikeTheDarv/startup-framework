﻿using StartupApp.Domain.Model;
using Startup.Framework.Net;

namespace StartupApp.Domain.DataTransferObjects.ServiceObjects
{
    public class UserRegisteredEmailDto
    {
        #region Properties
        public User User { get; set; }

        [EmailMessageToken("ActivationUrl")]
        public string ActivationUrl { get; set; }
        #endregion
    }
}
