﻿using StartupApp.Domain.Model;
using Startup.Framework.Net;

namespace StartupApp.Domain.DataTransferObjects.ServiceObjects
{
    public class UserForgotPasswordEmailDto
    {
        public User User { get; set; }

        [EmailMessageToken("TemporaryPassword")]
        public string TemporaryPassword { get; set; }
    }
}
