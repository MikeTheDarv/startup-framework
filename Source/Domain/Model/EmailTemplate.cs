﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using StartupApp.Common.Enums;
using Startup.Framework.Net.Impl;
using Startup.Framework.Net.Interfaces;

namespace StartupApp.Domain.Model
{
    public class EmailTemplate : ModelBase<int>
    {
        #region Constants
        private readonly char[] _emailAddressSeperators = { ';', '|', ',' };
        #endregion

        #region Properties
        [Required, MaxLength(200)]
        public virtual string Name { get; set; }

        [Required, MaxLength(500)]
        public virtual string Description { get; set; }

        [MaxLength(1000)]
        public virtual string BccList { get; set; }

        [MaxLength(1000)]
        public virtual string CcList { get; set; }

        public virtual bool IsBodyHtml { get; set; }

        public virtual string MailBodyHtml { get; set; }

        public virtual string MailBodyText { get; set; }

        [Required, MaxLength(1000)]
        public virtual string MailFrom { get; set; }

        [MaxLength(1000)]
        public virtual string ReplyTo { get; set; }

        public virtual MailPriority MailPriority { get; set; }

        [MaxLength(250)]
        public virtual string MailSubject { get; set; }

        [Required, MaxLength(1000)]
        public virtual string MailTo { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual ApplicationEmailType EmailType { get; set; }
        #endregion

        #region Constructors
        public EmailTemplate()
        {
            MailPriority = MailPriority.Normal;
            IsActive = true;
            IsBodyHtml = true;
            MailTo = string.Empty;
            MailSubject = String.Empty;
            ReplyTo = string.Empty;
            MailFrom = String.Empty;
            MailBodyText = String.Empty;
            MailBodyHtml = String.Empty;
            CcList = string.Empty;
            BccList = string.Empty;
            Description = string.Empty;
            Name = string.Empty;
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return string.Format("'{0}': {1}", Name, Description);
        }
        #endregion

        #region Public Methods
        public virtual ITokenizedEmailMessage ToTokenizedEmailMessage()
        {
            ITokenizedEmailMessage emailMessage = new TokenizedEmailMessage();
            MapTokenizedMessage(emailMessage);
            return emailMessage;
        }

        public virtual ITokenizedEmailMessage ToTokenizedEmailMessage(object payload)
        {
            ITokenizedEmailMessage emailMessage = new TokenizedEmailMessage(payload);
            MapTokenizedMessage(emailMessage);
            return emailMessage;
        }
        #endregion

        #region Private Methods
        private void MapTokenizedMessage(ITokenizedEmailMessage emailMessage)
        {
            emailMessage.SetFrom(MailFrom);

            foreach (var emailAddress in MailTo.Split(_emailAddressSeperators, StringSplitOptions.RemoveEmptyEntries))
            {
                emailMessage.AddTo(emailAddress);
            }

            foreach (var emailAddress in CcList.Split(_emailAddressSeperators, StringSplitOptions.RemoveEmptyEntries))
            {
                emailMessage.AddCc(emailAddress);
            }

            foreach (var emailAddress in BccList.Split(_emailAddressSeperators, StringSplitOptions.RemoveEmptyEntries))
            {
                emailMessage.AddBcc(emailAddress);
            }

            foreach (var emailAddress in ReplyTo.Split(_emailAddressSeperators, StringSplitOptions.RemoveEmptyEntries))
            {
                emailMessage.AddReplyTo(emailAddress);
            }

            emailMessage.BodyHtml = MailBodyHtml;
            emailMessage.BodyPlainText = MailBodyText;
            emailMessage.IsBodyHtml = IsBodyHtml;
            emailMessage.Priority = MailPriority;
            emailMessage.Subject = MailSubject;
        }
        #endregion
    }
}
