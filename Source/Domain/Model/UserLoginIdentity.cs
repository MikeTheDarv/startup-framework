﻿using System.ComponentModel.DataAnnotations;

namespace StartupApp.Domain.Model
{
    public class UserLoginIdentity : ModelBase<int>
    {
        #region Properties
        [Required, MaxLength(50)]
        public virtual string LoginProvider { get; set; }

        [Required, MaxLength(256)]
        public virtual string ProviderKey { get; set; }

        public virtual User User { get; set; } 
        #endregion

        #region Constructors
        public UserLoginIdentity()
        {
            LoginProvider = string.Empty;
            ProviderKey = string.Empty;
        }
        #endregion
    }
}
