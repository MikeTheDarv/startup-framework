﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Startup.Framework;
using Startup.Framework.Auditing.Interfaces;
using Startup.Framework.Domain.Impl;
using Startup.Framework.Domain.Interfaces;

namespace StartupApp.Domain.Model
{
    public interface IModelBase
    {

    }

    public interface IModelBase<TPrimaryKey> : IEntityWithTypedId<TPrimaryKey>, IAuditable<TPrimaryKey>, IModelBase
    {
        int Version { get; }
    }

    public abstract class ModelBase<TPrimaryKey> : EntityWithTypedId<TPrimaryKey>, IModelBase<TPrimaryKey>
    {
        #region Private Variables
        private bool _auditEnabledForCreated = true;
        private bool _auditEnabledForModifiedLast = true;
        private DateTime _createdUtc = PrimitiveConstants.NULL_DATE;
        private DateTime _modifiedLastUtc = PrimitiveConstants.NULL_DATE;
        #endregion

        #region Constructors
        protected ModelBase()
        {

        }

        protected ModelBase(TPrimaryKey defaultId)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            Id = defaultId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }
        #endregion

        #region Properties
// ReSharper disable once UnusedAutoPropertyAccessor.Global
        public virtual int Version { get; protected set; }

        [NotMapped]
        public virtual bool AuditEnabledForCreated
        {
            get { return _auditEnabledForCreated; }
            protected set { _auditEnabledForCreated = value; }
        }

        [NotMapped]
        public virtual bool AuditEnabledForModifiedLast
        {
            get { return _auditEnabledForModifiedLast; }
            set { _auditEnabledForModifiedLast = value; }
        }

        public virtual DateTime CreatedUtc
        {
            get { return _createdUtc; }
            set { _createdUtc = value; }
        }

        public virtual TPrimaryKey CreatedBy { get; set; }

        public virtual DateTime ModifiedLastUtc
        {
            get { return _modifiedLastUtc; }
            set { _modifiedLastUtc = value; }
        }

        public virtual TPrimaryKey ModifiedLastBy { get; set; }

        #endregion
    }
}
