﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using StartupApp.Common.Constants;
using StartupApp.Common.Enums;
using Startup.Framework.Net;

namespace StartupApp.Domain.Model
{
    public class User : ModelBase<int>
    {
        #region Private Variables
        private string _emailAddress = string.Empty;
        #endregion

        #region Properties
        [Required, MaxLength(100), EmailMessageToken("UserName")]
        public virtual string UserName { get; private set; }

        [MaxLength(512)]
        public virtual string PasswordHash { get; set; }

        [MaxLength(128)]
        public virtual string PasswordSalt { get; set; }

        [Required, MaxLength(50), EmailMessageToken("FirstName")]
        public virtual string FirstName { get; set; }

        [Required, MaxLength(50), EmailMessageToken("LastName")]
        public virtual string LastName { get; set; }

        [Required, MaxLength(100), EmailAddress, EmailMessageToken("EmailAddress")]
        public virtual string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
                UserName = value;
            }
        }

        public virtual bool IsActive { get; set; }

        public virtual bool IsActivated { get; set; }

        public virtual Guid ActivationCode { get; set; }

        public virtual bool IsSystemProtected { get; set; }

        public virtual bool IsPendingPasswordReset { get; set; }

        public virtual ApplicationUserType UserType { get; set; }

        [MaxLength(100)]
        public virtual string AvatarFileIdentifier { get; set; }

        public virtual ICollection<UserLoginIdentity> LoginIdentities { get; private set; }

        public virtual TimeZoneInfo TimeZone
        {
            get { return TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId); }
            set { TimeZoneId = value.Id; }
        }
        #endregion

        #region Internal Properties
        [MaxLength(100)]
        public virtual string TimeZoneId { get; private set; }
        #endregion

        #region Constructors
        public User() : base(UserConstants.ANONYMOUS_USER_ID)
        {
            UserName = string.Empty;
            PasswordHash = string.Empty;
            PasswordSalt = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            UserType = ApplicationUserType.Null;
            AvatarFileIdentifier = string.Empty;
            TimeZoneId = TimeZoneInfo.Local.Id;
            LoginIdentities = new HashSet<UserLoginIdentity>();
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }

        public override bool IsTransient()
        {
            return base.IsTransient() || Id == UserConstants.ANONYMOUS_USER_ID;
        }
        #endregion
    }
}
