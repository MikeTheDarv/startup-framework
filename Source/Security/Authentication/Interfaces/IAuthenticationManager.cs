﻿using System.Security.Claims;
using StartupApp.Domain.Model;

namespace StartupApp.Security.Authentication.Interfaces
{
    public interface IAuthenticationManager
    {
        ClaimsIdentity PersistPrincipal(User user, bool persistent);
        void RevokePrincipal();
    }
}
