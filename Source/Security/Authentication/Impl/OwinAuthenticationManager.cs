﻿using System.Security.Claims;
using System.Web;
using StartupApp.Common.Constants;
using StartupApp.Domain.Model;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using IAuthenticationManager = StartupApp.Security.Authentication.Interfaces.IAuthenticationManager;

namespace StartupApp.Security.Authentication.Impl
{
    public class OwinAuthenticationManager : IAuthenticationManager
    {
        #region Implementation of IPrincipalStorageProvider
        public ClaimsIdentity PersistPrincipal(User user, bool persistent)
        {
            var appClaimsIdentity = CreateClaimsIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

            var properties = new AuthenticationProperties { IsPersistent = persistent };
            HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            HttpContext.Current.GetOwinContext().Authentication.SignIn(properties, appClaimsIdentity);

            return appClaimsIdentity;
        }
        
        public void RevokePrincipal()
        {
            HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
        #endregion

        #region Private Methods
        private static ClaimsIdentity CreateClaimsIdentity(User user, string authenticationType)
        {
            var claimsIdentity = new ClaimsIdentity(authenticationType, ClaimTypes.Name, ClaimTypes.Role);
            claimsIdentity.AddClaim(new Claim(ClaimIdentityConstants.IDENTITY_PROVIDER, "LocalAuth", ClaimValueTypes.String));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, string.Format("{0} {1}", user.FirstName, user.LastName), ClaimValueTypes.String));
            claimsIdentity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserName, ClaimValueTypes.String));
            return claimsIdentity;
        }
        #endregion
    }
}
