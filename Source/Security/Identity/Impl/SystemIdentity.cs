﻿using StartupApp.Common.Enums;
using StartupApp.Domain.Model;
using StartupApp.Security.Identity.Interfaces;

namespace StartupApp.Security.Identity.Impl
{
    public class SystemIdentity : ISystemIdentity
    {
        #region Implementation of ISystemIdentity
        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string AvatarFileIdentifier { get; set; }

        public string TimeZoneId { get; set; }

        public ApplicationUserType UserType { get; set; }

        public bool IsPendingPasswordReset { get; set; }
        #endregion

        #region Constructors
        public SystemIdentity(User user)
        {
            Id = user.Id;
            Name = string.Format("{0} {1}", user.FirstName, user.LastName);
            UserName = user.UserName;
            EmailAddress = user.EmailAddress;
            AvatarFileIdentifier = user.AvatarFileIdentifier;
            TimeZoneId = user.TimeZoneId;
            UserType = user.UserType;
            IsPendingPasswordReset = user.IsPendingPasswordReset;
        }
        #endregion
    }
}
