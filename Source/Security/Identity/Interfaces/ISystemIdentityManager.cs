﻿namespace StartupApp.Security.Identity.Interfaces
{
    /// <summary>
    /// Load the current system identity from the
    /// database, cache, or cookie and populate the
    /// identity store.
    /// </summary>
    public interface ISystemIdentityManager
    {
        void InitializeIdentityStore();
    }
}
