﻿namespace StartupApp.Security.Identity.Interfaces
{
    /// <summary>
    /// Represents a local cache for services and controllers
    /// for retrieving the system identity.
    /// </summary>
    public interface ISystemIdentityStore
    {
        ISystemIdentity GetIdentity();
        void StoreIdentity(ISystemIdentity identity);
        void StaleIdentity();
    }
}
