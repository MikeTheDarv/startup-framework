﻿using StartupApp.Common.Enums;

namespace StartupApp.Security.Identity.Interfaces
{
    public interface ISystemIdentity
    {
        #region Properties
        int Id { get; set; }

        string UserName { get; set; }

        string EmailAddress { get; set; }

        string AvatarFileIdentifier { get; set; }

        ApplicationUserType UserType { get; set; }

        bool IsPendingPasswordReset { get; set; }
        #endregion
    }
}
