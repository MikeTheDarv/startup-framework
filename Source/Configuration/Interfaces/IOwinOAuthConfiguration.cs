﻿namespace StartupApp.Configuration.Interfaces
{
    public interface IOwinOAuthConfiguration
    {
        string TokenEndpointPath { get; }
        string AuthorizeEndpointPath { get; }
        int AccessTokenExpireTimeInDays { get; }
        bool AllowInsecureHttp { get; }
    }
}
