﻿namespace StartupApp.Configuration.Interfaces
{
    public interface IOwinCookieConfiguration
    {
        string Name { get; }
        string Path { get; }
        string LoginPath { get; }
        string ReturnUrlParameter { get; }
        string Domain { get; }
        int ExpireTimeInDays { get; }
        bool CookieHttpOnly { get; }
        bool CookieSecure { get; }
    }
}
