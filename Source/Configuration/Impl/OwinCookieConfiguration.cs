﻿using System;
using System.Configuration;
using StartupApp.Configuration.Interfaces;

namespace StartupApp.Configuration.Impl
{
    public class OwinCookieConfiguration : ConfigurationSection, IOwinCookieConfiguration
    {
        #region Properties

        [ConfigurationProperty("name", DefaultValue = "")]
        public string Name
        {
            get { return this["name"] as string ?? string.Empty; }
            private set { this["name"] = value; }
        }

        [ConfigurationProperty("path", DefaultValue = "")]
        public string Path
        {
            get { return this["path"] as string ?? string.Empty; }
            private set { this["path"] = value; }
        }

        [ConfigurationProperty("loginPath", DefaultValue = "")]
        public string LoginPath
        {
            get { return this["loginPath"] as string ?? string.Empty; }
            private set { this["loginPath"] = value; }
        }

        [ConfigurationProperty("returnUrlParameter", DefaultValue = "")]
        public string ReturnUrlParameter
        {
            get { return this["returnUrlParameter"] as string ?? string.Empty; }
            private set { this["returnUrlParameter"] = value; }
        }

        [ConfigurationProperty("domain", DefaultValue = "")]
        public string Domain
        {
            get { return this["domain"] as string ?? string.Empty; }
            private set { this["domain"] = value; }
        }

        [ConfigurationProperty("expireTimeInDays", DefaultValue = "14")]
        public int ExpireTimeInDays
        {
            get { return Convert.ToInt32(this["expireTimeInDays"] as string ?? "14"); }
            private set { this["expireTimeInDays"] = value; }
        }

        [ConfigurationProperty("cookieHttpOnly", DefaultValue = "true")]
        public bool CookieHttpOnly
        {
            get { return Convert.ToBoolean(this["cookieHttpOnly"] as string ?? "true"); }
            private set { this["cookieHttpOnly"] = value; }
        }

        [ConfigurationProperty("cookieSecure", DefaultValue = "false")]
        public bool CookieSecure
        {
            get { return Convert.ToBoolean(this["cookieSecure"] as string ?? "false"); }
            private set { this["cookieSecure"] = value; }
        }
        #endregion

        #region Constructors
        protected OwinCookieConfiguration()
        {
            
        }

        public OwinCookieConfiguration(string sectionName)
        {
            var configuration = (OwinCookieConfiguration)ConfigurationManager.GetSection(sectionName);

            Name = configuration.Name;
            Path = configuration.Path;
            LoginPath = configuration.LoginPath;
            ReturnUrlParameter = configuration.ReturnUrlParameter;
            Domain = configuration.Domain;
            ExpireTimeInDays = configuration.ExpireTimeInDays;
            CookieHttpOnly = configuration.CookieHttpOnly;
            CookieSecure = configuration.CookieSecure;
        }
        #endregion
    }
}
