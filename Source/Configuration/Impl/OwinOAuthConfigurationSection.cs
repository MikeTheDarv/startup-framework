﻿using System;
using System.Configuration;
using StartupApp.Configuration.Interfaces;

namespace StartupApp.Configuration.Impl
{
    public class OwinOAuthConfiguration : ConfigurationSection, IOwinOAuthConfiguration
    {
        #region Properties
        [ConfigurationProperty("tokenEndpointPath", DefaultValue = "/token/", IsRequired = true)]
        public string TokenEndpointPath
        {
            get { return this["tokenEndpointPath"] as string ?? "/token/"; }
            private set { this["tokenEndpointPath"] = value; }
        }

        [ConfigurationProperty("authorizeEndpointPath", DefaultValue = "/api/session/externallogin/", IsRequired = true)]
        public string AuthorizeEndpointPath
        {
            get { return this["authorizeEndpointPath"] as string ?? "/api/session/externallogin/"; }
            private set { this["authorizeEndpointPath"] = value; }
        }

        [ConfigurationProperty("accessTokenExpireTimeInDays", DefaultValue = "14")]
        public int AccessTokenExpireTimeInDays
        {
            get { return Convert.ToInt32(this["accessTokenExpireTimeInDays"] as string ?? "14"); }
            private set { this["accessTokenExpireTimeInDays"] = value; }
        }

        [ConfigurationProperty("allowInsecureHttp", DefaultValue = "true")]
        public bool AllowInsecureHttp
        {
            get { return Convert.ToBoolean(this["allowInsecureHttp"] as string ?? "true"); }
            private set { this["allowInsecureHttp"] = value; }
        }
        #endregion

        #region Constructors
        protected OwinOAuthConfiguration()
        {
            
        }

        public OwinOAuthConfiguration(string sectionName)
        {
            var configuration = (OwinOAuthConfiguration)ConfigurationManager.GetSection(sectionName);
            TokenEndpointPath = configuration.TokenEndpointPath;
            AuthorizeEndpointPath = configuration.AuthorizeEndpointPath;
            AccessTokenExpireTimeInDays = configuration.AccessTokenExpireTimeInDays;
            AllowInsecureHttp = configuration.AllowInsecureHttp;
        }
        #endregion
    }
}
