﻿using System;
using System.Data.Entity;
using System.Linq;
using Startup.Framework.Auditing.Interfaces;

namespace Startup.Framework.Impl.EntityFramework.Auditing
{
    public abstract class AuditableDbContext<TPrimaryKey> : DbContext
    {
        #region Constructors

        protected AuditableDbContext()
        {
            
        }

        protected AuditableDbContext(string connectionString) : base(connectionString)
        {
            
        } 
        #endregion

        #region Overrides
        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries().Where(p => p.Entity is IAuditable<TPrimaryKey> &&
                                                              (p.State == EntityState.Added ||
                                                               p.State == EntityState.Modified));
            
            foreach (var entity in entities)
            {
                var auditableEntity = (IAuditable<TPrimaryKey>) entity.Entity;

                // -- Insert
                if (auditableEntity.AuditEnabledForCreated && entity.State == EntityState.Added)
                {
                    auditableEntity.CreatedBy = GetCurrentIdentity();
                    auditableEntity.CreatedUtc = DateTime.UtcNow;
                }

                // -- Update
                if (auditableEntity.AuditEnabledForModifiedLast)
                {
                    auditableEntity.ModifiedLastBy = GetCurrentIdentity();
                    auditableEntity.ModifiedLastUtc = DateTime.UtcNow;
                }
            }

            return base.SaveChanges();
        }
        #endregion

        #region Abstract Methods
        public abstract TPrimaryKey GetCurrentIdentity();
        #endregion
    }
}
