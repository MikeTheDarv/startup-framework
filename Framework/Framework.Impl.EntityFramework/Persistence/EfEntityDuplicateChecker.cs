﻿using System;
using Startup.Framework.Domain.Interfaces;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.EntityFramework.Persistence
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// </summary>
    public class EfEntityDuplicateChecker : IEntityDuplicateChecker
    {
        /// <summary>
        /// Provides a behavior specific repository for checking if a duplicate exists of an existing entity.
        /// </summary>

        #region Implementation of IEntityDuplicateChecker
        public bool DoesDuplicateExistWithTypedIdOf<TId>(IEntityWithTypedId<TId> entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            throw new NotImplementedException();
        }
        #endregion
    }
}
