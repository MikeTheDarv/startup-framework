﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Startup.Framework.Domain.Interfaces;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.EntityFramework.Persistence
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EfRepository<T> : EfRepositoryWithTypedId<T, int>, IRepository<T> where T : class, IEntityWithTypedId<int>
    {
        public EfRepository(DbContext dbContext) : base(dbContext) { }
    }

    public class EfRepositoryWithTypedId<T, TId> : IRepositoryWithTypedId<T, TId> where T : class, IEntityWithTypedId<TId>
    {
        #region Private Variables
        private readonly DbContext _dbContext;
        private readonly IDbSet<T> _dbSet;
        #endregion

        #region Constructors
        public EfRepositoryWithTypedId(DbContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException("dbContext");

            _dbContext = dbContext;
            _dbSet = dbContext.Set<T>();
        }
        #endregion

        #region Implementation of IRepository
        public virtual IUnitOfWork UnitOfWork
        {
            get
            {
                return new EfUnitOfWork(_dbContext);
            }
        }

        public virtual T Select(TId id)
        {
            return _dbSet.Single(entity => id.Equals(entity.Id));
        }

        public T Proxy(TId id)
        {
            return LoadProxy(id);
        }

        public virtual IEnumerable<T> SelectAll()
        {
            return QueryAll().ToList();
        }

        public virtual IQueryable<T> QueryAll()
        {
            return _dbSet;
        }

        public int ExecuteQuery(string query, Dictionary<string, object> parameters, Dictionary<string, IEnumerable> parameterLists)
        {

            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.AddRange(parameters.Select(p => new SqlParameter(p.Key, p.Value)));
            sqlParams.AddRange(parameterLists.Select(p => new SqlParameter(p.Key, p.Value)));

            return _dbContext.Database.ExecuteSqlCommand(query, sqlParams);
        }

        public virtual T SaveOrUpdate(T entity)
        {
            if (entity == null) return null;

            if (entity.IsTransient())
            {
                _dbSet.Add(entity);
            }
            else
            {
                _dbSet.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            }

            return entity;
        }

        /// <summary>
        /// This deletes the object and commits the deletion immediately. We don't want to delay deletion
        /// until a transaction commits, as it may throw a foreign key constraint exception which we could
        /// likely handle and inform the user about. Accordingly, this tries to delete right away; if there
        /// is a foreign key constraint preventing the deletion, an exception will be thrown.
        /// </summary>
        public virtual void Delete(T entity)
        {
            _dbSet.Remove(entity);
            _dbContext.SaveChanges();
        }
        #endregion

        #region Private Methods

        private T LoadProxy(TId id)
        {
            // -- Attempt to load from 1st or 2nd level cache
            var cachedEntity = _dbContext.ChangeTracker.Entries()
                .Where(entry => ObjectContext.GetObjectType(entry.Entity.GetType()) == typeof(T))
                .SingleOrDefault(entityCtx =>
                {
                    var entityType = entityCtx.Entity.GetType();
                    var value = entityType.InvokeMember("Id", BindingFlags.GetProperty, null, entityCtx.Entity, new object[] { });
                    return value.Equals(id);
                });

            if (cachedEntity != null)
            {
                return (T)cachedEntity.Entity;
            }

            // -- Create a default instance with Id set if not found in cache
            T entityStub = (T)Activator.CreateInstance(typeof(T));
            typeof(T).InvokeMember("Id", BindingFlags.SetProperty, null, entityStub, new object[] { id });

            // -- Ensure we don't update the object we're referencing!
            _dbContext.Entry(entityStub).State = EntityState.Unchanged;

            return entityStub;
        }

        #endregion
    }

}
