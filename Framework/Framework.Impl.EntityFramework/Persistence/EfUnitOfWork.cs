﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.EntityFramework.Persistence
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// </summary>
    public class EfUnitOfWork : IUnitOfWork
    {
        #region Private Variables

        /*private static IDbTransaction _transaction;*/
        private readonly DbContext _dbContext;
        private bool _disposed;

        #endregion

        #region Constructors

        public EfUnitOfWork(DbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }

            _dbContext = dbContext;

            BeginSession();
        }

        #endregion

        #region Implementation of IUnitOfWork

        /// <summary>
        /// This isn't specific to any one DAO and flushes everything that has been
        /// changed since the last commit.
        /// </summary>
        public virtual void Flush()
        {
            SaveChanges();
        }

        public virtual void Commit()
        {
            SaveChanges();

            /*if (_transaction != null)
            {
                _transaction.Commit();
            }*/
        }

        public virtual void Rollback()
        {
            /*if (_transaction != null)
            {
                _transaction.Rollback();
            }*/
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (!_disposed)
            {
                /*if (_transaction != null)
                {
                    _transaction.Dispose();
                }*/

                _dbContext.Dispose();
            }

            _disposed = true;

            GC.SuppressFinalize(this);
        }

        #endregion

        #region Private Methods

        private void BeginSession()
        {
            if (_dbContext.Database.Connection.State == ConnectionState.Closed)
            {
                _dbContext.Database.Connection.Open();
            }

            /*_transaction = _dbContext.Database.Connection.BeginTransaction();*/
        }

        private void SaveChanges()
        {
            try
            {
                _dbContext.SaveChanges();

            }
            catch (DbEntityValidationException e)
            {
                var errorStringBilder = new StringBuilder();

                foreach (var entityValidationError in e.EntityValidationErrors)
                {
                    errorStringBilder.Append(string.Format("Entity \"{0}\" in state \"{1}\", errors:",
                        entityValidationError.Entry.Entity.GetType().Name, entityValidationError.Entry.State));

                    foreach (var error in entityValidationError.ValidationErrors)
                    {
                        errorStringBilder.Append(string.Format(" (Property: \"{0}\", Error: \"{1}\")",
                            error.PropertyName, error.ErrorMessage));
                    }
                }

                e.Data.Add("EntityValidationErrors", errorStringBilder.ToString());

                throw;
            }
        }

        #endregion
    }
}
