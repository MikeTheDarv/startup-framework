﻿using System;

namespace Startup.Framework.Persistence.Interfaces
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    ///
    ///     Note that outside of FlushChanges(), you shouldn't have to invoke this object very often.
    ///     If you're using the IUnitOfWork SessionModule HttpModule, then the transaction 
    ///     opening/committing will be taken care of for you.
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        void Flush();
        void Commit();
        void Rollback();
    }
}
