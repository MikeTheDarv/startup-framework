﻿using Startup.Framework.Domain.Interfaces;

namespace Startup.Framework.Persistence.Interfaces
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// </summary>
    public interface IEntityDuplicateChecker
    {
        bool DoesDuplicateExistWithTypedIdOf<TId>(IEntityWithTypedId<TId> entity);
    }
}
