﻿namespace Startup.Framework.Persistence.Interfaces
{
    public interface ISessionManager
    {
        void BeginSession();
        void EndSession(bool commitTransaction);
    }
}
