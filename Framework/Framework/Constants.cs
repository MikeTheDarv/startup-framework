﻿using System;

namespace Startup.Framework
{
    #region Nested Class: Primitive
    public static class PrimitiveConstants
    {
        public static readonly DateTime NULL_DATE = new DateTime(1900, 1, 1, 0, 0, 0);
    }
    #endregion
}
