﻿namespace Startup.Framework.Media
{
    public enum MediaAssetStatusType
    {
        Null = 0,
        Queued = 1,
        Scheduled = 2,
        Processing = 3,
        Finished = 4,
        Canceling = 5,
        Canceled = 6,
        Error = 7
    }
}
