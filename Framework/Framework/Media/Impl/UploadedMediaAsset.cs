﻿using Startup.Framework.Media.Interfaces;

namespace Startup.Framework.Media.Impl
{
    public class UploadedMediaAsset : IUploadedMediaAsset
    {
        #region Properties
        public bool IsComplete { get; set; }
        public double Progress { get; set; }
        public string FileIdentifier { get; set; }
        public string OriginalFileName { get; set; }
        public string FileUrl { get; set; }
        public string PreviewUrl { get; set; }
        public MediaAssetStatusType Status { get; set; }
        public int ContentLength { get; set; }
        #endregion
    }
}
