﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Startup.Framework.Media.Interfaces;

namespace Startup.Framework.Media.Impl
{
    /// <summary>
    /// Provides a good starting point for media upload, including configurable
    /// accepted file extensions, maximum file size, working directory, and 
    /// automatic Guid file renaming.
    /// </summary>
    public abstract class MediaUploaderBase : IMediaUploader
    {
        #region Private Variables
        private readonly IMediaUploaderConfiguration _configuration;
        private readonly IMediaStorage _mediaStorage;
        #endregion

        #region Constructors
        protected MediaUploaderBase(IMediaUploaderConfiguration configuration, IMediaStorage mediaStorage)
        {
            _configuration = configuration;
            _mediaStorage = mediaStorage;
        }

        #endregion

        #region Implementation of IMediaUploader
        public IEnumerable<IUploadedMediaAsset> Upload()
        {
            var metaData = GetUploadedMetaData();

            var hasError = false;

            // Provides access points for validation
            foreach (var data in metaData)
            {
                // -- Validate file extension, if invalid add a non
                //    successful status update.
                if (string.IsNullOrWhiteSpace(data.FileName) || 
                    !IsValidExtension(data.FileName))
                {
                    HandleInvalidFile(data);
                    hasError = true;
                }

                // -- Validate file doesn't exceed maximum size
                if (FileIsTooLarge(data.ContentLength))
                {
                    HandleFileExceededMaximumLength(data);
                    hasError = true;
                }
            }

            // -- If has error, don't bother processing.  Implementing class
            //    should deal with errors.
            var mediaAssets = hasError
                ? new List<IUploadedMediaAsset>()
                : new List<IUploadedMediaAsset>(ProcessUploads());

            foreach (var asset in mediaAssets)
            {
                if (asset.IsComplete)
                {
                    OnMediaAssetUploaded(asset);

                    if (_configuration.AutoComplete)
                    {
                        asset.FileIdentifier = CompleteUpload(asset.FileIdentifier);
                    }
                }
            }

            return mediaAssets;
        }

        public string CompleteUpload(string workingFileIdentifier, string fileIdentifierFormat = null)
        {
            var workingDir = FormatWorkingIdentifier(string.Empty);
            var isWorkingFile = workingFileIdentifier.ToLower().StartsWith(workingDir);

            // -- Verify it is a working file
            if (!isWorkingFile)
            {
                throw new ArgumentException("Invalid file provided.", "");
            }

            string finalIdentifier = workingFileIdentifier.Replace(workingDir, string.Empty,
                StringComparison.InvariantCultureIgnoreCase);

            if (!string.IsNullOrWhiteSpace(fileIdentifierFormat))
            {
                finalIdentifier = string.Format(fileIdentifierFormat, finalIdentifier);
            }

            _mediaStorage.MoveMedia(workingFileIdentifier, finalIdentifier);

            OnMediaAssetCompleted(finalIdentifier);

            return finalIdentifier;
        }

        public void CancelUpload(string workingFileIdentifier)
        {
            var isWorkingFile = workingFileIdentifier.ToLower().StartsWith(FormatWorkingIdentifier(string.Empty));

            // -- Only allow deletion of working files!
            if (!isWorkingFile)
            {
                throw new ArgumentException("Invalid file provided.", "");
            }

            _mediaStorage.DeleteMedia(workingFileIdentifier);

            OnMediaAssetCanceled(workingFileIdentifier);
        }
        #endregion

        #region Abstract Methods
        /// <summary>
        /// Used to retrieve the filename and upload content length
        /// of the file to be potentially uploaded.  Used for validation
        /// before upload starts.
        /// </summary>
        /// <returns></returns>
        protected abstract IEnumerable<UploadMetaData> GetUploadedMetaData();

        /// <summary>
        /// Process the current upload queue
        /// </summary>
        /// <returns></returns>
        protected abstract IEnumerable<IUploadedMediaAsset> ProcessUploads();

        /// <summary>
        /// Called when an upload is invalid (either invalid file type,
        /// length == 0, etc)
        /// </summary>
        /// <param name="metaData"></param>
        protected abstract void HandleInvalidFile(UploadMetaData metaData);

        /// <summary>
        /// Called when an upload exceeds maximum content length as
        /// specified in the configuration.
        /// </summary>
        /// <param name="metaData"></param>
        protected abstract void HandleFileExceededMaximumLength(UploadMetaData metaData);
        
        /// <summary>
        /// Called when an asset has finished uploading to the working directory.
        /// </summary>
        /// <param name="asset"></param>
        protected abstract void OnMediaAssetUploaded(IUploadedMediaAsset asset);

        /// <summary>
        /// Called when an asset has completed uploading.
        /// Typical behavior is to call CompleteUpload() to remove
        /// the working directory prefix.  However, in some circumstances
        /// upload completion should be postponed (i.e. there is a follow up
        /// form where additional fields are required).
        /// </summary>
        /// <param name="completedFileIdentifier"></param>
        protected abstract void OnMediaAssetCompleted(string completedFileIdentifier);

        /// <summary>
        /// Called when an asset has been canceled and deleted
        /// from the working directory.
        /// </summary>
        /// <param name="workingFileIdentifier"></param>
        protected abstract void OnMediaAssetCanceled(string workingFileIdentifier);
        #endregion

        #region Protected Methods
        protected string FormatWorkingIdentifier(string identifier)
        {
            return string.Format("{0}/{1}", _configuration.WorkingDirectory.ToLower(), identifier).TrimStart(new[] {'/'});
        }

        protected string FormatFinalIdentifier(string identifier)
        {
            var extension = Path.GetExtension(identifier) ?? string.Empty;
            var finalIdentifier = _configuration.RenameToGuid
                ? string.Format("{0}.{1}", Guid.NewGuid(), extension.TrimStart(new[] {'.'}))
                : identifier;

            return FormatWorkingIdentifier(finalIdentifier);
        }
        #endregion

        #region Private Methods
        private bool IsValidExtension(string fileName)
        {
            string[] validExtensions = _configuration.AllowedExtensions.Split(new[] { ',', ';' },
                StringSplitOptions.RemoveEmptyEntries);

            if (validExtensions.Length == 0)
            {
                return true;
            }

            var fileNameExtension = Path.GetExtension(fileName);

            return validExtensions.Any(ext =>
            {
                if (!ext.StartsWith("."))
                {
                    ext = string.Format(".{0}", ext);
                }

                return ext.Equals(fileNameExtension, StringComparison.CurrentCultureIgnoreCase);
            });
        }

        private bool FileIsTooLarge(int totalBytes)
        {
            if (_configuration.MaxFileSizeInBytes <= 0)
            {
                throw new ApplicationException("Invalid MaxFileSizeInBytes property specified in configuration.");
            }

            return totalBytes > _configuration.MaxFileSizeInBytes;
        }
        #endregion

        #region Nested Class:
        protected class UploadMetaData
        {
            #region Properties
            public string FileName { get; set; }
            public int ContentLength { get; set; }
            #endregion
        }
        #endregion
    }
}
