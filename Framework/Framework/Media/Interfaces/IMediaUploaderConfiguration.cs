﻿namespace Startup.Framework.Media.Interfaces
{
    public interface IMediaUploaderConfiguration
    {
        /// <summary>
        /// Temporary directory used for processing / chunked
        /// uploading.
        /// </summary>
        string WorkingDirectory { get; }

        /// <summary>
        /// Automatically generate a new filename using a
        /// random guid once upload completes.
        /// </summary>
        bool RenameToGuid { get; }

        /// <summary>
        /// Automatically move finished working file to final
        /// destination.  Used in conjunction with "CompleteUpload" 
        /// and "CancelUpload" methods.
        /// </summary>
        bool AutoComplete { get; }

        /// <summary>
        /// Restrict uploading to specific extensions.
        /// </summary>
        string AllowedExtensions { get; }

        /// <summary>
        /// Restrict a maximum allowed file size.
        /// </summary>
        int MaxFileSizeInBytes { get; }

        /// <summary>
        /// For display purposes -- Currently used
        /// in the MedialUploaderHttpHandler to restrict
        /// GET access to the http handler for working files.
        /// </summary>
        bool IsPublic { get; set; }
    }
}