﻿using System.Collections.Generic;

namespace Startup.Framework.Media.Interfaces
{
    /// <summary>
    /// Process data uploads, both bulk and chunked and persist.
    /// For added modularity, IMediaUploader implementations should
    /// use the IMediaPersister class for persisted the once processed.
    /// </summary>
    public interface IMediaUploader
    {
        IEnumerable<IUploadedMediaAsset> Upload();
        string CompleteUpload(string workingFileIdentifier, string fileIdentifierFormat = null);
        void CancelUpload(string workingFileIdentifier);
    }
}
