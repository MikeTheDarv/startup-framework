﻿namespace Startup.Framework.Media.Interfaces
{
    public interface IMediaAsset
    {
        string FileIdentifier { get; set; }
        string OriginalFileName { get; set; }
        string FileUrl { get; set; }
        string PreviewUrl { get; set; }
        MediaAssetStatusType Status { get; set; }
    }
}
