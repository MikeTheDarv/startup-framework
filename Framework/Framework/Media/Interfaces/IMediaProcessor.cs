﻿namespace Startup.Framework.Media.Interfaces
{
    /// <summary>
    /// Process a given media asset.
    /// </summary>
    public interface IMediaProcessor
    {
        void Process(IMediaAsset mediaAsset, IMediaProcessEventHandler eventHandler);

        void Delete(IMediaAsset mediaAsset);
    }
}
