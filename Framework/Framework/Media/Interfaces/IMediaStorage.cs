﻿using System.Collections.Generic;
using System.IO;

namespace Startup.Framework.Media.Interfaces
{
    /// <summary>
    /// Persist media data (e.g. to blob storage, file system, ftp, database, etc).
    /// File identifier's can mimick nested folder structures using '/' and '\' characters.
    /// </summary>
    public interface IMediaStorage
    {
        void StoreMedia(string fileIdentifier, Stream dataStream, IDictionary<string, string> metaData);
        void StoreMediaChunk(string fileIdentifier, Stream dataStream, int chunkIndex, bool isFinalChunk, IDictionary<string, string> metaData);
        void MoveMedia(string oldFileIdentifier, string fileIdentifier);
        Stream OpenMediaStream(string fileIdentifier);
        string GetMediaUrl(string fileIdentifier);
        void DeleteMedia(string fileIdentifier);
    }
}