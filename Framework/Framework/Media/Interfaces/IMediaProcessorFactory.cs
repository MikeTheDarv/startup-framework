﻿namespace Startup.Framework.Media.Interfaces
{
    /// <summary>
    /// Factory method to generate a media processor
    /// specific to a given media asset
    /// </summary>
    public interface IMediaProcessorFactory
    {
        IMediaProcessor GetProcessor(IMediaAsset mediaAsset);
    }
}
