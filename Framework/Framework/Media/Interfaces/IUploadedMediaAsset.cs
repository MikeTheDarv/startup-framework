﻿namespace Startup.Framework.Media.Interfaces
{
    /// <summary>
    /// Represents an uploaded data block.  This could be a data chunk
    /// if chunked uploading is used, or a completed file upload.
    /// </summary>
    public interface IUploadedMediaAsset : IMediaAsset
    {
        bool IsComplete { get; set; }
        double Progress { get; set; }
        int ContentLength { get; set; }
    }
}
