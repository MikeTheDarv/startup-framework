﻿namespace Startup.Framework.Media.Interfaces
{
    public interface IMediaProcessEventHandler
    {
        void OnComplete(IMediaAsset mediaAsset);
        void OnProgress(IMediaAsset mediaAsset, MediaAssetStatusType status);
        void OnError(IMediaAsset mediaAsset, string errorMessage);
    }
}
