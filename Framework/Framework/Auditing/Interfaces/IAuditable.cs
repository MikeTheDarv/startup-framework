﻿using System;

namespace Startup.Framework.Auditing.Interfaces
{
    public interface IAuditable<TPrimaryKey>
    {
        bool AuditEnabledForCreated { get; }
        bool AuditEnabledForModifiedLast { get; }
        DateTime CreatedUtc { get; set; }
        TPrimaryKey CreatedBy { get; set; }
        DateTime ModifiedLastUtc { get; set; }
        TPrimaryKey ModifiedLastBy { get; set; }
    }
}
