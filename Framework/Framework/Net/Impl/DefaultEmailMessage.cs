﻿using System;
using System.Net.Mail;
using Startup.Framework.Net.Interfaces;

namespace Startup.Framework.Net.Impl
{
    [Serializable]
    public class DefaultEmailMessage : MailMessage, IEmailMessage
    {
        #region Private Variables
        private string _bodyHtml = string.Empty;
        private string _bodyPlainText = string.Empty;
        #endregion

        #region Properties
        public string BodyHtml
        {
            get { return _bodyHtml; }
            set { _bodyHtml = value; }
        }

        public string BodyPlainText
        {
            get { return _bodyPlainText; }
            set { _bodyPlainText = value; }
        }
        #endregion

        #region Implementation of IEmailMessage
        public MailMessage ToMailMessage()
        {
            return this;
        }
        #endregion
    }
}
