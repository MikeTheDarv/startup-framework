﻿using System;
using System.Collections.Generic;
using Startup.Framework.Net.Interfaces;

namespace Startup.Framework.Net.Impl
{
    public class DefaultEmailMessageService : IEmailMessageService
    {
        #region Private Variables
        private readonly ISmtpClient _smtpClient;
        #endregion

        #region Constructors
        public DefaultEmailMessageService(ISmtpClient smtpClient)
        {
            _smtpClient = smtpClient;
        }
        #endregion

        #region Implementation of IEmailMessageService
        public virtual bool SendEmail(IEmailMessage emailMessage)
        {
            if (emailMessage == null)
            {
                throw new ArgumentNullException("emailMessage");
            }

            using (emailMessage)
            {
                MailUtility.CreateViews(emailMessage);

                _smtpClient.Send(emailMessage);

                return true;
            }
        }

        public virtual bool SendEmails(IEnumerable<IEmailMessage> emailMessages)
        {
            foreach (var emailMessage in emailMessages)
            {
                SendEmail(emailMessage);
            }

            return true;
        }
        #endregion
    }
}
