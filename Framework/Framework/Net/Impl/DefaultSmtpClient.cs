﻿using System.Net.Mail;
using Startup.Framework.Net.Interfaces;

namespace Startup.Framework.Net.Impl
{
    public class DefaultSmtpClient : SmtpClient, ISmtpClient
    {
        #region Constructors
        public DefaultSmtpClient()
        {
            
        }

        public DefaultSmtpClient(string host) : base(host)
        {
            
        }

        public DefaultSmtpClient(string host, int port):base(host, port)
        {
            
        }
        #endregion

        #region Implementation of ISmtpClient
        public void Send(IEmailMessage emailMessage)
        {
            Send(emailMessage.ToMailMessage());
        }
        #endregion
    }
}
