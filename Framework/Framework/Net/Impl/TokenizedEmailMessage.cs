﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using Startup.Framework.Net.Interfaces;

namespace Startup.Framework.Net.Impl
{
    [Serializable]
    public class TokenizedEmailMessage : DefaultEmailMessage, ITokenizedEmailMessage
    {
        #region Private Variables
        private object _payload;
        private bool _isMerged;
        private readonly Dictionary<string, object> _mappings = new Dictionary<string, object>();
        private readonly IList<string> _internalToEmails = new List<string>(); 
        private readonly IList<string> _internalCcEmails = new List<string>(); 
        private readonly IList<string> _internalBccEmails = new List<string>(); 
        private readonly IList<string> _internalReplyToEmails = new List<string>();
        private string _internalFrom = string.Empty;
        #endregion
        
        #region Properties
        public object Payload
        {
            get { return _payload; }
            set { _payload = value; }
        }

        /// <summary>
        /// Generates the mappings between tokens and its values
        /// by compiling all expressions and evaluating them as well
        /// as any static token mappings.
        /// 
        /// This method compiles it each time, so it should only be
        /// called once with its value being stored in a local variable.
        /// </summary>
        public virtual NameValueCollection TokenMappings
        {
            get { return MapTokenValues(); }
        }
        #endregion

        #region Constructors
        public TokenizedEmailMessage() { }

        public TokenizedEmailMessage(object payload)
        {
            _payload = payload;
        }
        #endregion

        #region Implementation of ITokenizedEmailMessage
        public void MapToken<TPayload>(string token, Expression<Func<TPayload, object>> tokenExpression)
        {
            if (_mappings.Keys.Contains(token))
            {
                throw new ArgumentException("Provided token is already mapped to a value.");
            }

            _mappings.Add(token, tokenExpression.Compile());
        }

        public void MapToken(string token, string value)
        {
            if (_mappings.Keys.Contains(token))
            {
                throw new ArgumentException("Provided token is already mapped to a value.");
            }

            _mappings.Add(token, value);
        }

        public void AddTo(MailAddress address)
        {
            To.Add(address);
        }

        public void AddTo(string addresses)
        {
            _internalToEmails.Add(addresses);
        }

        public void AddCc(MailAddress address)
        {
            CC.Add(address);
        }

        public void AddCc(string addresses)
        {
            _internalCcEmails.Add(addresses);
        }

        public void AddBcc(MailAddress address)
        {
            Bcc.Add(address);
        }

        public void AddBcc(string addresses)
        {
            _internalBccEmails.Add(addresses);
        }

        public void AddReplyTo(MailAddress address)
        {
            ReplyToList.Add(address);
        }

        public void AddReplyTo(string addresses)
        {
            _internalReplyToEmails.Add(addresses);
        }

        public void SetFrom(MailAddress address)
        {
            From = address;
        }

        public void SetFrom(string address)
        {
            _internalFrom = address;
        }

        public void MergeTokens()
        {
            if (_isMerged)
            {
                throw new ApplicationException("Tokenized email message already merged.");
            }

            foreach (string key in TokenMappings.AllKeys)
            {
                ReplaceTokenWithValue(key, TokenMappings[key]);
            }

            MergeInternals();

            _isMerged = true;
        }
        #endregion

        #region Private Methods
        private NameValueCollection MapTokenValues()
        {
            NameValueCollection values = Payload == null
                                             ? new NameValueCollection()
                                             : MailUtility.GetEmailMessageTokensFromAttributes(Payload);

            foreach (string key in _mappings.Keys)
            {
                if (values.AllKeys.Contains(key))
                {
                    throw new ArgumentException(string.Format("Token Key already mapped from attribute.  Key Value was [{0}]", key));
                }

                object keyValue = _mappings[key];
                Delegate @delegate = keyValue as Delegate;
                if (@delegate != null)
                {
                    object obj = @delegate.DynamicInvoke(Payload);
                    string propValue = obj == null ? string.Empty : obj.ToString();
                    values.Add(key, propValue);
                }
                else
                {
                    values.Add(key, keyValue as string ?? string.Empty);
                }
            }
            return values;
        }

        private void ReplaceTokenWithValue(string token, string value)
        {
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }

            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            // -- Replace all mail-to's
            for (int i = 0; i < _internalToEmails.Count; i++)
            {
                _internalToEmails[i] = _internalToEmails[i].Replace(token, value);
            }

            // -- Replace all CC's
            for (int i = 0; i < _internalCcEmails.Count; i++)
            {
                _internalCcEmails[i] = _internalCcEmails[i].Replace(token, value);
            }

            // -- Replace all BCC's
            for (int i = 0; i < _internalBccEmails.Count; i++)
            {
                _internalBccEmails[i] = _internalBccEmails[i].Replace(token, value);
            }

            // -- Replace all Reply To's
            for (int i = 0; i < _internalReplyToEmails.Count; i++)
            {
                _internalReplyToEmails[i] = _internalReplyToEmails[i].Replace(token, value);
            }

            // -- Replace From
            if (!string.IsNullOrWhiteSpace(_internalFrom))
            {
                _internalFrom = _internalFrom.Replace(token, value);
            }

            // -- Replace Subject
            Subject = Subject.Replace(token, value);

            // -- Replace HTML/Text Body
            BodyHtml = BodyHtml.Replace(token, value.Replace(Environment.NewLine, "<br />"));
            BodyPlainText = BodyPlainText.Replace(token, value);
        }

        private void MergeInternals()
        {
            /* To */
            foreach (var email in _internalToEmails)
            {
                To.Add(email);
            }

            /* CC */
            foreach (var email in _internalCcEmails)
            {
                CC.Add(email);
            }

            /* Bcc */
            foreach (var email in _internalBccEmails)
            {
                Bcc.Add(email);
            }

            /* Reply To */
            foreach (var email in _internalReplyToEmails)
            {
                ReplyToList.Add(email);
            }

            /* From */
            if (!string.IsNullOrWhiteSpace(_internalFrom))
            {
                From = new MailAddress(_internalFrom);
            }
        }
        #endregion
    }
}
