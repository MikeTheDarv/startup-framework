﻿using System;
using System.Collections.Generic;
using Startup.Framework.Net.Interfaces;

namespace Startup.Framework.Net.Impl
{
    public class TokenizedEmailMessageService : ITokenizedEmailMessageService
    {
        #region Private Variables
        private readonly ISmtpClient _smtpClient;
        #endregion

        #region Constructors
        public TokenizedEmailMessageService(ISmtpClient smtpClient)
        {
            _smtpClient = smtpClient;
        }
        #endregion

        #region Implementation of IEmailMessageService
        public virtual bool SendEmail(ITokenizedEmailMessage emailMessage)
        {
            if (emailMessage == null)
            {
                throw new ArgumentNullException("emailMessage");
            }

            using (emailMessage)
            {
                emailMessage.MergeTokens();
                
                MailUtility.CreateViews(emailMessage);

                _smtpClient.Send(emailMessage);

                return true;
            }
        }

        public virtual bool SendEmails(IEnumerable<ITokenizedEmailMessage> emailMessages)
        {
            foreach (var emailMessage in emailMessages)
            {
                SendEmail(emailMessage);
            }

            return true;
        }
        #endregion
    }
}
