﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using Startup.Framework.Net.Interfaces;

namespace Startup.Framework.Net
{
    #region Nested Class: MailUtility
    public static class MailUtility
    {
        #region Public Methods
        public static NameValueCollection GetEmailMessageTokensFromAttributes(object obj)
        {
            return GetEmailMessageTokensFromAttributes(obj, true);
        }
        #endregion

        #region Internal Methods
        internal static void CreateViews(IEmailMessage emailMessage)
        {
            AlternateView plainView = AlternateView.CreateAlternateViewFromString(emailMessage.BodyPlainText, Encoding.UTF8, "text/plain");
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(emailMessage.BodyHtml, Encoding.UTF8, "text/html");

            emailMessage.AlternateViews.Add(plainView);
            emailMessage.AlternateViews.Add(htmlView);
        }

        internal static NameValueCollection GetEmailMessageTokensFromAttributes(object obj, bool formatTokens)
        {
            NameValueCollection collection = new NameValueCollection();
            GetSimpleEmailTokens(collection, obj, formatTokens);
            GetComplexEmailTokens(collection, obj);
            return collection;
        }
        #endregion

        #region Private Methods
        private static void GetSimpleEmailTokens(NameValueCollection tokens, object obj, bool formatTokens)
        {
            PropertyInfo[] destinationProperties = obj.GetType().GetProperties();

            foreach (PropertyInfo property in destinationProperties)
            {
                Attribute[] tokenAttributes = Attribute.GetCustomAttributes(property, typeof(EmailMessageTokenAttribute), true);
                if (tokenAttributes.Length == 0)
                {
                    continue;
                }

                EmailMessageTokenAttribute tokenAttribute = (EmailMessageTokenAttribute)tokenAttributes[0];

                string tokenName = formatTokens
                                       ? string.Format("{0}{1}{2}", tokenAttribute.OpeningSymbol, tokenAttribute.TokenName, tokenAttribute.ClosingSymbol)
                                       : tokenAttribute.TokenName;

                if (tokens.AllKeys.Contains(tokenName))
                {
                    throw new ArgumentException(string.Format("TokenName already mapped to another property.  Value was '{0}'", tokenName));
                }

                object propObject = property.GetValue(obj, null);
                string propValue = propObject == null ? string.Empty : propObject.ToString();

                string tokenValue = string.IsNullOrWhiteSpace(tokenAttribute.DisplayFormat)
                                        ? propValue
                                        : string.Format(tokenAttribute.DisplayFormat, propValue);

                tokens.Add(tokenName, tokenValue);
            }
        }

        private static void GetComplexEmailTokens(NameValueCollection tokens, object obj)
        {
            PropertyInfo[] destinationProperties = obj.GetType().GetProperties();

            foreach (PropertyInfo property in destinationProperties)
            {
                Attribute[] tokenAttributes = Attribute.GetCustomAttributes(property, typeof(EmailMessageComplexTokenAttribute), true);
                if (!property.PropertyType.IsClass || tokenAttributes.Length == 0)
                {
                    continue;
                }

                EmailMessageComplexTokenAttribute tokenAttribute = (EmailMessageComplexTokenAttribute)tokenAttributes[0];
                object complexObject = property.GetValue(obj, null);
                if (complexObject == null)
                {
                    continue;
                }

                var objectTokens = GetEmailMessageTokensFromAttributes(complexObject, false);
                foreach (string objectToken in objectTokens)
                {
                    string tokenName = string.Format("{0}{1}.{2}{3}", tokenAttribute.OpeningSymbol,
                                                     tokenAttribute.TokenPrefix, objectToken,
                                                     tokenAttribute.ClosingSymbol);

                    if (tokens.AllKeys.Contains(tokenName))
                    {
                        throw new ArgumentException(
                            string.Format("TokenName already mapped to another property.  Value was '{0}'", tokenName));
                    }

                    string tokenValue = string.IsNullOrWhiteSpace(tokenAttribute.DisplayFormat)
                                            ? objectTokens[objectToken]
                                            : string.Format(tokenAttribute.DisplayFormat, objectTokens[objectToken]);

                    tokens.Add(tokenName, tokenValue);
                }
            }
        }
        #endregion
    }
    #endregion
}
