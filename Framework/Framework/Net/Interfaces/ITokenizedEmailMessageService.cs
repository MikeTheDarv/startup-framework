﻿using System.Collections.Generic;

namespace Startup.Framework.Net.Interfaces
{
    public interface ITokenizedEmailMessageService
    {
        bool SendEmail(ITokenizedEmailMessage email);

        bool SendEmails(IEnumerable<ITokenizedEmailMessage> emails);
    }
}
