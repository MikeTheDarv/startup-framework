﻿using System;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Net.Mail;

namespace Startup.Framework.Net.Interfaces
{
    public interface ITokenizedEmailMessage : IEmailMessage
    {
        object Payload { get; set; }

        NameValueCollection TokenMappings { get; }

        void MapToken<TPayload>(string token, Expression<Func<TPayload, object>> tokenExpression);
        void MapToken(string token, string value);

        void AddTo(MailAddress address);
        void AddTo(string addresses);

        void AddCc(MailAddress address);
        void AddCc(string addresses);

        void AddBcc(MailAddress address);
        void AddBcc(string addresses);

        void AddReplyTo(MailAddress address);
        void AddReplyTo(string addresses);

        void SetFrom(MailAddress address);
        void SetFrom(string address);

        void MergeTokens();
    }
}
