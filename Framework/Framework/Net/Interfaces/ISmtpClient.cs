﻿using System.Net;

namespace Startup.Framework.Net.Interfaces
{
    public interface ISmtpClient
    {
        string Host { get; set; }

        int Port { get; set; }

        bool UseDefaultCredentials { get; set; }
        
        ICredentialsByHost Credentials { get; set; }

        void Send(IEmailMessage emailMessage);
    }
}
