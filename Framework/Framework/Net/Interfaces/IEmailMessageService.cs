﻿using System.Collections.Generic;

namespace Startup.Framework.Net.Interfaces
{
    public interface IEmailMessageService
    {
        bool SendEmail(IEmailMessage email);

        bool SendEmails(IEnumerable<IEmailMessage> emailMessages);
    }
}
