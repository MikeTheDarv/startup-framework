﻿namespace Startup.Framework.Net.Interfaces
{
    public interface IEmailAddress
    {
        string Address { get; set; }
    }
}
