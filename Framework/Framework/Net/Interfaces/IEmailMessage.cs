﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;

namespace Startup.Framework.Net.Interfaces
{  
    public interface IEmailMessage : IDisposable
    {
        MailAddress From { get; set; }

        MailAddress Sender { get; set; }

        MailAddressCollection ReplyToList { get; }

        MailAddressCollection To { get; }

        MailAddressCollection Bcc { get; }

        MailAddressCollection CC { get; }

        MailPriority Priority { get; set; }

        DeliveryNotificationOptions DeliveryNotificationOptions { get; set; }

        string Subject { get; set; }

        NameValueCollection Headers { get; }

        bool IsBodyHtml { get; set; }

        AttachmentCollection Attachments { get; }

        AlternateViewCollection AlternateViews { get; }

        string BodyHtml { get; set; }

        string BodyPlainText { get; set; }

        MailMessage ToMailMessage();
    }
}
