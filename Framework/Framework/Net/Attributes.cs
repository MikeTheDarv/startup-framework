﻿using System;

namespace Startup.Framework.Net
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class EmailMessageTokenAttribute : Attribute
    {
        #region Private Variables
        private string _tokenName = string.Empty;
        private string _displayFormat = "{0}";
        private string _openingSymbol = "{";
        private string _closingSymbol = "}";
        #endregion

        #region Properties
        public virtual string TokenName
        {
            get { return _tokenName; }
            set { _tokenName = value; }
        }

        public virtual string DisplayFormat
        {
            get { return _displayFormat; }
            set { _displayFormat = value; }
        }

        public virtual string OpeningSymbol
        {
            get { return _openingSymbol; }
            set { _openingSymbol = value; }
        }

        public virtual string ClosingSymbol
        {
            get { return _closingSymbol; }
            set { _closingSymbol = value; }
        }
        #endregion

        #region Constructors
        public EmailMessageTokenAttribute(string tokenName) : this(tokenName, "{0}") { }

        public EmailMessageTokenAttribute(string tokenName, string openingSymbol, string closingSymbol) : this(tokenName, "{0}", openingSymbol, closingSymbol) { }

        public EmailMessageTokenAttribute(string tokenName, string displayFormat)
        {
            _tokenName = tokenName;
            _displayFormat = displayFormat;
        }

        public EmailMessageTokenAttribute(string tokenName, string displayFormat, string openingSymbol, string closingSymbol)
        {
            _tokenName = tokenName;
            _displayFormat = displayFormat;
            _openingSymbol = openingSymbol;
            _closingSymbol = closingSymbol;
        }
        #endregion
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class EmailMessageComplexTokenAttribute : Attribute
    {
        #region Private Variables
        private string _tokenPrefix = string.Empty;
        private string _displayFormat = "{0}";
        private string _openingSymbol = "{";
        private string _closingSymbol = "}";
        #endregion

        #region Properties
        public virtual string TokenPrefix
        {
            get { return _tokenPrefix; }
            set { _tokenPrefix = value; }
        }

        public virtual string DisplayFormat
        {
            get { return _displayFormat; }
            set { _displayFormat = value; }
        }

        public virtual string OpeningSymbol
        {
            get { return _openingSymbol; }
            set { _openingSymbol = value; }
        }

        public virtual string ClosingSymbol
        {
            get { return _closingSymbol; }
            set { _closingSymbol = value; }
        }
        #endregion

        #region Constructors
        public EmailMessageComplexTokenAttribute(string tokenPrefix) : this(tokenPrefix, "{0}") { }

        public EmailMessageComplexTokenAttribute(string tokenPrefix, string openingSymbol, string closingSymbol) : this(tokenPrefix, "{0}", openingSymbol, closingSymbol) { }

        public EmailMessageComplexTokenAttribute(string tokenPrefix, string displayFormat)
        {
            _tokenPrefix = tokenPrefix;
            _displayFormat = displayFormat;
        }

        public EmailMessageComplexTokenAttribute(string tokenPrefix, string displayFormat, string openingSymbol, string closingSymbol)
        {
            _tokenPrefix = tokenPrefix;
            _displayFormat = displayFormat;
            _openingSymbol = openingSymbol;
            _closingSymbol = closingSymbol;
        }
        #endregion
    }
}
