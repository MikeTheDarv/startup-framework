﻿using System.ComponentModel;

namespace Startup.Framework
{
    #region Nested Enum: UrlScheme
    public enum UrlScheme
    {
        [Description("file")]
        File,

        [Description("ftp")]
        FTP,

        [Description("gopher")]
        Gopher,

        [Description("http")]
        Http,

        [Description("https")]
        Https,

        [Description("mailto")]
        MailTo,

        [Description("news")]
        News
    }
    #endregion
}
