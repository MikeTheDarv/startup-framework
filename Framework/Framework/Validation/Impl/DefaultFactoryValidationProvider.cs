﻿using System;
using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Validation.Impl
{
    public class DefaultFactoryValidationProvider : IValidationProvider
    {
        #region Private Variables
        private readonly Func<Type, IValidator> _validatorFactory;
        #endregion

        #region Constructors

        public DefaultFactoryValidationProvider(Func<Type, IValidator> validatorFactory)
        {
            _validatorFactory = validatorFactory;
        }

        #endregion

        #region Implementation of IValidationProvider
        public IValidator GetValidator(Type entityType)
        {
            return _validatorFactory(entityType);
        }
        #endregion
    }
}
