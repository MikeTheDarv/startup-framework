﻿using System.Collections.Generic;
using System.Linq;
using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Validation.Impl
{
    public class NullValidator : IValidator
    {
        public IEnumerable<IValidationError> ValidationErrors
        {
            get { return Enumerable.Empty<IValidationError>(); }
        }

        public bool Validate(object entity)
        {
            return true;
        }
    }
}
