﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Validation.Impl
{
    public class DataAnnotationsValidatorBase : IValidator
    {
        #region Private Variables
        private readonly IList<IValidationError> _validationErrors = new List<IValidationError>();
        #endregion
        
        #region Implementation of IValidator
        public bool Validate(object entity)
        {
            _validationErrors.Clear();

            ValidateDataAnnotations(entity);

            ValidateEntity(entity);

            return _validationErrors.Count == 0;
        }

        public IEnumerable<IValidationError> ValidationErrors
        {
            get { return _validationErrors; }
        }
        #endregion

        #region Protected Methods
        protected void AddValidationError(IValidationError error)
        {
            _validationErrors.Add(error);
        }
        #endregion

        #region Abstract Methods
        protected virtual void ValidateEntity(object entity)
        {
        }
        #endregion

        #region Private Methods
        private void ValidateDataAnnotations(object entity)
        {
            // -- Validate but don't actually add the errors
            //    to the collection, as they will already be available
            //    via ModelState
            //
            // -- Update: 4/12/2013
            //    Populate the errors collection with annotated
            //    validation errors.  If used with conjuction of ModelState
            //    for display purposes, it is now up to the developer to clear
            //    the model state prior to adding these errors.
            var context = new ValidationContext(entity);
            var results = new List<ValidationResult>();

            Validator.TryValidateObject(entity, context, results, true);

            foreach (var validationResult in results)
            {
                _validationErrors.Add(new DefaultValidationError(string.Join(",", validationResult.MemberNames), validationResult.ErrorMessage));
            }
        }
        #endregion
    }
}
