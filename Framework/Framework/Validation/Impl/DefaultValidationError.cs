﻿using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Validation.Impl
{
    public class DefaultValidationError : IValidationError
    {
        #region Properties
        public string Key { get; private set; }

        public string Message { get; private set; }
        #endregion

        #region Constructors
        public DefaultValidationError(string key, string message)
        {
            Key = key;
            Message = message;
        }
        #endregion
    }
}
