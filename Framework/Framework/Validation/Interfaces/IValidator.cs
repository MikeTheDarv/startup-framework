﻿using System.Collections.Generic;

namespace Startup.Framework.Validation.Interfaces
{
    public interface IValidator
    {
        IEnumerable<IValidationError> ValidationErrors { get; }
        bool Validate(object entity);
    }
}
