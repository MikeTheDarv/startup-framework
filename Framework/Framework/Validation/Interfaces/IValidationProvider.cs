﻿using System;

namespace Startup.Framework.Validation.Interfaces
{
    public interface IValidationProvider
    {
        IValidator GetValidator(Type entityType);
    }
}
