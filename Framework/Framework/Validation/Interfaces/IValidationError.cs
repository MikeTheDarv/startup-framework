﻿namespace Startup.Framework.Validation.Interfaces
{
    public interface IValidationError
    {
        string Key { get; }
        string Message { get; }
    }
}
