﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Startup.Framework.Validation
{

    #region Nested Attribute: MinValueAttribute
    public class MinValueAttribute : ValidationAttribute
    {
        private readonly double _minValue;

        public MinValueAttribute(double minValue)
        {
            _minValue = minValue;
            ErrorMessage = "Enter a value greater or equal than " + _minValue;
        }

        public MinValueAttribute(int minValue)
        {
            _minValue = minValue;
            ErrorMessage = "Enter a value greater or equal than " + _minValue;
        }

        public override bool IsValid(object value)
        {
            return Convert.ToDouble(value) >= _minValue;
        }
    }
    #endregion

    #region Nested Attribute: MaxValueAttribute
    public class MaxValueAttribute : ValidationAttribute
    {
        private readonly double _maxValue;

        public MaxValueAttribute(double maxValue)
        {
            _maxValue = maxValue;
            ErrorMessage = "Enter a value less or equal than " + _maxValue;
        }

        public MaxValueAttribute(int maxValue)
        {
            _maxValue = maxValue;
            ErrorMessage = "Enter a value less or equal than " + _maxValue;
        }

        public override bool IsValid(object value)
        {
            return Convert.ToDouble(value) <= _maxValue;
        }
    }
    #endregion
}
