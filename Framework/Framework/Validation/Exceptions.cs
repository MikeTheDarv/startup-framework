﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Validation
{
    #region Nested Exception: ValidationException
    public class ValidationException : Exception
    {
        #region Private Variables
        private const string VALIDATION_EXCEPTION_MESSAGE = "A validation exception has occurred: {0}{1}";
        #endregion

        #region Properties
        public ReadOnlyCollection<IValidationError> ValidationErrors { get; private set; }
        #endregion

        #region Constructors
        public ValidationException(IEnumerable<IValidationError> validationErrors)
        {
            ValidationErrors = new ReadOnlyCollection<IValidationError>(validationErrors.ToArray());
        }
        #endregion

        #region Overrides

        public override string Message
        {
            get
            {
                return string.Format(VALIDATION_EXCEPTION_MESSAGE, Environment.NewLine,
                    string.Join(Environment.NewLine, ValidationErrors.Select(error => error.Message)));
            }
        }

        #endregion
    }
    #endregion
}
