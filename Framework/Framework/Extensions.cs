﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Startup.Framework
{
    #region Enum Extensions
    public static class EnumExtensions
    {
        public static string ToDescription(this Enum e)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])e.GetType().GetField(e.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : ToString(e);
        }

        public static string ToString(this Enum e)
        {
            return e.ToString().Replace("_", " ");
        }

        public static bool Has<T>(this Enum e, T value)
        {
            var source = (int)(object)e;
            var target = (int)(object)value;
            return ((source & target) == target);
        }
    }
    #endregion

    #region String Extensions
    public static class StringExtensions
    {
        public static string ToAlphaNumeric(this string s)
        {
            return ToAlphaNumeric(s, string.Empty);
        }

        public static string ToAlphaNumeric(this string s, string extraAllowedCharacters)
        {
            return Regex.Replace(s, string.Format("[^A-Za-z0-9{0}]", extraAllowedCharacters), string.Empty);
        }

        public static string Replace(this string source, string oldValue, string newValue, StringComparison comparison)
        {
            StringBuilder sb = new StringBuilder();

            int previousIndex = 0;
            int index = source.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(source.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = source.IndexOf(oldValue, index, comparison);
            }
            sb.Append(source.Substring(previousIndex));

            return sb.ToString();
        }

        public static string SplitCamelCase(this string input)
        {
            return Regex.Replace(input, "(?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z])", " $1", RegexOptions.Compiled).Trim();
        }
    }
    #endregion

    #region Date Extensions
    public static class DateExtensions
    {
        public static DateTime FromUnixTimestamp(this double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        public static double ToUnixTimestamp(this DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        public static string ToRelativeDateString(this DateTime date, TimeZoneInfo timeZone, string yearOldFormat = "MMMM dd, yyyy", string monthOldFormat = "MMMM dd")
        {
            var utcDate = ConvertTimeToUtc(date, timeZone);
            return ToRelativeDateString(utcDate, DateTime.UtcNow, yearOldFormat, monthOldFormat);
        }

        public static string ToRelativeDateToString(this DateTime date, TimeZoneInfo timeZone, string toStringFormat)
        {
            var utcDate = ConvertTimeToUtc(date, timeZone);
            return utcDate.ToString(toStringFormat);
        }

        public static string ToRelativeDateString(this DateTime date, DateTime sourceDate, string yearOldFormat = "MMMM dd, yyyy", string monthOldFormat = "MMMM dd")
        {
            return GetRelativeDateValue(date, sourceDate, yearOldFormat, monthOldFormat);
        }

        public static string ToRelativeDateTimeString(this DateTime date, TimeZoneInfo timeZone, string yearOldFormat = "MMMM dd, yyyy", string monthOldFormat = "MMMM dd")
        {
            var utcDate = ConvertTimeToUtc(date, timeZone);
            return ToRelativeDateTimeString(utcDate, DateTime.UtcNow, yearOldFormat, monthOldFormat);
        }

        public static string ToRelativeDateTimeToString(this DateTime date, TimeZoneInfo timeZone, string toStringFormat)
        {
            var utcDate = ConvertTimeToUtc(date, timeZone);
            return utcDate.ToString(toStringFormat);
        }
        
        public static string ToRelativeDateTimeString(this DateTime date, DateTime sourceDate, string yearOldFormat = "MMMM dd, yyyy", string monthOldFormat = "MMMM dd")
        {
            return GetRelativeDateTimeValue(date, sourceDate, yearOldFormat, monthOldFormat);
        }
        
        public static DateTime ConvertServerToLocalDateTime(this DateTime dateTime, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.Local, timeZone);
        }

        #region Private Methods
        private static string GetRelativeDateValue(DateTime date, DateTime comparedTo, string yearOldDateFormat, string monthOldDateFormat)
        {
            TimeSpan diff = comparedTo.Subtract(date);
            if (Convert.ToInt32(diff.TotalDays) >= 365)
                return string.Concat("on ", date.ToString(yearOldDateFormat));

            if (Convert.ToInt32(diff.TotalDays) >= 7)
                return string.Concat("on ", date.ToString(monthOldDateFormat));

            if (Convert.ToInt32(diff.TotalDays) > 1)
            {
                string day = Convert.ToInt32(diff.TotalDays) == 1 ? "day" : "days";
                return string.Format("{0:N0} {1} ago", Convert.ToInt32(diff.TotalDays), day);
            }

            if (Convert.ToInt32(diff.TotalDays) == 1)
                return "yesterday";

            if (Convert.ToInt32(diff.TotalHours) >= 2)
            {
                string hour = Convert.ToInt32(diff.TotalHours) == 1 ? "hour" : "hours";
                return string.Format("{0:N0} {1} ago", Convert.ToInt32(diff.TotalHours), hour);
            }

            if (Convert.ToInt32(diff.TotalMinutes) >= 60)
                return "more than an hour ago";

            if (Convert.ToInt32(diff.TotalMinutes) >= 5)
            {
                string minute = Convert.ToInt32(diff.TotalMinutes) == 1 ? "minute" : "minutes";
                return string.Format("{0:N0} {1} ago", Convert.ToInt32(diff.TotalMinutes), minute);
            }

            if (Convert.ToInt32(diff.TotalMinutes) >= 1)
                return "a few minutes ago";

            return "a few seconds ago";
        }

        private static string GetRelativeDateTimeValue(DateTime date, DateTime comparedTo, string yearOldDateFormat, string monthOldDateFormat)
        {
            TimeSpan diff = comparedTo.Subtract(date);
            if (Convert.ToInt32(diff.TotalDays) >= 365)
                return string.Format("on {0} @ {1}", date.ToString(yearOldDateFormat), date.ToShortTimeString());

            if (Convert.ToInt32(diff.TotalDays) >= 7)
                return string.Format("on {0} @ {1}", date.ToString(monthOldDateFormat), date.ToShortTimeString());

            if (Convert.ToInt32(diff.TotalDays) > 1)
            {
                string day = Convert.ToInt32(diff.TotalDays) == 1 ? "day" : "days";
                return string.Format("{0:N0} {1} ago @ {2}", Convert.ToInt32(diff.TotalDays), day, date.ToShortTimeString());
            }

            if (Convert.ToInt32(diff.TotalDays) == 1)
                return string.Format("yesterday @ {0}", date.ToShortTimeString());

            if (Convert.ToInt32(diff.TotalHours) >= 2)
            {
                string hour = Convert.ToInt32(diff.TotalHours)== 1 ? "hour" : "hours";
                return string.Format("{0:N0} {1} ago", Convert.ToInt32(diff.TotalHours), hour);
            }

            if (Convert.ToInt32(diff.TotalMinutes) >= 60)
                return "more than an hour ago";

            if (Convert.ToInt32(diff.TotalMinutes) >= 5)
            {
                string minute = Convert.ToInt32(diff.TotalMinutes) == 1 ? "minute" : "minutes";
                return string.Format("{0:N0} {1} ago", Convert.ToInt32(diff.TotalMinutes), minute);
            }

            if (Convert.ToInt32(diff.TotalMinutes) >= 1)
                return "a few minutes ago";

            return "a few seconds ago";
        }

        private static DateTime ConvertTimeToUtc(DateTime date, TimeZoneInfo timeZone)
        {
            date = DateTime.SpecifyKind(date, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeToUtc(date, timeZone);
        }
        #endregion
    }
    #endregion

    #region Linq Extensions
    public static class LinqExtensions
    {
        // Returns the element of source for which valueFunc is maximal
        // If there are several maxima the first one is returned
        // NaNs are never considered as maximum.
        // If the sequence is empty or consists only of NaNs the result is `defaultValue`
        public static T ArgMaxOrDefault<T>(this IEnumerable<T> source, Func<T, double> valueFunc, T defaultValue)
        {
            T result = defaultValue;
            double max = 0;
            bool first = true;

            foreach (T elem in source)
            {
                double value = valueFunc(elem);
                
                if (double.IsNaN(value))
                {
                    continue;
                }

                if (first || value > max)
                {
                    max = value;
                    result = elem;
                    first = false;
                }
            }

            return result;
        }

        public static T ArgMaxOrDefault<T>(this IEnumerable<T> source, Func<T, double> valueFunc)
        {
            return ArgMaxOrDefault(source, valueFunc, default(T));
        }

        //Return -1 if no element satisfies the predicate
        public static int FirstIndex<T>(this IEnumerable<T> source, Predicate<T> predicate)
        {
            int i = 0;

            foreach (T element in source)
            {
                if (predicate(element))
                {
                    return i;
                }

                i++;
            }

            return -1;
        }

        //Return -1 if no element satisfies the predicate
        public static int FirstIndex<T>(this IEnumerable<T> source, Func<T, int, bool> predicate)
        {
            int i = 0;

            foreach (T element in source)
            {
                if (predicate(element, i))
                {
                    return i;
                }

                i++;
            }

            return -1;
        }

        //Return -1 if no element satisfies the predicate
        public static int LastIndex<T>(this IEnumerable<T> source, Predicate<T> predicate)
        {
            return LastIndex(source, (elem, i) => predicate(elem));
        }

        public static int LastIndex<T>(this IEnumerable<T> source, Func<T, int, bool> predicate)
        {
            IList<T> sourceList = source as IList<T>;
            int i;

            if (sourceList != null)
            {
                for (i = sourceList.Count; i >= 0; i--)
                {
                    if (predicate(sourceList[i], i))
                    {
                        return i;
                    }
                }

                return -1;
            }
            
            i = 0;
            int last = -1;
            
            foreach (T element in source)
            {
                if (predicate(element, i))
                {
                    last = i;
                }

                i++;
            }

            return last;
        }

        public static IEnumerable<T> TakeWhileIncludingTerminator<T>(this IEnumerable<T> source, Predicate<T> predecate)
        {
            foreach (T element in source)
            {
                yield return element;

                if (predecate(element))
                {
                    yield break;
                }
            }
        }

        public static T? FirstOrNull<T>(this IEnumerable<T> source) where T : struct
        {
            return source.Select(element => (T?)element).FirstOrDefault();
        }

        public static T? FirstOrNull<T>(this IEnumerable<T> source, Func<T, bool> predicate) where T : struct
        {
            return source.Where(predicate).Select(element => (T?)element).FirstOrDefault();
        }

        public static T FirstOrDefault<T>(this IEnumerable<T> source, T defaultValue)
        {
            IEnumerator<T> enumerator = source.GetEnumerator();
            using (enumerator)
            {
                if (enumerator.MoveNext())
                {
                    return enumerator.Current;
                }

                return defaultValue;
            }
        }

        public static T FirstOrDefault<T>(this IEnumerable<T> source, Func<T, bool> predicate, T defaultValue)
        {
            IEnumerator<T> enumerator = source.Where(predicate).GetEnumerator();
            using (enumerator)
            {
                if (enumerator.MoveNext())
                {
                    return enumerator.Current;
                }
                return defaultValue;
            }
        }

        public static IEnumerable<KeyValuePair<int, T>> IndexValuePairs<T>(this IEnumerable<T> seq)
        {
            int i = 0;
            foreach (T value in seq)
            {
                yield return new KeyValuePair<int, T>(i, value);
                i++;
            }
        }

        public static string ConcatToString(this IEnumerable<String> strings, string seperator)
        {
            StringBuilder sb = new StringBuilder();
            bool first = true;

            foreach (string s in strings)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    sb.Append(seperator);
                }

                sb.Append(s);
            }

            return sb.ToString();
        }

        public static string ConcatToString(this IEnumerable<String> strings)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string s in strings)
            {
                sb.Append(s);
            }
            return sb.ToString();
        }
        
        public static IEnumerable<T> DepthFirstSearch<T>(this T root) where T : IEnumerable<T>
        {
            return DepthFirstSearch(root, x => x);
        }

        public static IEnumerable<T> DepthFirstSearch<T>(this T root, Func<T, IEnumerable<T>> getChildren)
        {
            yield return root;
            foreach (T child in getChildren(root))
            {
                foreach (T descendant in DepthFirstSearch(child, getChildren))
                {
                    yield return descendant;
                }
            }
        }
    }
    #endregion
}
