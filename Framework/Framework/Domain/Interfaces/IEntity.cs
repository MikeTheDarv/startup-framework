﻿using System.Collections.Generic;
using System.Reflection;
using Startup.Framework.Domain.Impl;

namespace Startup.Framework.Domain.Interfaces
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// 
    ///     This serves as a base interface for <see cref="EntityWithTypedId{TId}" /> and 
    ///     <see cref = "Entity" />. Also provides a simple means to develop your own base entity.
    /// </summary>
    public interface IEntity
    {
        IEnumerable<PropertyInfo> GetSignatureProperties();
        bool IsTransient();
    }

    /// <summary>
    ///     This serves as a base interface for <see cref="EntityWithTypedId{TId}" /> and 
    ///     <see cref = "Entity" />. Also provides a simple means to develop your own base entity.
    /// </summary>
    public interface IEntityWithTypedId<out TPrimaryKey> : IEntity
    {
        TPrimaryKey Id { get; }
    }
}
