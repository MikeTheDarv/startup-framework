﻿namespace Startup.Framework.Cryptography.Interfaces
{
    public interface ICryptoProvider
    {
        #region Properties
        string Password { get; set; }
        string Salt { get; set; }
        string IV { get; set; }
        int PasswordIterations { get; set; }
        int KeySize { get; set; }
        #endregion

        #region Public Methods
        string Encrypt(string message);
        string Decrypt(string cryptedMessage);
        string GenerateIV();
        string GenerateSalt();
        #endregion
    }
}
