﻿using System;
using System.Security.Cryptography;
using System.Text;
using Startup.Framework.Cryptography.Interfaces;

namespace Startup.Framework.Cryptography.Impl
{
    public class AesCryptoProvider : ICryptoProvider
    {
        #region Private Variables
        private string _password = string.Empty;
        private string _salt = string.Empty;
        private string _iv = string.Empty;

        private int _passwordIterations = 100;
        private int _keySize = 128; // Must be 128, 192, or 256

        private readonly Random _randomGenerator = new Random();
        #endregion

        #region Properties
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string Salt
        {
            get { return _salt; }
            set { _salt = value; }
        }

        public string IV
        {
            get { return _iv; }
            set { _iv = value; }
        }

        public int PasswordIterations
        {
            get { return _passwordIterations; }
            set { _passwordIterations = value; }
        }

        public int KeySize
        {
            get { return _keySize; }
            set { _keySize = value; }
        }
        #endregion

        #region Constructors
        public AesCryptoProvider()
        {

        }

        public AesCryptoProvider(string password)
        {
            _password = password;
        }

        public AesCryptoProvider(string password, string salt, string iv)
        {
            _password = password;
            _salt = salt;
            _iv = iv;
        }

        public AesCryptoProvider(string password, string salt, string iv, int passwordIterations, int keySize)
        {
            _password = password;
            _salt = salt;
            _iv = iv;
            _passwordIterations = passwordIterations;
            _keySize = keySize;
        }
        #endregion

        #region Implementation of ICryptoProvider
        public string Encrypt(string message)
        {
            VerifyCryptoSettings();

            if (string.IsNullOrEmpty(message))
            {
                return string.Empty;
            }

            byte[] saltBytes = Encoding.UTF8.GetBytes(Salt);
            byte[] passwordBytes = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(Password), saltBytes, PasswordIterations).GetBytes(16);
            byte[] initializationVectorBytes = Encoding.UTF8.GetBytes(IV);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);

            RijndaelManaged rijndaelCipher = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = KeySize,
                BlockSize = 128,
                Key = passwordBytes,
                IV = initializationVectorBytes
            };

            byte[] cipherTextBytes;
            using (ICryptoTransform cryptoTransform = rijndaelCipher.CreateEncryptor())
            {
                cipherTextBytes = cryptoTransform.TransformFinalBlock(messageBytes, 0, messageBytes.Length);
            }

            rijndaelCipher.Clear();
            return Convert.ToBase64String(cipherTextBytes);
        }

        public string Decrypt(string cryptedMessage)
        {
            VerifyCryptoSettings();

            if (string.IsNullOrEmpty(cryptedMessage))
            {
                return string.Empty;
            }

            byte[] saltBytes = Encoding.UTF8.GetBytes(Salt);
            byte[] passwordBytes = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(Password), saltBytes, PasswordIterations).GetBytes(16);
            byte[] initializationVectorBytes = Encoding.UTF8.GetBytes(IV);
            byte[] messageBytes = Convert.FromBase64String(cryptedMessage);

            RijndaelManaged rijndaelCipher = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = KeySize,
                BlockSize = 128,
                Key = passwordBytes,
                IV = initializationVectorBytes
            };

            byte[] plainTextBytes;
            using (ICryptoTransform cryptoTransform = rijndaelCipher.CreateDecryptor())
            {
                plainTextBytes = cryptoTransform.TransformFinalBlock(messageBytes, 0, messageBytes.Length);
            }

            rijndaelCipher.Clear();
            return Encoding.UTF8.GetString(plainTextBytes);
        }

        public string GenerateIV()
        {
            IV = RandomString(16);
            return IV;
        }

        public string GenerateSalt()
        {
            Salt = RandomString(32);
            return Salt;
        }
        #endregion

        #region Private Methods
        private void VerifyCryptoSettings()
        {
            if (string.IsNullOrEmpty(Password))
            {
                throw new ApplicationException("Password cannot be blank.");
            }

            if (string.IsNullOrEmpty(Salt))
            {
                throw new ApplicationException("Salt cannot be blank.");
            }

            if (string.IsNullOrEmpty(IV) || IV.Length != 16)
            {
                throw new ApplicationException("IV cannot be blank and must be 16 ASCII characters long.");
            }

            if (!(KeySize == 128 || KeySize == 192 || KeySize == 256))
            {
                throw new ApplicationException("Key size must be 128, 192, or 256.");
            }
        }

        private string RandomString(int size)
        {
            StringBuilder randomString = new StringBuilder(size);

            for (int i = 0; i < size; i++)
            {
                randomString.Append((char)(26 * _randomGenerator.NextDouble() + 65)); // ASCII characters start at 65
            }

            return randomString.ToString();
        }
        #endregion
    }
}
