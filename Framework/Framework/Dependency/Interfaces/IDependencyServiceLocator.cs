﻿using System;
using System.Collections.Generic;

namespace Startup.Framework.Dependency.Interfaces
{
    public interface IDependencyServiceLocator
    {
        object GetInstance(Type serviceType);
        object GetInstance(Type serviceType, string key);
        IEnumerable<object> GetAllInstances(Type serviceType);

        TService GetInstance<TService>();
        TService GetInstance<TService>(string key);
        IEnumerable<TService> GetAllInstances<TService>();
    }
}