﻿using System.Drawing;

namespace Startup.Framework.Graphics.Interfaces
{
    /// <summary>
    /// Enhanced image processor.  Performs a single
    /// process on an image, returning a resulting <see cref="EnhancedImage"/>.
    /// Process SHOULD dispose of original image to prevent memory
    /// leaks.
    /// </summary>
    public interface IImageProcessor
    {
        Image Process(Image image);
    }
}
