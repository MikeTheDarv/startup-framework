﻿using System.Drawing.Imaging;
using System.IO;

namespace Startup.Framework.Graphics
{
    internal static class Utility
    {
        public static ImageFormat GetImageFormat(string imageName)
        {
            string extension = (Path.GetExtension(imageName) ?? string.Empty).TrimStart(new[] { '.' });
            switch (extension.ToLower())
            {
                case "png":
                    return ImageFormat.Png;
                case "bmp":
                    return ImageFormat.Bmp;
                case "gif":
                    return ImageFormat.Gif;
                default:
                    return ImageFormat.Jpeg;
            }
        }
    }
}
