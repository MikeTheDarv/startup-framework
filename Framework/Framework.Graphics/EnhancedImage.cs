﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics
{
    public class EnhancedImage : IDisposable
    {
        #region Private Variables
        private Image _rawImage;
        private readonly List<IImageProcessor> _processors = new List<IImageProcessor>();
        #endregion

        #region Properties
        public Image RawImage
        {
            get { return _rawImage; }
            internal set { _rawImage = value; }
        }
        #endregion

        #region Constructors
        internal EnhancedImage() { }

        public EnhancedImage(Image original)
        {
            _rawImage = new Bitmap(original);
        }

        public EnhancedImage(Stream stream)
        {
            _rawImage = new Bitmap(stream);
        }

        public EnhancedImage(string filename)
        {
            _rawImage = new Bitmap(filename);
        }

        public EnhancedImage(IntPtr hicon)
        {
            _rawImage = Bitmap.FromHicon(hicon);
        }

        public EnhancedImage(IntPtr hinstance, string bitmapName)
        {
            _rawImage = Bitmap.FromResource(hinstance, bitmapName);
        }

        public EnhancedImage(Image original, Size newSize)
        {
            _rawImage = new Bitmap(original, newSize);
        }

        public EnhancedImage(int width, int height)
        {
            _rawImage = new Bitmap(width, height);
        }

        public EnhancedImage(Stream stream, bool useIcm)
        {
            _rawImage = new Bitmap(stream, useIcm);
        }

        public EnhancedImage(string filename, bool useIcm)
        {
            _rawImage = new Bitmap(filename, useIcm);
        }

        public EnhancedImage(Type type, string resource)
        {
            _rawImage = new Bitmap(type, resource);
        }

        public EnhancedImage(Image original, int width, int height)
        {
            _rawImage = new Bitmap(original, width, height);
        }

        public EnhancedImage(int width, int height, System.Drawing.Graphics g)
        {
            _rawImage = new Bitmap(width, height, g);
        }

        public EnhancedImage(int width, int height, PixelFormat format)
        {
            _rawImage = new Bitmap(width, height, format);
        }

        public EnhancedImage(int width, int height, int stride, PixelFormat format, IntPtr scan0)
        {
            _rawImage = new Bitmap(width, height, stride, format, scan0);
        }
        #endregion

        #region Implementation of IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            if (_rawImage != null)
            {
                _rawImage.Dispose();
            }
        }
        #endregion

        #region Public Methods
        public void AddProcessor(IImageProcessor processor)
        {
            _processors.Add(processor);
        }

        public void Process()
        {
            foreach (IImageProcessor processor in _processors)
            {
                _rawImage = processor.Process(_rawImage);
            }
        }

        public void Save(string filePath, bool recodeImage = false)
        {
            if (!recodeImage)
            {
                _rawImage.Save(filePath, Utility.GetImageFormat(filePath));
                return;
            }

            using (Bitmap output = new Bitmap(_rawImage.Width, _rawImage.Height))
            {
                RecodeImage(output);
                output.Save(filePath, Utility.GetImageFormat(filePath));
            }
        }

        public void Save(Stream outputStream, ImageFormat imageFormat, bool recodeImage = false)
        {
            if (!recodeImage)
            {
                _rawImage.Save(outputStream, imageFormat);
                return;
            }

            using (Bitmap output = new Bitmap(_rawImage.Width, _rawImage.Height))
            {
                RecodeImage(output);
                output.Save(outputStream, imageFormat);
            }
        }

        public void Save(Stream outputStream, ImageCodecInfo codecInfo, EncoderParameters parameters, bool recodeImage = false)
        {
            if (!recodeImage)
            {
                _rawImage.Save(outputStream, codecInfo, parameters);
                return;
            }

            using (Bitmap output = new Bitmap(_rawImage.Width, _rawImage.Height))
            {
                RecodeImage(output);
                output.Save(outputStream, codecInfo, parameters);
            }
        }

        private void RecodeImage(Bitmap output)
        {
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(output))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.DrawImage(_rawImage, 0, 0, _rawImage.Width, _rawImage.Height);
            }
        }
        #endregion
    }
}
