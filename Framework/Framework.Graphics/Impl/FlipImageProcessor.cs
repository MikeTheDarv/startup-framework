﻿using System.Drawing;
using System.Drawing.Drawing2D;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics.Impl
{
    public class FlipImageProcessor: IImageProcessor
    {
        #region Private Variables
        private readonly FlipDirection _direction;
        private SmoothingMode _smoothingMode = SmoothingMode.AntiAlias;
        private InterpolationMode _interpolationMode = InterpolationMode.HighQualityBicubic;
        private CompositingQuality _compositingQuality = CompositingQuality.HighQuality;
        private PixelOffsetMode _pixelOffsetMode = PixelOffsetMode.HighQuality;
        private GraphicsUnit _graphicsUnit = GraphicsUnit.Pixel;
        #endregion

        #region Properties
        public virtual FlipDirection Direction
        {
            get { return _direction; }
        }

        public virtual SmoothingMode SmoothingMode
        {
            get { return _smoothingMode; }
            set { _smoothingMode = value; }
        }

        public virtual InterpolationMode InterpolationMode
        {
            get { return _interpolationMode; }
            set { _interpolationMode = value; }
        }

        public virtual CompositingQuality CompositingQuality
        {
            get { return _compositingQuality; }
            set { _compositingQuality = value; }
        }

        public virtual PixelOffsetMode PixelOffsetMode
        {
            get { return _pixelOffsetMode; }
            set { _pixelOffsetMode = value; }
        }

        public virtual GraphicsUnit GraphicsUnit
        {
            get { return _graphicsUnit; }
            set { _graphicsUnit = value; }
        }
        #endregion

        #region Constructors
        public FlipImageProcessor(FlipDirection direction)
        {
            _direction = direction;
        }
        #endregion

        #region Implementation of IProcessor

        public Image Process(Image image)
        {
            Image enhancedImage = new Bitmap(image.Width, image.Height);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                Matrix matrix;
                
                if (Direction == FlipDirection.Horizontal)
                {
                    matrix = new Matrix(-1, 0, 0, 1, 0, 0);
                    matrix.Translate(image.Width, 0, MatrixOrder.Append);
                }
                else
                {
                    matrix =new Matrix(1, 0, 0, -1, 0, 0);
                    matrix.Translate(0, image.Height, MatrixOrder.Append);
                }

                g.Transform = matrix;
                g.DrawImage(image, 0, 0, new Rectangle(0, 0, image.Width, image.Height), _graphicsUnit);

                matrix.Dispose();
            }

            return enhancedImage;
        }

        #endregion

        #region NestedEnum
        public enum FlipDirection
        {
            Horizontal,
            Vertical
        }
        #endregion
    }
}
