﻿using System.Drawing;
using System.Drawing.Drawing2D;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics.Impl
{
    public class ResizeImageProcessor: IImageProcessor
    {
        #region Private Variables
        private readonly Size _size;
        private SmoothingMode _smoothingMode = SmoothingMode.AntiAlias;
        private InterpolationMode _interpolationMode = InterpolationMode.HighQualityBicubic;
        private CompositingQuality _compositingQuality = CompositingQuality.HighQuality;
        private PixelOffsetMode _pixelOffsetMode = PixelOffsetMode.HighQuality;
        private GraphicsUnit _graphicsUnit = GraphicsUnit.Pixel;
        #endregion

        #region Properties
        public virtual Size Size
        {
            get { return _size; }
        }

        public virtual SmoothingMode SmoothingMode
        {
            get { return _smoothingMode; }
            set { _smoothingMode = value; }
        }

        public virtual InterpolationMode InterpolationMode
        {
            get { return _interpolationMode; }
            set { _interpolationMode = value; }
        }

        public virtual CompositingQuality CompositingQuality
        {
            get { return _compositingQuality; }
            set { _compositingQuality = value; }
        }

        public virtual PixelOffsetMode PixelOffsetMode
        {
            get { return _pixelOffsetMode; }
            set { _pixelOffsetMode = value; }
        }

        public virtual GraphicsUnit GraphicsUnit
        {
            get { return _graphicsUnit; }
            set { _graphicsUnit = value; }
        }
        #endregion

        #region Constructors
        public ResizeImageProcessor(Size size)
        {
            _size = size;
        }
        #endregion

        #region Implementation of IProcessor

        public Image Process(Image image)
        {
            Image enhancedImage = new Bitmap(_size.Width, _size.Height);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                g.DrawImage(image, 0, 0, _size.Width, _size.Height);
            }

            return enhancedImage;
        }

        #endregion
    }
}
