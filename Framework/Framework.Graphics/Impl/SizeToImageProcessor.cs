﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics.Impl
{
    public class SizeToImageProcessor: IImageProcessor
    {
        #region Private Variables
        private readonly Size _size;
        private SmoothingMode _smoothingMode = SmoothingMode.AntiAlias;
        private InterpolationMode _interpolationMode = InterpolationMode.HighQualityBicubic;
        private CompositingQuality _compositingQuality = CompositingQuality.HighQuality;
        private PixelOffsetMode _pixelOffsetMode = PixelOffsetMode.HighQuality;
        private GraphicsUnit _graphicsUnit = GraphicsUnit.Pixel;
        private AnchorPoint _anchor = AnchorPoint.TopLeft;
        #endregion

        #region Properties
        public virtual Size Size
        {
            get { return _size; }
        }

        public virtual AnchorPoint Anchor
        {
            get { return _anchor; }
            set { _anchor = value; }
        }

        public virtual SmoothingMode SmoothingMode
        {
            get { return _smoothingMode; }
            set { _smoothingMode = value; }
        }

        public virtual InterpolationMode InterpolationMode
        {
            get { return _interpolationMode; }
            set { _interpolationMode = value; }
        }

        public virtual CompositingQuality CompositingQuality
        {
            get { return _compositingQuality; }
            set { _compositingQuality = value; }
        }

        public virtual PixelOffsetMode PixelOffsetMode
        {
            get { return _pixelOffsetMode; }
            set { _pixelOffsetMode = value; }
        }

        public virtual GraphicsUnit GraphicsUnit
        {
            get { return _graphicsUnit; }
            set { _graphicsUnit = value; }
        }
        #endregion

        #region Constructors
        public SizeToImageProcessor(Size size, AnchorPoint anchorPoint)
        {
            _size = size;
            _anchor = anchorPoint;
        }
        #endregion

        #region Implementation of IProcessor

        public Image Process(Image image)
        {
            if (_size.Width <= 0 && _size.Height <= 0)
            {
                throw new ArgumentException(
                    "Invalid size specified to SizeTo processor.  At least one dimension must be > 0.");
            }

            int width = _size.Width, height = _size.Height;
            if (_size.Width <= 0)
            {
                double delta = _size.Height * 1.0 / image.Height * 1.0;
                width = Convert.ToInt32(image.Width * delta);
                height = _size.Height;
            }

            if (_size.Height <= 0)
            {
                double delta = _size.Width * 1.0 / image.Width * 1.0;
                width = _size.Width;
                height = Convert.ToInt32(image.Height * delta);
            }
            
            Image enhancedImage = new Bitmap(width, height);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                Point p = GetPointFromAnchor(image.Width, image.Height, width, height);

                int sourceX = width < image.Width ? 0 : p.X;
                int sourceY = height < image.Height ? 0 : p.Y;
                int destX = width < image.Width ? p.X : 0;
                int destY = height < image.Height ? p.Y : 0;

                g.DrawImage(image, new Rectangle(sourceX, sourceY, width, height), new Rectangle(destX, destY, width, height), _graphicsUnit);
            }

            return enhancedImage;
        }

        #endregion

        #region Private Methods
        private Point GetPointFromAnchor(int width, int height, int targetWidth, int targetHeight)
        {
            int x = 0, y = 0;

            switch (_anchor)
            {
                case AnchorPoint.TopLeft:
                    x = 0;
                    y = 0;
                    break;
                case AnchorPoint.TopMiddle:
                    x = (int) Math.Abs((width/2.0) - (targetWidth/2.0));
                    y = 0;
                    break;
                case AnchorPoint.TopRight:
                    x = Math.Abs(width - targetWidth);
                    y = 0;
                    break;
                case AnchorPoint.LeftMiddle:
                    x = 0;
                    y = (int) Math.Abs((height/2.0) - (targetHeight/2.0));
                    break;
                case AnchorPoint.Middle:
                    x = (int)Math.Abs((width / 2.0) - (targetWidth / 2.0));
                    y = (int)Math.Abs((height / 2.0) - (targetHeight / 2.0));
                    break;
                case AnchorPoint.RightMiddle:
                    x = Math.Abs(width - targetWidth);
                    y = (int)Math.Abs((height / 2.0) - (targetHeight / 2.0));
                    break;
                case AnchorPoint.BottomLeft:
                    x = 0;
                    y = Math.Abs(height - targetHeight);
                    break;
                case AnchorPoint.BottomMiddle:
                    x = (int)Math.Abs((width / 2.0) - (targetWidth / 2.0));
                    y = Math.Abs(height - targetHeight);
                    break;
                case AnchorPoint.BottomRight:
                    x = Math.Abs(width - targetWidth);
                    y = Math.Abs(height - targetHeight);
                    break;
            }

            return new Point(x, y);
        }
        #endregion

        #region Nested Enum: AnchorPoint
        public enum AnchorPoint
        {
            TopLeft,
            TopMiddle,
            TopRight,

            LeftMiddle,
            Middle,
            RightMiddle,

            BottomLeft,
            BottomMiddle,
            BottomRight
        }
        #endregion
    }
}
