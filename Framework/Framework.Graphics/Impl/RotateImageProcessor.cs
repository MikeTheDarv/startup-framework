﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics.Impl
{
    public class RotateImageProcessor: IImageProcessor
    {
        #region Private Variables
        private readonly double _angle;
        private SmoothingMode _smoothingMode = SmoothingMode.AntiAlias;
        private InterpolationMode _interpolationMode = InterpolationMode.HighQualityBicubic;
        private CompositingQuality _compositingQuality = CompositingQuality.HighQuality;
        private PixelOffsetMode _pixelOffsetMode = PixelOffsetMode.HighQuality;
        private GraphicsUnit _graphicsUnit = GraphicsUnit.Pixel;
        #endregion

        #region Properties
        public virtual double Angle
        {
            get { return _angle; }
        }

        public virtual SmoothingMode SmoothingMode
        {
            get { return _smoothingMode; }
            set { _smoothingMode = value; }
        }

        public virtual InterpolationMode InterpolationMode
        {
            get { return _interpolationMode; }
            set { _interpolationMode = value; }
        }

        public virtual CompositingQuality CompositingQuality
        {
            get { return _compositingQuality; }
            set { _compositingQuality = value; }
        }

        public virtual PixelOffsetMode PixelOffsetMode
        {
            get { return _pixelOffsetMode; }
            set { _pixelOffsetMode = value; }
        }

        public virtual GraphicsUnit GraphicsUnit
        {
            get { return _graphicsUnit; }
            set { _graphicsUnit = value; }
        }
        #endregion

        #region Constructors
        public RotateImageProcessor(double angle)
        {
            _angle = angle;
        }
        #endregion

        #region Implementation of IProcessor
        public Image Process(Image image)
        {
            float angle = (float)Angle%360.0F;

            // Ensure angle is [0, 360)
            if (angle < 0.0) angle += 360.0F;

            // Corners, clockwise starting at upper left
            PointF[] corners =
            {
                new PointF(0, 0),
                new PointF(image.Width, 0),
                new PointF(image.Width, image.Height),
                new PointF(0, image.Height)
            };

            double maxX = Double.MinValue;
            double maxY = Double.MinValue;
            double minX = Double.MaxValue;
            double minY = Double.MaxValue;

            // -- Rotate about the origin, compute the
            //    new bounds of transformed corners
            using (Matrix matrix = new Matrix())
            {
                matrix.Rotate(angle);
                matrix.TransformPoints(corners);

                foreach (PointF p in corners)
                {
                    maxX = Math.Max(maxX, p.X);
                    minX = Math.Min(minX, p.X);
                    maxY = Math.Max(maxY, p.Y);
                    minY = Math.Min(minY, p.Y);
                }
            }

            double newWidth = Math.Floor(maxX - minX);
            double newHeight = Math.Floor(maxY - minY);

            Image enhancedImage = new Bitmap((int)newWidth, (int)newHeight);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                // -- Center of new image
                PointF center = new PointF((float)(newWidth / 2.0F), (float)(newHeight / 2.0F));              

                // -- Center position of old image into rotated one
                PointF position = new PointF(center.X - (float)(image.Width / 2.0), center.Y - (float)(image.Height / 2.0));

                Console.WriteLine("Center: " + center.X + " - " + center.Y);
                Console.WriteLine("position: " + position.X + " - " + position.Y);

                using (Matrix matrix = new Matrix())
                {
                    matrix.RotateAt(angle, center);
                    //matrix.Rotate(angle);

                    g.SmoothingMode = _smoothingMode;
                    g.InterpolationMode = _interpolationMode;
                    g.CompositingQuality = _compositingQuality;
                    g.PixelOffsetMode = _pixelOffsetMode;

                    g.Transform = matrix;
                    g.DrawImage(image, position);
                }
            }

            return enhancedImage;
        }

        #endregion
    }
}
