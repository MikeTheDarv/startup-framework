﻿using System.Drawing;
using System.Drawing.Drawing2D;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics.Impl
{
    public class CropImageProcessor: IImageProcessor
    {
        #region Private Variables
        private readonly Rectangle _cropArea;
        private SmoothingMode _smoothingMode = SmoothingMode.AntiAlias;
        private InterpolationMode _interpolationMode = InterpolationMode.HighQualityBicubic;
        private CompositingQuality _compositingQuality = CompositingQuality.HighQuality;
        private PixelOffsetMode _pixelOffsetMode = PixelOffsetMode.HighQuality;
        private GraphicsUnit _graphicsUnit = GraphicsUnit.Pixel;
        #endregion

        #region Properties
        public virtual Rectangle CropArea
        {
            get { return _cropArea; }
        }

        public virtual SmoothingMode SmoothingMode
        {
            get { return _smoothingMode; }
            set { _smoothingMode = value; }
        }

        public virtual InterpolationMode InterpolationMode
        {
            get { return _interpolationMode; }
            set { _interpolationMode = value; }
        }

        public virtual CompositingQuality CompositingQuality
        {
            get { return _compositingQuality; }
            set { _compositingQuality = value; }
        }

        public virtual PixelOffsetMode PixelOffsetMode
        {
            get { return _pixelOffsetMode; }
            set { _pixelOffsetMode = value; }
        }

        public virtual GraphicsUnit GraphicsUnit
        {
            get { return _graphicsUnit; }
            set { _graphicsUnit = value; }
        }
        #endregion

        #region Constructors
        public CropImageProcessor(Rectangle cropArea)
        {
            _cropArea = cropArea;
        }
        #endregion

        #region Implementation of IProcessor

        public Image Process(Image image)
        {
            // -- Validate dimensions
            int width = _cropArea.X + _cropArea.Width > image.Width
                            ? (image.Width - _cropArea.X)
                            : _cropArea.Width;

            int height = _cropArea.Y + _cropArea.Height > image.Height
                             ? image.Height - _cropArea.Y
                             : _cropArea.Height;

            Image enhancedImage = new Bitmap(width, height);


            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                g.DrawImage(image, new Rectangle(0, 0, width, height), new Rectangle(_cropArea.X, _cropArea.Y, width, height), _graphicsUnit);
            }

            return enhancedImage;
        }

        #endregion
    }
}
