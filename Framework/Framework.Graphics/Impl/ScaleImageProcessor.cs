﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Startup.Framework.Graphics.Interfaces;

namespace Startup.Framework.Graphics.Impl
{
    public class ScaleImageProcessor: IImageProcessor
    {
        #region Private Variables
        private readonly ScaleType _scalingMethod;
        private readonly Size _size;
        private readonly double _percent;
        private readonly bool _isByPercent;
        private SmoothingMode _smoothingMode = SmoothingMode.AntiAlias;
        private InterpolationMode _interpolationMode = InterpolationMode.HighQualityBicubic;
        private CompositingQuality _compositingQuality = CompositingQuality.HighQuality;
        private PixelOffsetMode _pixelOffsetMode = PixelOffsetMode.HighQuality;
        private GraphicsUnit _graphicsUnit = GraphicsUnit.Pixel;
        #endregion

        #region Properties
        public virtual Size Size
        {
            get { return _size; }
        }

        public virtual ScaleType ScalingMethod
        {
            get { return _scalingMethod; }
        }

        public virtual SmoothingMode SmoothingMode
        {
            get { return _smoothingMode; }
            set { _smoothingMode = value; }
        }

        public virtual InterpolationMode InterpolationMode
        {
            get { return _interpolationMode; }
            set { _interpolationMode = value; }
        }

        public virtual CompositingQuality CompositingQuality
        {
            get { return _compositingQuality; }
            set { _compositingQuality = value; }
        }

        public virtual PixelOffsetMode PixelOffsetMode
        {
            get { return _pixelOffsetMode; }
            set { _pixelOffsetMode = value; }
        }

        public virtual GraphicsUnit GraphicsUnit
        {
            get { return _graphicsUnit; }
            set { _graphicsUnit = value; }
        }
        #endregion

        #region Constructors
        public ScaleImageProcessor(double percent)
        {
            _isByPercent = true;
            _percent = percent;
        }

        public ScaleImageProcessor(ScaleType scalingMethod, Size size)
        {
            _scalingMethod = scalingMethod;
            _size = size;
        }
        #endregion

        #region Implementation of IProcessor

        public Image Process(Image image)
        {
            if (_isByPercent)
            {
                return ScaleToPercent(image);
            }

            switch (_scalingMethod)
            {
                case ScaleType.ToWidth:
                    return ScaleToWidth(image);
                case ScaleType.ToHeight:
                    return ScaleToHeight(image);
                case ScaleType.Fixed:
                    return ScaleToFixed(image);
                default:
                    throw new ArgumentOutOfRangeException(
                        string.Format("Invalid option specified to scale processor.  Value was [{0}].", _scalingMethod));
            }
        }

        #endregion

        #region Nested Enum: ScaleType
        public enum ScaleType
        {
            ToWidth,
            ToHeight,
            Fixed, // -- To width and height w/Padding
        }
        #endregion

        #region Private Methods
        private Image ScaleToWidth(Image image)
        {
            double delta = _size.Width * 1.0 / image.Width * 1.0;

            int newWidth = _size.Width;
            int newHeight = Convert.ToInt32(image.Height * delta);

            Image enhancedImage = new Bitmap(newWidth, newHeight);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                g.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight), new Rectangle(0, 0, image.Width, image.Height), _graphicsUnit);
            }

            return enhancedImage;
        }

        private Image ScaleToHeight(Image image)
        {
            double delta = _size.Height * 1.0 / image.Height * 1.0;

            int newWidth = Convert.ToInt32(image.Width * delta);
            int newHeight = _size.Height;

            Image enhancedImage = new Bitmap(newWidth, newHeight);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                g.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight), new Rectangle(0, 0, image.Width, image.Height), _graphicsUnit);
            }

            return enhancedImage;
        }

        private Image ScaleToPercent(Image image)
        {
            int newWidth = Convert.ToInt32(image.Width * (_percent / 100.0));
            int newHeight = Convert.ToInt32(image.Height * (_percent / 100.0));

            Image enhancedImage = new Bitmap(newWidth, newHeight);

            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                g.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return enhancedImage;
        }

        private Image ScaleToFixed(Image image)
        {
            int newWidth, newHeight;

            if (image.Width > image.Height)
            {
                double delta = _size.Width * 1.0 / image.Width * 1.0;
                newWidth = _size.Width;
                newHeight = Convert.ToInt32(image.Height * delta);

                // -- Now test height for containment
                if (newHeight > _size.Height)
                {
                    delta = _size.Height * 1.0 / newHeight * 1.0;
                    newWidth = Convert.ToInt32(newWidth * delta);
                    newHeight = _size.Height;
                }
            }
            else
            {
                double delta = _size.Height * 1.0 / image.Height * 1.0;
                newWidth = Convert.ToInt32(image.Width * delta);
                newHeight = _size.Height;

                // -- Now test width for containment
                if (newWidth > _size.Width)
                {
                    delta = _size.Width * 1.0 / image.Width * 1.0;
                    newWidth = _size.Width;
                    newHeight = Convert.ToInt32(newHeight * delta);
                }
            }
            
            int x = Convert.ToInt32((_size.Width/2.0) - (newWidth/2.0));

            int y = Convert.ToInt32((_size.Height/2.0) - (newHeight/2.0));

            Image enhancedImage = new Bitmap(_size.Width, _size.Height);
            
            using (image)
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(enhancedImage))
            {
                g.SmoothingMode = _smoothingMode;
                g.InterpolationMode = _interpolationMode;
                g.CompositingQuality = _compositingQuality;
                g.PixelOffsetMode = _pixelOffsetMode;

                g.DrawImage(image, new Rectangle(x, y, newWidth, newHeight), new Rectangle(0, 0, image.Width, image.Height), _graphicsUnit);
            }

            return enhancedImage;
        }
        #endregion
    }
}
