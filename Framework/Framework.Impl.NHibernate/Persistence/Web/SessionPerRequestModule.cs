﻿using System;
using System.Web;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.NHibernate.Persistence.Web
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// 
    ///     Taken from http://nhforge.org/blogs/nhibernate/archive/2011/03/03/effective-nhibernate-session-management-for-web-apps.aspx
    /// </summary>
    public class SessionPerRequestModule : IHttpModule
    {
        #region Private Variables
        private readonly ISessionManager _sessionManager;
        #endregion

        #region Constructors
        public SessionPerRequestModule(ISessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }
        #endregion

        #region Implementation of IHttpModule
        public void Init(HttpApplication context)
        {
            context.BeginRequest += OnBeginRequest;
            context.EndRequest += OnEndRequest;
            context.Error += OnError;
        }
        
        public void Dispose()
        {
            
        }
        #endregion

        #region Private Methods
        private void OnBeginRequest(object sender, EventArgs e)
        {
            _sessionManager.BeginSession();
        }

        private void OnEndRequest(object sender, EventArgs e)
        {
            _sessionManager.EndSession(false);
        }

        private void OnError(object sender, EventArgs e)
        {
            _sessionManager.EndSession(false);
        }
        #endregion
    }
}
