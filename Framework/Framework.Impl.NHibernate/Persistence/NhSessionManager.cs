﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.NHibernate.Persistence
{
    public class NhSessionManager : ISessionManager
    {
        #region Private Variables
        private readonly IEnumerable<ISessionFactory> _sessionFactories;
        #endregion

        #region Constructors
        public NhSessionManager(IEnumerable<ISessionFactory> sessionFactories)
        {
            _sessionFactories = sessionFactories;
        }
        #endregion

        #region Implementation of ISessionManager
        public void BeginSession()
        {
            foreach (var sessionFactory in GetSessionFactories())
            {
                var localFactory = sessionFactory;

                NhSessionContext.Bind(new Lazy<ISession>(() => BeginSession(localFactory)), sessionFactory);
            }
        }

        public void EndSession(bool commitTransaction)
        {
            foreach (var sessionfactory in GetSessionFactories())
            {
                var session = NhSessionContext.UnBind(sessionfactory);
                if (session == null) continue;
                EndSession(session, commitTransaction);
            }
        }
        #endregion

        #region Private Methods
        private IEnumerable<ISessionFactory> GetSessionFactories()
        {
            if (_sessionFactories == null || !_sessionFactories.Any())
            {
                throw new TypeLoadException("At least one ISessionFactory has not been registered with IoC");
            }

            return _sessionFactories;
        }

        private static ISession BeginSession(ISessionFactory sessionFactory)
        {
            var session = sessionFactory.OpenSession();
            session.BeginTransaction();
            return session;
        }

        private static void EndSession(ISession session, bool commitTransaction)
        {
            try
            {
                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    if (commitTransaction)
                    {
                        try
                        {
                            session.Transaction.Commit();
                        }
                        catch
                        {
                            session.Transaction.Rollback();
                            throw;
                        }
                    }
                    else
                    {
                        session.Transaction.Rollback();
                    }
                }
            }
            finally
            {
                if (session.IsOpen)
                {
                    session.Close();
                }

                session.Dispose();
            }
        }
        #endregion
    }
}
