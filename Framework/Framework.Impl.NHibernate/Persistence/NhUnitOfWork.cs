﻿using System;
using NHibernate;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.NHibernate.Persistence
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// </summary>
    public class NhUnitOfWork : IUnitOfWork
    {
        #region Private Variables
        private readonly ISessionFactory _sessionFactory;
        private readonly ITransaction _transaction;
        private bool _disposed;
        #endregion

        #region Constructors
        public NhUnitOfWork(ISessionFactory sessionFactory)
        {
            if (sessionFactory == null) throw new ArgumentNullException("sessionFactory");

            _sessionFactory = sessionFactory;
            _transaction = _sessionFactory.GetCurrentSession().BeginTransaction();
        }
        #endregion

        #region Implementation of IUnitOfWork
        /// <summary>
        /// This isn't specific to any one DAO and flushes everything that has been
        /// changed since the last commit.
        /// </summary>
        public virtual void Flush()
        {
            _sessionFactory.GetCurrentSession().Flush();
        }

        public virtual void Commit()
        {
            _sessionFactory.GetCurrentSession().Transaction.Commit();
        }

        public virtual void Rollback()
        {
            _sessionFactory.GetCurrentSession().Transaction.Rollback();
        }
        #endregion

        #region Implementation of IDisposable
        public void Dispose()
        {
            if (!_disposed)
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                }
            }

            _disposed = true;

            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
