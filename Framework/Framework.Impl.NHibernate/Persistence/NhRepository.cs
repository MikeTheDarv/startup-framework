﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using Startup.Framework.Persistence.Interfaces;

namespace Startup.Framework.Impl.NHibernate.Persistence
{
    /// <summary>
    ///     Taken from SharpLite project v0.9
    ///     Migrated into existing framework
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NhRepository<T> : NhRepositoryWithTypedId<T, int>, IRepository<T> where T : class
    {
        public NhRepository(ISessionFactory sessionFactory) : base(sessionFactory) { }
    }

    public class NhRepositoryWithTypedId<T, TId> : IRepositoryWithTypedId<T, TId> where T : class
    {
        #region Private Variables
        private readonly ISessionFactory _sessionFactory;
        #endregion

        #region Constructors
        public NhRepositoryWithTypedId(ISessionFactory sessionFactory)
        {
            if (sessionFactory == null) throw new ArgumentNullException("sessionFactory");

            _sessionFactory = sessionFactory;
        }
        #endregion

        #region Protected Properties
        protected virtual ISession Session
        {
            get
            {
                return _sessionFactory.GetCurrentSession();
            }
        }
        #endregion

        #region Implementation of IRepository
        public virtual IUnitOfWork UnitOfWork
        {
            get
            {
                return new NhUnitOfWork(_sessionFactory);
            }
        }

        public virtual T Select(TId id)
        {
            return Session.Get<T>(id);
        }

        public virtual T Proxy(TId id)
        {
            return Session.Load<T>(id);
        }

        public virtual IEnumerable<T> SelectAll()
        {
            return QueryAll().ToList();
        }

        public virtual IQueryable<T> QueryAll()
        {
            return Session.Query<T>();
        }

        public virtual int ExecuteQuery(string query, Dictionary<string, object> parameters, Dictionary<string, IEnumerable> parameterLists)
        {
            var dbQuery = Session.CreateQuery(query);
            
            if (parameters != null && parameters.Keys.Count > 0)
            {
                foreach (var key in parameters.Keys)
                {
                    dbQuery.SetParameter(key, parameters[key]);
                }
            }

            if (parameterLists != null && parameterLists.Keys.Count > 0)
            {
                foreach (var key in parameterLists.Keys)
                {
                    dbQuery.SetParameterList(key, parameterLists[key]);
                }
            }
            
            return dbQuery.ExecuteUpdate();
        }

        public virtual T SaveOrUpdate(T entity)
        {
            Session.SaveOrUpdate(entity);
            return entity;
        }

        /// <summary>
        /// This deletes the object and commits the deletion immediately.  We don't want to delay deletion
        /// until a transaction commits, as it may throw a foreign key constraint exception which we could
        /// likely handle and inform the user about.  Accordingly, this tries to delete right away; if there
        /// is a foreign key constraint preventing the deletion, an exception will be thrown.
        /// </summary>
        public virtual void Delete(T entity)
        {
            Session.Delete(entity);
            Session.Flush();
        }
        #endregion
    }
}
