﻿using System;
using NHibernate.Event;
using Startup.Framework.Domain.Interfaces;
using Startup.Framework.Validation;
using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Impl.NHibernate.Validation
{
    [Obsolete("Validation listener deprecated, validation moved to service layer.")]
    public class NhEntityValidationInsertEventListener<TPrimaryKey> : IPreInsertEventListener
    {
        #region Private Variables
        private readonly IValidationProvider _validatorProvider;
        #endregion

        #region Constructors
        public NhEntityValidationInsertEventListener(IValidationProvider validatorProvider)
        {
            _validatorProvider = validatorProvider;
        }
        #endregion

        #region IPreInsertEventListener Members
        public bool OnPreInsert(PreInsertEvent @event)
        {
            return DoPreInsert(@event);
        }
        #endregion

        #region Virtual Methods
        public virtual bool PreInsert(PreInsertEvent @event)
        {
            return false;
        }
        #endregion

        #region Private Methods
        private bool DoPreInsert(PreInsertEvent @event)
        {
            var entity = @event.Entity as IEntityWithTypedId<TPrimaryKey>;

            if (entity == null)
            {
                return PreInsert(@event);
            }

            var validator = _validatorProvider.GetValidator(entity.GetType());

            if (validator != null && !validator.Validate(entity))
            {
                throw new ValidationException(validator.ValidationErrors);
            }
            
            return PreInsert(@event);
        }
        #endregion
    }
}
