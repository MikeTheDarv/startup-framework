﻿using System;
using NHibernate.Event;

namespace Startup.Framework.Impl.NHibernate.Validation
{
    [Obsolete("Validation listener deprecated, validation moved to service layer.")]
    public class NhEntityValidationDeleteEventListener : IPreDeleteEventListener
    {
        #region Implementation of IPreDeleteEventListener
        public bool OnPreDelete(PreDeleteEvent @event)
        {
            return DoPreDelete(@event);
        }
        #endregion

        #region Virtual Methods
        public virtual bool PreDelete(PreDeleteEvent @event)
        {
            return false;
        }
        #endregion

        #region Private Methods
        private bool DoPreDelete(PreDeleteEvent @event)
        {
            return PreDelete(@event);
        }
        #endregion
    }
}
