﻿using System;
using NHibernate.Event;
using Startup.Framework.Domain.Interfaces;
using Startup.Framework.Validation;
using Startup.Framework.Validation.Interfaces;

namespace Startup.Framework.Impl.NHibernate.Validation
{
    [Obsolete("Validation listener deprecated, validation moved to service layer.")]
    public class NhEntityValidationUpdateEventListener<TPrimaryKey> : IPreUpdateEventListener
    {
        #region Private Variables
        private readonly IValidationProvider _validatorProvider;
        #endregion

        #region Constructors
        public NhEntityValidationUpdateEventListener(IValidationProvider validatorProvider)
        {
            _validatorProvider = validatorProvider;
        }
        #endregion

        #region IPreInsertEventListener Members
        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            return DoPreUpdate(@event);
        }
        #endregion

        #region Virtual Methods
        public virtual bool PreUpdate(PreUpdateEvent @event)
        {
            return false;
        }
        #endregion

        #region Private Methods
        private bool DoPreUpdate(PreUpdateEvent @event)
        {
            var entity = @event.Entity as IEntityWithTypedId<TPrimaryKey>;

            if (entity == null)
            {
                return PreUpdate(@event);
            }

            var validator = _validatorProvider.GetValidator(entity.GetType());

            if (validator != null && !validator.Validate(entity))
            {
                throw new ValidationException(validator.ValidationErrors);
            }

            return PreUpdate(@event);
        }
        #endregion
    }
}
