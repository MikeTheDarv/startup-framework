﻿using System.Collections.Generic;

namespace Startup.Framework.Web
{
    public class JsonDataResult
    {
        #region Properties
        public IDictionary<string, IEnumerable<string>>  Errors { get; set; }
        public object D { get; set; }
        #endregion
    }
}
