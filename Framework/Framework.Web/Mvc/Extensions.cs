﻿using System.Web.Mvc;
using Newtonsoft.Json;

namespace Startup.Framework.Web.Mvc
{
    #region Controller Extensions
    public static class ControllerExtensions
    {
        public static JsonNetResult JsonNet(this Controller controller, object data)
        {
            return new JsonNetResult {Data = data};
        }

        public static JsonNetResult JsonNet(this Controller controller, object data, JsonRequestBehavior jsonRequestBehavior)
        {
            return new JsonNetResult { Data = data, JsonRequestBehavior = jsonRequestBehavior };
        }

        public static JsonNetResult JsonNet(this Controller controller, object data, JsonRequestBehavior jsonRequestBehavior, JsonSerializerSettings settings)
        {
            return new JsonNetResult {Data = data, JsonRequestBehavior = jsonRequestBehavior, Settings = settings};
        }
    }
    #endregion
}
