﻿using System;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Startup.Framework.Web.Mvc
{
    public class HttpStatusCodeJsonResult : ActionResult
    {
        #region Properties
        public int StatusCode { get; private set; }

        public object Data { get; private set; }

        public Encoding ContentEncoding { get; set; }

        public string ContentType { get; set; }

        public Formatting Formatting { get; set; }

        public JsonSerializerSettings Settings { get; set; }
        #endregion

        #region Constructors
        public HttpStatusCodeJsonResult(int statusCode)
            : this(statusCode, null)
        {
        }

        public HttpStatusCodeJsonResult(HttpStatusCode statusCode)
            : this(statusCode, null)
        {
        }

        public HttpStatusCodeJsonResult(HttpStatusCode statusCode, object data)
            : this((int)statusCode, data)
        {
        }

        public HttpStatusCodeJsonResult(int statusCode, object data)
        {
            StatusCode = statusCode;
            Data = data;
            Formatting = Formatting.None;
            Settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
        }
        #endregion

        #region Overrides
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;
            
            response.StatusCode = StatusCode;

            response.ContentType = !String.IsNullOrEmpty(ContentType)
                ? ContentType
                : "application/json";

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data != null)
            {
                response.Write(JsonConvert.SerializeObject(Data, Formatting, Settings));
            }
        }
        #endregion
    }
}
