﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Startup.Framework.Web.Mvc
{
    /// <summary>
    /// Simple Json Result that implements the Json.NET serialiser offering more versatile serialisation
    /// </summary>
    public class JsonNetResult : ActionResult
    {
        #region Properties
        public Encoding ContentEncoding { get; set; }

        public string ContentType { get; set; }

        public object Data { get; set; }

        public JsonRequestBehavior JsonRequestBehavior { get; set; }

        public Formatting Formatting { get; set; }

        public JsonSerializerSettings Settings { get; set; }
        #endregion

        #region Constructors

        public JsonNetResult()
        {
            Formatting = Formatting.None;
            JsonRequestBehavior = JsonRequestBehavior.DenyGet;
            Settings = new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()};
        }
        #endregion

        #region Overrides
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            if (JsonRequestBehavior == JsonRequestBehavior.DenyGet &&
                String.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("This request has been blocked because sensitive information could be disclosed to third party web sites when this is used in a GET request. To allow GET requests, set JsonRequestBehavior to AllowGet.");
            }

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !String.IsNullOrEmpty(ContentType)
                ? ContentType
                : "application/json";

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data != null)
            {
                response.Write(JsonConvert.SerializeObject(Data, Formatting, Settings));
            }
        }
        #endregion

        /*
        #region Properties
        /// <summary>Gets or sets the serialiser settings</summary>
        public JsonSerializerSettings Settings { get; set; }

        /// <summary>Gets or sets the encoding of the response</summary>
        public Encoding ContentEncoding { get; set; }

        /// <summary>Gets or sets the content type for the response</summary>
        public string ContentType { get; set; }

        /// <summary>Gets or sets the body of the response</summary>
        public object ResponseBody { get; set; }

        /// <summary>Gets the formatting types depending on whether we are in debug mode</summary>
        private Formatting Formatting
        {
            get
            {
                return Debugger.IsAttached ? Formatting.Indented : Formatting.None;
            }
        }
        #endregion

        #region Constructors
        public JsonNetResult()
        {
        }

        public JsonNetResult(object responseBody)
        {
            ResponseBody = responseBody;
        }

        public JsonNetResult(object responseBody, JsonSerializerSettings settings)
        {
            ResponseBody = responseBody;
            Settings = settings;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Serialises the response and writes it out to the response object
        /// </summary>
        /// <param name="context">The execution context</param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            // set content type
            if (!string.IsNullOrEmpty(ContentType))
            {
                response.ContentType = ContentType;
            }
            else
            {
                response.ContentType = "application/json";
            }

            // set content encoding
            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (ResponseBody != null)
            {
                response.Write(JsonConvert.SerializeObject(ResponseBody, Formatting, Settings));
            }
        }
        #endregion
        */
    }
}
