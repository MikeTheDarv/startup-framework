﻿using System;
using System.Web.Routing;

namespace Startup.Framework.Web.Mvc
{
    public class TrailingSlashRoute : Route
    {
        #region Constructors
        public TrailingSlashRoute(string url, IRouteHandler routeHandler)
            : base(url, routeHandler) { }

        public TrailingSlashRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
            : base(url, defaults, routeHandler) { }

        public TrailingSlashRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
                              IRouteHandler routeHandler)
            : base(url, defaults, constraints, routeHandler) { }

        public TrailingSlashRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
                              RouteValueDictionary dataTokens, IRouteHandler routeHandler)
            : base(url, defaults, constraints, dataTokens, routeHandler) { }
        #endregion

        #region Overrides
        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            VirtualPathData path = base.GetVirtualPath(requestContext, values);

            if (path != null)
            {
                var virtualPath = path.VirtualPath;
                if (!string.IsNullOrWhiteSpace(path.VirtualPath))
                {
                    var queryStringIndex = virtualPath.IndexOf("?", StringComparison.Ordinal);
                    virtualPath = queryStringIndex == -1
                        ? string.Concat(virtualPath, "/")
                        : string.Concat(virtualPath.Substring(0, queryStringIndex), "/", virtualPath.Substring(queryStringIndex));

                    path.VirtualPath = virtualPath;
                }
            }

            return path;
        }
        #endregion
    }
}
