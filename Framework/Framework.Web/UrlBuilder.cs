﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;

namespace Startup.Framework.Web
{
    public class UrlBuilder : UriBuilder
    {
        #region Private Variables
        private StringDictionary _queryStringCollection;
        #endregion

        #region Properties
        public StringDictionary QueryStringCollection
        {
            get { return _queryStringCollection ?? (_queryStringCollection = new StringDictionary()); }
        }

        public string PageName
        {
            get
            {
                return Path.Substring(Path.LastIndexOf("/", StringComparison.Ordinal) + 1);
            }
            set
            {
                string path = Path.Substring(0, Path.LastIndexOf("/", StringComparison.Ordinal));
                Path = string.Concat(path, "/", value);
            }
        }
        #endregion

        #region Constructors
        public UrlBuilder()
        {
        }

        public UrlBuilder(string uri)
            : base(uri)
        {
            PopulateQueryStringCollection();
        }

        public UrlBuilder(Uri uri)
            : base(uri)
        {
            PopulateQueryStringCollection();
        }

        public UrlBuilder(string schemeName, string hostName)
            : base(schemeName, hostName)
        {
        }

        public UrlBuilder(string scheme, string host, int portNumber)
            : base(scheme, host, portNumber)
        {
        }

        public UrlBuilder(string scheme, string host, int port, string pathValue)
            : base(scheme, host, port, pathValue)
        {
        }

        public UrlBuilder(string scheme, string host, int port, string path, string extraValue)
            : base(scheme, host, port, path, extraValue)
        {
        }

        public UrlBuilder(UrlScheme schemeName, string hostName)
            : this(schemeName.ToDescription(), hostName)
        {
        }

        public UrlBuilder(UrlScheme scheme, string host, int portNumber)
            : this(scheme.ToDescription(), host, portNumber)
        {
        }

        public UrlBuilder(UrlScheme scheme, string host, int port, string pathValue)
            : this(scheme.ToDescription(), host, port, pathValue)
        {
        }

        public UrlBuilder(UrlScheme scheme, string host, int port, string path, string extraValue)
            : this(scheme.ToDescription(), host, port, path, extraValue)
        {
        }
        #endregion

        #region Public Methods
        public string ToUrl()
        {
            ParseQueryStringCollection();
            return Uri.AbsoluteUri;
        }
        #endregion

        #region Private methods
        private void PopulateQueryStringCollection()
        {
            string query = Query;

            if (query == string.Empty)
            {
                return;
            }

            // -- Remove prefixed ?
            query = query.Substring(1);

            QueryStringCollection.Clear();

            string[] queryStringValues = query.Split(new[] {'&'}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string queryString in queryStringValues)
            {
                string[] pair = queryString.Split(new[] {'='}, StringSplitOptions.RemoveEmptyEntries);
                string value = pair.Length > 1 ? pair[1] : string.Empty;

                if (QueryStringCollection.ContainsKey(pair[0]))
                {
                    QueryStringCollection[pair[0]] = value;
                }
                else
                {
                    QueryStringCollection.Add(pair[0], value);
                }
            }
        }

        private void ParseQueryStringCollection()
        {
            int count = QueryStringCollection.Count;

            if (count == 0)
            {
                Query = string.Empty;
                return;
            }

            Query = QueryStringCollection.Count.ToString();

            string[] queryStringValues = (from DictionaryEntry entry in QueryStringCollection select string.Concat(entry.Key, "=", entry.Value)).ToArray();
            Query = string.Join("&", queryStringValues);
        }
        #endregion
    }
}
