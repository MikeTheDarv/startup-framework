﻿using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using Startup.Framework.Media.Interfaces;

namespace Startup.Framework.Web.Impl.Media
{
    public abstract class MediaUploaderHttpHandlerBase : HttpContextMediaUploader, IHttpHandler, IRequiresSessionState
    {
        #region Private Variables
        private readonly IMediaStorage _mediaStorage;
        private readonly IMediaUploaderConfiguration _configuration;
        #endregion

        #region Constructors
        protected MediaUploaderHttpHandlerBase(
            IMediaUploaderConfiguration configuration,
            IMediaStorage mediaStorage)
            : base(configuration, mediaStorage)
        {
            _configuration = configuration;
            _mediaStorage = mediaStorage;
        }
        #endregion

        #region Implementation of IHttpHandler
        public void ProcessRequest(HttpContext context)
        {
            SetHeaders();
            HandleMethod();
        }

        public bool IsReusable
        {
            get { return false; }
        }

        #endregion

        #region Private Methods
        private void HandleMethod()
        {
            var context = GetCurrentContext();

            switch (context.Request.HttpMethod.ToUpper())
            {
                case "HEAD":
                case "GET":
                    TryDisplayFile();
                    break;
                case "POST":
                case "PUT":
                    TryUploadFile();
                    break;
                case "DELETE":
                    TryDeleteFile();
                    break;
                default:
                    context.Response.ClearHeaders();
                    context.Response.StatusCode = 405;
                    context.Response.Clear();
                    context.Response.End();
                    break;
            }
        }

        private void SetHeaders()
        {
            var context = GetCurrentContext();

            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");
            context.Response.AddHeader("Vary", "Accept");

            try
            {
                context.Response.ContentType = context.Request["HTTP_ACCEPT"].Contains("application/json")
                                                   ? "application/json"
                                                   : "text/plain";
            }
            catch
            {
                context.Response.ContentType = "text/plain";
            }
        }

        private void TryDisplayFile()
        {
            var context = GetCurrentContext();

            var fileIdentifier = context.Request["guid"] ?? string.Empty;
            if (!_configuration.IsPublic ||
                string.IsNullOrWhiteSpace(fileIdentifier))
            {
                context.Response.StatusCode = 404;
                return;
            }

            try
            {
                using (var fileStream = _mediaStorage.OpenMediaStream(fileIdentifier))
                {
                    context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", fileIdentifier));
                    context.Response.ContentType = "application/octet-stream";
                    context.Response.ClearContent();

                    fileStream.CopyTo(context.Response.OutputStream);
                }
            }
            catch
            {
                context.Response.StatusCode = 404;
            }
        }

        private void TryUploadFile()
        {
            try
            {
                var results = Upload();
                var context = GetCurrentContext();

                var jsonResult = new JsonDataResult {D = results};

                // -- Serialize JSON
                context.Response.StatusCode = 200;
                context.Response.Write(jsonResult.ToJson());
                context.Response.End();
            }
            catch (HttpException e)
            {
                ThrowHttpException(e.GetHttpCode(), e.GetHtmlErrorMessage());
            }
        }

        private void TryDeleteFile()
        {
            var context = GetCurrentContext();
            var fileIdentifier = context.Request["guid"] ?? string.Empty;

            try
            {
                CancelUpload(fileIdentifier);
            }
            catch
            {
                context.Response.StatusCode = 404;
            }
        }

        private void ThrowHttpException(int statusCode, string message)
        {
            var context = GetCurrentContext();
            var errorSet = new Dictionary<string, IEnumerable<string>> { { "Upload", new[] { message } } };
            var result = new JsonDataResult {Errors = errorSet};

            context.Response.Clear();
            context.Response.StatusCode = statusCode;
            context.Response.Write(result.ToJson());
            context.Response.End();
        }

        private HttpContext GetCurrentContext()
        {
            return HttpContext.Current;
        }
        #endregion
    }
}
