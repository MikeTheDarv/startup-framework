﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Startup.Framework.Media.Impl;
using Startup.Framework.Media.Interfaces;

namespace Startup.Framework.Web.Impl.Media
{
    /// <summary>
    /// Implementation of the MediaUploaderBase abstract class
    /// on top of the HttpContext.
    /// * NOTE: Requires HttpHandler to inherit from IRequiresSessionState
    /// </summary>
    public class HttpContextMediaUploader : MediaUploaderBase
    {
        #region Private Variables
        private readonly IMediaStorage _mediaStorage;
        #endregion

        #region Constructors
        public HttpContextMediaUploader(
            IMediaUploaderConfiguration configuration,
            IMediaStorage mediaStorage)
            : base(configuration, mediaStorage)
        {
            _mediaStorage = mediaStorage;
        }
        #endregion

        #region Implementation of IMediaUploader
        protected override IEnumerable<IUploadedMediaAsset> ProcessUploads()
        {
            var mediaAssets = new List<IUploadedMediaAsset>();

            if (IsChunkedUpload())
            {
                mediaAssets.Add(UploadPartialFile());
            }
            else
            {
                mediaAssets.AddRange(UploadAllFiles());
            }

            return mediaAssets;
        }

        protected override IEnumerable<UploadMetaData> GetUploadedMetaData()
        {
            var context = GetContext();
            var headers = context.Request.Headers;
            var metaData = new List<UploadMetaData>();

            if (IsChunkedUpload())
            {
                // -- Chunked upload.  Validate and return necessary meta data
                var contentRange = GetContentRangeFromHeaders(headers);

                if (contentRange.Length != 3 ||
                    context.Request.Files.Count != 1)
                {
                    HandleInvalidFile(new UploadMetaData());
                }
                else
                {
                    // -- jQuery blue imp adding an extra byte for
                    //    some reason.  Not sure if this is by design
                    //    or how this line will effect other uploading scripts.
                    int totalBytes = contentRange[2] - 1;
                    metaData.Add(new UploadMetaData { FileName = context.Request.Files[0].FileName, ContentLength = totalBytes });
                }
            }
            else
            {
                // -- Add meta data from all valid files.
                metaData.AddRange(
                    from string fileKey in context.Request.Files
                    select context.Request.Files[fileKey]
                        into file
                        where file != null && file.ContentLength != 0
                        select new UploadMetaData { FileName = file.FileName, ContentLength = file.ContentLength }
                    );
            }

            return metaData;
        }

        protected override void HandleInvalidFile(UploadMetaData metaData)
        {
            throw new HttpException((int)HttpStatusCode.BadRequest, string.Format("Invalid file provided."));
        }

        protected override void HandleFileExceededMaximumLength(UploadMetaData metaData)
        {
            throw new HttpException((int)HttpStatusCode.BadRequest, "File exceeds maximum upload size.");
        }

        protected override void OnMediaAssetUploaded(IUploadedMediaAsset asset)
        {
        }

        protected override void OnMediaAssetCompleted(string completedFileIdentifier)
        {
        }

        protected override void OnMediaAssetCanceled(string workingFileIdentifier)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Provides a pluggable method to perform post processing
        /// directly on the stream before it is persisted.  This is
        /// useful for images and others that need to be compressed
        /// for web use.  Filename reference is given in case the file
        /// name must be updated.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public virtual Func<Stream, Stream> GetPreProcessor(ref string fileName)
        {
            return null;
        }

        public virtual Dictionary<string, string> GetMediaMetaData(string originalFileName)
        {
            var metaData = new Dictionary<string, string>
            {
                {"OriginalExtension", Path.GetExtension(originalFileName)},
                {"OriginalFilename", originalFileName},
                {"UserIdentity", GetContext().User.Identity.Name}
            };

            return metaData;
        }

        /// <summary>
        /// Used as an entry point for subclasses that need to perform additional
        /// work on the completed upload.
        /// </summary>
        /// <param name="asset"></param>
        public virtual void OnMediaAssetCompleted(IUploadedMediaAsset asset)
        {
        }
        #endregion

        #region Private Methods

        private bool IsChunkedUpload()
        {
            return !string.IsNullOrWhiteSpace(GetContext().Request.Headers["Content-Range"]);
        }

        private IUploadedMediaAsset UploadPartialFile()
        {
            var context = GetContext();
            var headers = context.Request.Headers;
            var contentRange = GetContentRangeFromHeaders(headers);
            var chunkSize = GetChunkSizeFromHeaders(headers);
            var file = context.Request.Files[0];
            var endingByte = contentRange[1];

            // -- jQuery blue imp adding an extra byte for
            //    some reason.  Not sure if this is by design
            //    or how this line will effect other uploading scripts.
            var totalBytes = contentRange[2] - 1;

            if (chunkSize <= 0)
            {
                throw new HttpException((int)HttpStatusCode.BadRequest, "Invalid chunk size specified.");
            }

            var chunkIndex = (int)Math.Floor(endingByte * 1.0 / chunkSize) + (endingByte % chunkSize == 0 ? 0 : 1);
            var isLastChunk = endingByte >= totalBytes;
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.FileName);

            // -- Using SessionID as identifier for chunked uploading.
            var workingFileName = FormatWorkingIdentifier(string.Format("{0}_{1}.part", context.Session.SessionID, fileNameWithoutExtension));
            var metaData = GetMediaMetaData(file.FileName);
            var fileStream = file.InputStream;
            var uploadedAsset = new UploadedMediaAsset
            {
                ContentLength = totalBytes,
                FileIdentifier = workingFileName,
                OriginalFileName = file.FileName,
                IsComplete = isLastChunk,
                Progress = (endingByte * 1.0) / totalBytes
            };

            using (fileStream)
            {
                _mediaStorage.StoreMediaChunk(workingFileName, fileStream, chunkIndex, isLastChunk, metaData);
            }

            if (isLastChunk)
            {
                // -- Update filename and file path variables for the
                //    file status object that gets written.
                var finalFileName = FormatFinalIdentifier(file.FileName);

                // -- Perform any pre-processing
                var preProcessor = GetPreProcessor(ref finalFileName);

                if (preProcessor != null)
                {
                    // -- Put the newly created part (which is now complete) through the processor,
                    //    then create a new file from the processed stream.
                    using (var mediaStream = _mediaStorage.OpenMediaStream(workingFileName))
                    using (var transformedStream = preProcessor(mediaStream))
                    {
                        // -- Make sure we're at the beginning of the stream
                        //    before we store it.
                        if (transformedStream.CanSeek)
                        {
                            transformedStream.Seek(0, SeekOrigin.Begin);
                        }

                        uploadedAsset.ContentLength = (int)transformedStream.Length;

                        // -- Save the transformed stream to its final name (creates a new file).
                        _mediaStorage.StoreMedia(finalFileName, transformedStream, metaData);
                    }

                    // -- Now we have to delete the working file
                    _mediaStorage.DeleteMedia(workingFileName);
                }
                else
                {
                    // -- Simply rename the working file to the final name
                    _mediaStorage.MoveMedia(workingFileName, finalFileName);
                }

                uploadedAsset.FileIdentifier = finalFileName;
            }

            return uploadedAsset;
        }

        private static int GetChunkSizeFromHeaders(NameValueCollection headers)
        {
            var chunkSize = Convert.ToInt32(headers["X-Chunk-Size"] ?? "0");
            return chunkSize;
        }

        private static int[] GetContentRangeFromHeaders(NameValueCollection headers)
        {
            return Regex.Split(headers["Content-Range"], @"[^0-9]+")
                .Where(value => !string.IsNullOrWhiteSpace(value))
                .Select(value => Convert.ToInt32(value))
                .ToArray();
        }

        private IEnumerable<IUploadedMediaAsset> UploadAllFiles()
        {
            var context = GetContext();
            var assets = new List<IUploadedMediaAsset>();

            foreach (string fileKey in context.Request.Files)
            {
                var file = context.Request.Files[fileKey];

                // -- Only process valid files
                if (file == null || file.ContentLength == 0)
                {
                    continue;
                }

                // -- Rename if necessary
                var fileName = FormatFinalIdentifier(file.FileName);

                int fileLength;

                // -- Perform any pre-processing
                var preProcessor = GetPreProcessor(ref fileName);

                using (var fileStream = preProcessor != null ? preProcessor(file.InputStream) : file.InputStream)
                {
                    // -- Make sure we're at the beginning of the stream
                    //    before we store it.
                    if (fileStream.CanSeek)
                    {
                        fileStream.Seek(0, SeekOrigin.Begin);
                    }

                    fileLength = (int)fileStream.Length;

                    // -- Persist the media file
                    _mediaStorage.StoreMedia(fileName, fileStream, GetMediaMetaData(file.FileName));
                }

                var uploadedAsset = new UploadedMediaAsset
                {
                    FileIdentifier = fileName,
                    OriginalFileName = file.FileName,
                    ContentLength = fileLength,
                    IsComplete = true,
                    Progress = 100
                };

                assets.Add(uploadedAsset);
            }

            return assets;
        }

        private HttpContext GetContext()
        {
            return HttpContext.Current;
        }
        #endregion
    }
}
