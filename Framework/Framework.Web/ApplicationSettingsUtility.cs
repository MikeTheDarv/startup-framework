﻿using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace Startup.Framework.Web
{
    public static class ApplicationSettingsUtility
    {
        #region public static Methods
        public static T GetSessionSetting<T>(string key)
        {
            if (IsInWebContext())
            {
                var sessionObj = HttpContext.Current.Session[key];
                return sessionObj == null ? default(T) : (T)sessionObj;
            }

            var dataObj = CallContext.GetData(key);
            return dataObj == null ? default(T) : (T)dataObj;
        }

        public static void SetSessionSetting(string key, object setting)
        {
            if (IsInWebContext())
            {
                HttpContext.Current.Session[key] = setting;
                return;
            }

            CallContext.SetData(key, setting);
        }

        public static T GetCacheSetting<T>(string key)
        {
            string cacheKey = string.Format("cache_{0}", key);

            if (IsInWebContext())
            {
                var sessionObj = HttpContext.Current.Cache[cacheKey];
                return sessionObj == null ? default(T) : (T)sessionObj;
            }

            var dataObj = CallContext.GetData(cacheKey);
            return dataObj == null ? default(T) : (T)dataObj;
        }

        public static void SetCacheSetting(string key, object setting)
        {
            string cacheKey = string.Format("cache_{0}", key);

            if (IsInWebContext())
            {
                HttpContext.Current.Cache[cacheKey] = setting;
                return;
            }

            CallContext.SetData(cacheKey, setting);
        }

        public static string GetConfigSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static bool IsInWebContext()
        {
            return HttpContext.Current != null;
        }
        #endregion
    }
}
