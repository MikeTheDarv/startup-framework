﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Formatting = Newtonsoft.Json.Formatting;

namespace Startup.Framework.Web
{
    #region Object Extensions
    public static class ObjectExtensions
    {
        public static string ToJson(this object obj)
        {
            var settings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            return ToJson(obj, settings);
        }

        public static string ToJson(this object obj, JsonSerializerSettings settings)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None, settings);
        }

        public static string ToXml(this object obj)
        {
            return ToXml(obj, null);
        }

        public static string ToXml(this object obj, XmlWriterSettings settings)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            StringBuilder serializedAsXml = new StringBuilder();
            XmlWriter xwriter = settings != null
                ? XmlWriter.Create(serializedAsXml, settings)
                : XmlWriter.Create(serializedAsXml);

            serializer.Serialize(xwriter, obj);

            return serializedAsXml.ToString();
        }
    }
    #endregion
}
